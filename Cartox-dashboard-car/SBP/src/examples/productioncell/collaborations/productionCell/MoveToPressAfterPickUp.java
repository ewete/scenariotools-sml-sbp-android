package examples.productioncell.collaborations.productionCell;

import examples.productioncell.collaborations.ProductionCell;
import sbp.specification.events.Message;
import sbp.specification.events.MessageParameter;
import sbp.specification.scenarios.violations.Violation;

@SuppressWarnings("serial")
public class MoveToPressAfterPickUp extends ProductionCell {

	@Override
	protected void initialisation() {
		addInitializingMessage(new Message(ts, c, "blankArrived", MessageParameter.RANDOM));
	}

	@Override
	protected void registerRoleBindings() {
		bindRoleToObject(armA, c.getBinding().getArmA());
	}

	@Override
	protected void body() throws Violation {
		request(c, armA, "pickUp");
		waitFor(armA, c, "pickedUpBlank");
		request(true, c, armA, "moveToPress");
	}

	@Override
	protected void registerAlphabet() {
		setBlocked(blankArrived);
		setBlocked(c, armA, "pickUp");
		setBlocked(armA, c, "pickedUpBlank");
		setBlocked(c, armA, "moveToPress");
	}

}
