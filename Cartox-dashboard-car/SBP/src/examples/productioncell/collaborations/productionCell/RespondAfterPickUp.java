package examples.productioncell.collaborations.productionCell;

import examples.productioncell.collaborations.ProductionCell;
import sbp.specification.events.Message;
import sbp.specification.scenarios.violations.Violation;

@SuppressWarnings("serial")
public class RespondAfterPickUp extends ProductionCell {

	@Override
	protected void initialisation() {
		addInitializingMessage(new Message(c, armA, "pickUp"));
	}

	@Override
	protected void registerRoleBindings() {

	}

	@Override
	protected void body() throws Violation {
		request(armA, c, "pickedUpBlank");
	}

	@Override
	protected void registerAlphabet() {
		setBlocked(c, armA, "moveToPress");
		setBlocked(armA, c, "pickedUpBlank");
	}

}
