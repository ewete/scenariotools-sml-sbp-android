package examples.productioncell.runtime;

import examples.productioncell.specification.ProductionCellSpecification;
import sbp.runtime.SpecificationRunconfig;
import sbp.runtime.adapter.DistributedRuntimeAdapter;
import sbp.ui.ConsoleFrame;

public class ProductionCellRunconfig_9000_Controller extends SpecificationRunconfig<ProductionCellSpecification> {

	public ProductionCellRunconfig_9000_Controller() {
		super(new ProductionCellSpecification());
		enableDistributedExecutionMode(9000);
		getAdapter().setSpecificationRunconfig(this);
		ConsoleFrame logFrame = new ConsoleFrame("Controller");
		logFrame.setVisible(true);
		ProductionCellSettings.setSettings(logFrame.getRuntimeLog(), logFrame.getServerLog());
	}

	@Override
	protected void registerParticipatingObjects() {
		ProductionCellObjectSystem objectSystem = ProductionCellObjectSystem.getInstance();
		registerObject(objectSystem.tableSensor, UNCONTROLLABLE);
		registerObject(objectSystem.controller, CONTROLLABLE);
		registerObject(objectSystem.armA, UNCONTROLLABLE);
	}

	@Override
	protected void registerNetworkAdressesForObjects() {
		ProductionCellObjectSystem objectSystem = ProductionCellObjectSystem.getInstance();
		registerAddress(objectSystem.tableSensor, "localhost", 9002);
		// registerAddress(objectSystem.controller, "localhost", 9000);
		registerAddress(objectSystem.armA, "localhost", 9001);
	}

	public static void main(String[] args) {
		// Logger.enableLogging();
		SpecificationRunconfig.run(ProductionCellRunconfig_9000_Controller.class);
	}

	@Override
	protected void registerObservers() {
		// TODO Auto-generated method stub

	}

}
