package examples.productioncell.runtime;

import examples.productioncell.specification.ProductionCellSpecification;
import sbp.runtime.SpecificationRunconfig;
import sbp.runtime.adapter.DistributedRuntimeAdapter;

public class ProductionCellRunconfig_9002_TableSensor extends SpecificationRunconfig<ProductionCellSpecification> {

	public ProductionCellRunconfig_9002_TableSensor() {
		super(new ProductionCellSpecification());
		enableDistributedExecutionMode(9002);
		getAdapter().setSpecificationRunconfig(this);
		ProductionCellEnvironmentFrame logFrame = new ProductionCellEnvironmentFrame("TableSensor", getAdapter());
		logFrame.setVisible(true);
		ProductionCellSettings.setSettings(logFrame.getRuntimeLog(), logFrame.getServerLog());
	}

	@Override
	protected void registerParticipatingObjects() {
		ProductionCellObjectSystem objectSystem = ProductionCellObjectSystem.getInstance();
		registerObject(objectSystem.tableSensor, UNCONTROLLABLE);
		registerObject(objectSystem.controller, UNCONTROLLABLE);
		registerObject(objectSystem.armA, UNCONTROLLABLE);
	}

	@Override
	protected void registerNetworkAdressesForObjects() {
		ProductionCellObjectSystem objectSystem = ProductionCellObjectSystem.getInstance();
		// registerAddress(objectSystem.tableSensor, "localhost", 9002);
		registerAddress(objectSystem.controller, "localhost", 9000);
		registerAddress(objectSystem.armA, "localhost", 9001);
	}

	public static void main(String[] args) {
		// Logger.enableLogging();
		SpecificationRunconfig.run(ProductionCellRunconfig_9002_TableSensor.class);
	}

	@Override
	protected void registerObservers() {
		// TODO Auto-generated method stub

	}

}
