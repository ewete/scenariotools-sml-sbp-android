package sbp.exceptions;

import sbp.specification.Specification;

@SuppressWarnings("serial")
public class NoSpecificationScenariosRegisteredException extends RuntimeException {

	public NoSpecificationScenariosRegisteredException(Specification specification) {
		super("The Specification " + specification.getClass().getName()
				+ " has not registered any specification scenarios!");
	}

}
