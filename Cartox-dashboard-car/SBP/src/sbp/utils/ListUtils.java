package sbp.utils;

import java.util.ArrayList;
import java.util.List;

public class ListUtils {

	public static <T> List<T> arrayToList(T[] array) {
		List<T> list = new ArrayList<T>();
		for (T entry : array) {
			list.add(entry);
		}
		return list;
	}

}
