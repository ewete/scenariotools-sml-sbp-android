package sbp.runtime.network.client;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import sbp.runtime.network.messages.Messages;

public class ServerConnector {

	private Socket socket;
	private ObjectOutputStream out;

	private String hostName;
	private int port;
	private Object objectBinding;

	public ServerConnector(String hostName, int port, Socket socket) {
		this.hostName = hostName;
		this.port = port;
		this.socket = socket;
		try {
			this.out = new ObjectOutputStream(socket.getOutputStream());
		} catch (IOException ignored) {
		}
	}

	public String getHostName() {
		return hostName;
	}

	public int getPort() {
		return port;
	}

	public void send(Object objectToSend) throws UnknownHostException, IOException {
		out.writeObject(objectToSend);
		out.flush();
	}

	public void close() throws IOException {
		send(Messages.BYE.toString());
		socket.close();
	}

	public void setObjectBinding(Object objectBinding) {
		this.objectBinding = objectBinding;
	}
	
	public Object getObjectBinding() {
		return objectBinding;
	}

}
