package sbp.runtime.network;

import java.util.HashMap;
import java.util.Map;

public class ObjectNetworkMapper {

	private Map<NetworkEntry, Object> networkToEntryObjectMap;
	private Map<Object, NetworkEntry> objectToNetworkEntryMap;
	private Map<Object, Boolean> objectIsClientConnectedMap;
	private Map<Object, Boolean> objectIsServerConnectedMap;

	public Map<NetworkEntry, Object> getNetworkToEntryObjectMap() {
		if (networkToEntryObjectMap == null) {
			networkToEntryObjectMap = new HashMap<NetworkEntry, Object>();
		}
		return networkToEntryObjectMap;
	}

	public Map<Object, NetworkEntry> getObjectToNetworkEntryMap() {
		if (objectToNetworkEntryMap == null) {
			objectToNetworkEntryMap = new HashMap<Object, NetworkEntry>();
		}
		return objectToNetworkEntryMap;
	}

	public Map<Object, Boolean> getObjectIsClientConnectedMap() {
		if (objectIsClientConnectedMap == null) {
			objectIsClientConnectedMap = new HashMap<Object, Boolean>();
		}
		return objectIsClientConnectedMap;
	}

	public Map<Object, Boolean> getObjectIsServerConnectedMap() {
		if (objectIsServerConnectedMap == null) {
			objectIsServerConnectedMap = new HashMap<Object, Boolean>();
		}
		return objectIsServerConnectedMap;
	}

	public void addMapping(NetworkEntry networkEntry, Object object) {
		getNetworkToEntryObjectMap().put(networkEntry, object);
		getObjectToNetworkEntryMap().put(object, networkEntry);
		getObjectIsClientConnectedMap().put(object, false);
		getObjectIsServerConnectedMap().put(object, false);
	}

	public Object getObject(NetworkEntry networkEntry) {
		return getNetworkToEntryObjectMap().get(networkEntry);
	}

	public NetworkEntry getNetworkEntry(Object object) {
		return getObjectToNetworkEntryMap().get(object);
	}

	public boolean isClientConnected(Object object) {
		return getObjectIsClientConnectedMap().get(object);
	}

	public boolean setClientConnected(Object object, boolean connected) {
		boolean prev = isClientConnected(object);
		getObjectIsClientConnectedMap().put(object, connected);
		return prev;
	}

	public boolean isServerConnected(Object object) {
		return getObjectIsServerConnectedMap().get(object);
	}

	public boolean setServerConnected(Object object, boolean connected) {
		boolean prev = isServerConnected(object);
		getObjectIsServerConnectedMap().put(object, connected);
		return prev;
	}

	public boolean isEverythingServerConnected() {
		for (Object object : getObjectIsServerConnectedMap().keySet()) {
			if (!getObjectIsServerConnectedMap().get(object)) {
				return false;
			}
		}
		return true;
	}

}
