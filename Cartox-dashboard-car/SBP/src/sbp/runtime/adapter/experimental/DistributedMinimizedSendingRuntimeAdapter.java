package sbp.runtime.adapter.experimental;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import sbp.runtime.ObjectRegistry;
import sbp.runtime.SpecificationRunconfig;
import sbp.runtime.adapter.AbstractRuntimeAdapter;
import sbp.runtime.network.NetworkEntry;
import sbp.runtime.network.client.Client;
import sbp.runtime.network.exceptions.UnableToConnectException;
import sbp.runtime.network.server.Server;
import sbp.specification.events.Message;
import sbp.specification.role.Role;
import sbp.specification.scenarios.Scenario;

public class DistributedMinimizedSendingRuntimeAdapter extends AbstractRuntimeAdapter {

	private Client client;
	private Server server;
	private int port;

	public DistributedMinimizedSendingRuntimeAdapter(SpecificationRunconfig<?> specificationRunconfig, int port) {
		super(specificationRunconfig);
		this.port = port;
	}

	@Override
	public void run() {
		startServer();
		connectClients();
	}

	@Override
	public void startServer() {
		server = new Server(port);
		server.setEventQueueScenario(getEventQueueScenario());
		server.start();
	}

	@Override
	public void connectClients() {
		client = new Client();
		try {
			for (NetworkEntry networkEntry : ObjectRegistry.getInstance().getNetworkAddresses()) {
				if (!client.isConnectedTo(networkEntry.getHostName(), networkEntry.getPort())) {
					Object object = ObjectRegistry.getInstance().getObjectNetworkMapper().getNetworkToEntryObjectMap()
							.get(networkEntry);
					client.connect(networkEntry.getHostName(), networkEntry.getPort(), object);
					ObjectRegistry.getInstance().getObjectNetworkMapper().setClientConnected(object, true);
				}
			}
		} catch (IOException | UnableToConnectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void publish(Message event) {
		try {
			client.publish(event.serializeForNetwork(), findRelevantObjects(event));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private List<Object> findRelevantObjects(Message event) {
		List<Object> relevantObjects = new ArrayList<Object>();

		Role<?> senderRole = event.getSender();
		Role<?> receiverRole = event.getReceiver();
		Object sender = senderRole.getBinding();
		Object receiver = receiverRole.getBinding();

		List<Scenario> scenarios = getSpecificationRunconfig().getRunningScenarios();
		for (Scenario scenario : scenarios) {
			for (Object object : scenario.getActiveBindings()) {
				if (object == sender || object == receiver) {
					relevantObjects.addAll(scenario.getActiveBindings());
					break;
				}
			}
		}

		System.out.println(relevantObjects);

		return relevantObjects;
	}

	@Override
	public void receive(Message event) {
		getEventQueueScenario().enqueueEvent(event);
	}

}
