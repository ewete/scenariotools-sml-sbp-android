package sbp.runtime.adapter;

import sbp.runtime.SpecificationRunconfig;
import sbp.specification.events.Message;

public class LocalAdapter extends AbstractRuntimeAdapter {

	public LocalAdapter(SpecificationRunconfig<?> specificationRunconfig) {
		super(specificationRunconfig);
	}

	@Override
	public void run() {
		// Nothing to do...
	}

	@Override
	public void startServer() {
		// Nothing to do...
	}

	@Override
	public void connectClients() {
		// Nothing to do...
	}

	@Override
	public void publish(Message event) {
		// Nothing to do...
	}

	@Override
	public void receive(Message event) {
		getEventQueueScenario().enqueueEvent(event);
	}

}
