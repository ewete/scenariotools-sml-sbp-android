package sbp.runtime;

import static bp.BProgram.bp;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import bApplication.BApplication;
import bp.BProgram;
import bp.BThread;
import sbp.runtime.adapter.DistributedRuntimeAdapter;
import sbp.runtime.adapter.LocalAdapter;
import sbp.runtime.adapter.RuntimeAdapter;
import sbp.specification.Specification;
import sbp.specification.scenarios.Scenario;
import sbp.specification.scenarios.system.EventQueueScenario;
import sbp.specification.scenarios.system.ExecutorScenario;
import sbp.specification.scenarios.system.IdleScenario;
import sbp.specification.scenarios.system.InitializerScenario;
import sbp.specification.scenarios.system.PublisherScenario;
import sbp.specification.scenarios.system.SpectatorScenario;
import sbp.specification.scenarios.utils.ExecutionMode;
import sbp.specification.scenarios.utils.ScenarioObserver;

public abstract class SpecificationRunconfig<T extends Specification> implements BApplication {

	public static final int CONTROLLABLE = 0;
	public static final int UNCONTROLLABLE = 1;

	protected static boolean ENABLE_IDLE_SCENARIO = true;
	protected static boolean ENABLE_EVENTQUEUE_SCENARIO = true;
	protected static boolean ENABLE_SPECTATOR_SCENARIO = true;
	protected static boolean ENABLE_PUBLISHER_SCENARIO = true;
	protected static boolean ENABLE_EXECUTOR_SCENARIO = true;
	protected static boolean ENABLE_INITIALIZER_SCENARIO = true;

	private static SpecificationRunconfig<?> instance;

	private T specification;
	private RuntimeAdapter adapter;

	private List<Object> participatingObjects;

	private double latestSpecificationScenarioPriority = 1.0d;
	private double latestAssumptionScenarioPriority = (Double.MAX_VALUE / 2);

	private IdleScenario idleScenario;
	private EventQueueScenario eventQueueScenario;
	private SpectatorScenario spectatorScenario;
	private PublisherScenario publisherScenario;
	private ExecutorScenario executorScenario;
	private InitializerScenario initializerScenario;

	private List<Scenario> testScenarios;

	public SpecificationRunconfig(T specification) {
		this.specification = specification;
		SpecificationRunconfig.setInstance(this);
		registerParticipatingObjects();
		registerNetworkAdressesForObjects();
		registerObservers();
	}

	protected abstract void registerParticipatingObjects();

	protected abstract void registerNetworkAdressesForObjects();

	protected abstract void registerObservers();

	/**
	 * Registers an object as controllable.
	 * 
	 * @param object
	 */
	protected void registerObject(Object object) {
		registerObject(object, false);
	}

	/**
	 * Registers an object as CONTROLLABLE or UNCONTROLLABLE.
	 * 
	 * @param object
	 * @param controllableMode
	 */
	protected void registerObject(Object object, int controllableMode) {
		if (controllableMode == CONTROLLABLE) {
			registerObject(object, false);
		} else if (controllableMode == UNCONTROLLABLE) {
			registerObject(object, true);
		}
	}

	private void registerObject(Object object, boolean uncontrollable) {
		getParticipatingObjects().add(object);
		ObjectRegistry.getInstance().addObject(object);
		if (uncontrollable) {
			ObjectRegistry.getInstance().setUncontrollable(object);
		}
	}

	/**
	 * Registers a network address for an object.
	 * 
	 * @param object
	 * @param hostName
	 * @param port
	 */
	protected void registerAddress(Object object, String hostName, int port) {
		ObjectRegistry.getInstance().setNetworkAddress(object, hostName, port);
	}

	/**
	 * Returns the currently running instance of this class.
	 * 
	 * @return
	 */
	public static SpecificationRunconfig<?> getInstance() {
		return instance;
	}

	/**
	 * Sets the currently running instance of this class.
	 * 
	 * @param instance
	 */
	public static void setInstance(SpecificationRunconfig<?> instance) {
		SpecificationRunconfig.instance = instance;
	}

	/**
	 * This method configures the runtime as local simulation and enables the
	 * {@link LocalAdapter}.
	 */
	protected void enableLocalSimulationMode() {
		setAdapter(new LocalAdapter(this));
	}

	/**
	 * This method configures the runtime as distributed execution and enables
	 * the {@link DistributedRuntimeAdapter}.
	 * 
	 * @param port
	 */
	protected void enableDistributedExecutionMode(int port) {
		setAdapter(new DistributedRuntimeAdapter(this, port));
	}

	public T getSpecification() {
		return specification;
	}

	/**
	 * Returns a list of registered objects.
	 * 
	 * @return
	 */
	public List<Object> getParticipatingObjects() {
		if (participatingObjects == null) {
			participatingObjects = new ArrayList<Object>();
		}
		return participatingObjects;
	}

	/**
	 * Returns the configured runtime adapter.
	 * 
	 * @return
	 */
	public RuntimeAdapter getAdapter() {
		if (adapter == null) {
			enableLocalSimulationMode();
		}
		return adapter;
	}

	/**
	 * Sets the runtime adapter for the runtime.
	 * 
	 * @param adapter
	 */
	protected void setAdapter(RuntimeAdapter adapter) {
		this.adapter = adapter;
	}

	private double getNextSpecificationScenarioPriority() {
		latestSpecificationScenarioPriority = Math.nextUp(latestSpecificationScenarioPriority);
		return latestSpecificationScenarioPriority;
	}

	private double getNextAssumptionScenarioPriority() {
		latestAssumptionScenarioPriority = Math.nextUp(latestAssumptionScenarioPriority);
		return latestAssumptionScenarioPriority;
	}

	public EventQueueScenario getEventQueueScenario() {
		if (eventQueueScenario == null) {
			eventQueueScenario = new EventQueueScenario();
			eventQueueScenario.setType(Scenario.SYSTEM);
		}
		return eventQueueScenario;
	}

	public IdleScenario getIdleScenario() {
		if (idleScenario == null) {
			idleScenario = new IdleScenario();
			idleScenario.setType(Scenario.SYSTEM);
		}
		return idleScenario;
	}

	public SpectatorScenario getSpectatorScenario() {
		if (spectatorScenario == null) {
			spectatorScenario = new SpectatorScenario();
			spectatorScenario.setType(Scenario.SYSTEM);
		}
		return spectatorScenario;
	}

	public PublisherScenario getPublisherScenario() {
		if (publisherScenario == null) {
			publisherScenario = new PublisherScenario(getAdapter());
			publisherScenario.setType(Scenario.SYSTEM);
		}
		return publisherScenario;
	}

	public ExecutorScenario getExecutorScenario() {
		if (executorScenario == null) {
			executorScenario = new ExecutorScenario();
			executorScenario.setType(Scenario.SYSTEM);
		}
		return executorScenario;
	}

	public InitializerScenario getInitializerScenario() {
		if (initializerScenario == null) {
			initializerScenario = new InitializerScenario();
			initializerScenario.setType(Scenario.SYSTEM);
		}
		return initializerScenario;
	}

	public Scenario addSpecificationScenarioToBApplication(Scenario scenario) {
		addScenarioToBApplication(scenario, getNextSpecificationScenarioPriority());
		return scenario;
	}

	public Scenario addAssumptionScenarioToBApplication(Scenario scenario) {
		addScenarioToBApplication(scenario, getNextAssumptionScenarioPriority());
		return scenario;
	}

	private Scenario addScenarioToBApplication(Scenario scenario, double priority) {
		scenario.setPriority(priority);
		bp.add(scenario, priority);
		return scenario;
	}

	/**
	 * Adds an observer to the {@link SpectatorScenario}.
	 * 
	 * @param scenarioObserver
	 */
	protected void addScenarioObserver(ScenarioObserver scenarioObserver) {
		scenarioObserver.setScenario(getSpectatorScenario());
	}

	@Override
	public void runBApplication() {
		adapter.run();
		double priorityExecutorScenario = Double.MAX_VALUE;
		double priorityPublisherScenario = Math.nextAfter(priorityExecutorScenario, Double.MIN_VALUE);
		double prioritySpectatorScenario = Math.nextAfter(priorityPublisherScenario, Double.MIN_VALUE);
		double priorityInitializerScenario = Math.nextAfter(prioritySpectatorScenario, Double.MIN_VALUE);
		double priorityIdleScenario = Math.nextAfter(priorityInitializerScenario, Double.MIN_VALUE);
		double priorityEventQueueScenario = Math.nextAfter(priorityIdleScenario, Double.MIN_VALUE);
		if (ENABLE_EVENTQUEUE_SCENARIO)
			addScenarioToBApplication(getEventQueueScenario(), priorityEventQueueScenario);
		if (ENABLE_IDLE_SCENARIO)
			addScenarioToBApplication(getIdleScenario(), priorityIdleScenario);
		if (ENABLE_INITIALIZER_SCENARIO)
			addScenarioToBApplication(getInitializerScenario(), priorityInitializerScenario);
		if (ENABLE_SPECTATOR_SCENARIO)
			addScenarioToBApplication(getSpectatorScenario(), prioritySpectatorScenario);
		if (ENABLE_PUBLISHER_SCENARIO)
			addScenarioToBApplication(getPublisherScenario(), priorityPublisherScenario);
		if (ENABLE_EXECUTOR_SCENARIO) {
			getExecutorScenario().getTransformationRules().addAll(specification.getTransformationRules());
			addScenarioToBApplication(getExecutorScenario(), priorityExecutorScenario);
		}
		bp.startAll();
	}

	/**
	 * Initializes the SBP runtime with the given
	 * {@link SpecificationRunconfig}.
	 * 
	 * @param specificationRunconfigClass
	 */
	public static void run(
			Class<? extends SpecificationRunconfig<? extends Specification>> specificationRunconfigClass) {
		try {
			BProgram.startBApplication(specificationRunconfigClass, specificationRunconfigClass.getName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Returns a list of active scenarios.
	 * 
	 * @return
	 */
	public List<Scenario> getRunningScenarios() {
		List<Scenario> scenarios = new ArrayList<Scenario>();

		for (Entry<Double, BThread> entry : bp.getAllBThreads().entrySet()) {
			BThread value = entry.getValue();
			if (value instanceof Scenario) {
				Scenario scenario = (Scenario) value;
				if (!scenario.getType().equals(Scenario.SYSTEM)) {
					scenarios.add(scenario);
				}
			}
		}

		return scenarios;
	}

	/**
	 * Returns a list of test scenarios.
	 * 
	 * @return
	 */
	public List<Scenario> getTestScenarios() {
		if (testScenarios == null) {
			testScenarios = new ArrayList<Scenario>();
		}
		return testScenarios;
	}

	/**
	 * Adds a test scenario to the runtime and runs it.
	 * 
	 * @param scenario
	 */
	public void runTestScenario(Scenario scenario) {
		getTestScenarios().add(scenario);
		scenario.setExecutionMode(ExecutionMode.TEST);
		scenario.setType(Scenario.SYSTEM);
		addSpecificationScenarioToBApplication(scenario);
		scenario.startBThread();
	}

}
