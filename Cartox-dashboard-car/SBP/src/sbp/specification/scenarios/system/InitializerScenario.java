package sbp.specification.scenarios.system;

import sbp.runtime.SpecificationRunconfig;
import sbp.specification.Specification;
import sbp.specification.events.Message;
import sbp.specification.scenarios.Scenario;
import sbp.specification.scenarios.violations.Violation;

@SuppressWarnings("serial")
public class InitializerScenario extends SystemScenario {

	@Override
	protected void body() throws Violation {
		// Wait for events
		waitForAnything();
		// Retrieve components
		SpecificationRunconfig<?> runconfiguration = SpecificationRunconfig.getInstance();
		Specification specification = runconfiguration.getSpecification();

		Message lastEvent = getLastMessage();

		for (Scenario scenario : specification.getSpecificationScenarios()) {
			if (scenario.getInitializingMessages().contains(lastEvent)) {
				runconfiguration.addSpecificationScenarioToBApplication(scenario.copy()).startBThread();
			}
		}
		for (Scenario scenario : specification.getAssumptionScenarios()) {
			if (scenario.getInitializingMessages().contains(lastEvent)) {
				runconfiguration.addAssumptionScenarioToBApplication(scenario.copy()).startBThread();
			}
		}
	}

}
