package sbp.specification.events;

import java.util.List;

public class MessageParameter {

	public static final ParameterRandom RANDOM = ParameterRandom.getInstance();

	private String name;

	public MessageParameter(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public static boolean compareParameters(Object parameter1, Object parameter2) {
		if (parameter1 == RANDOM || parameter2 == RANDOM) {
			return true;
		}
		if (parameter1.equals(parameter2)) {
			return true;
		}
		return false;
	}

	public static boolean compareParameterLists(List<Object> parameterList1, List<Object> parameterList2) {
		if (parameterList1.size() != parameterList2.size()) {
			return false;
		}
		for (int i = 0; i < parameterList1.size(); i++) {
			Object parameter1 = parameterList1.get(i);
			Object parameter2 = parameterList2.get(i);
			if (!compareParameters(parameter1, parameter2)) {
				return false;
			}
		}
		return true;
	}

}
