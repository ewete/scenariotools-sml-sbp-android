package sbp.specification.events;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ObjectParameter implements Serializable {

	private int id;

	public ObjectParameter(Integer id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

}
