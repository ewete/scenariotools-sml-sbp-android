package sbp.specification;

import java.util.ArrayList;
import java.util.List;

import sbp.exceptions.NoSpecificationScenariosRegisteredException;
import sbp.runtime.objectsystem.TransformationRule;
import sbp.specification.scenarios.Scenario;

public abstract class Specification {

	private List<Scenario> specificationScenarios = new ArrayList<Scenario>();
	private List<Scenario> assumptionScenarios = new ArrayList<Scenario>();
	private List<TransformationRule> transformationRules;

	public Specification() {
		registerScenarios();
		registerTransformationRules();
		if (specificationScenarios.isEmpty()) {
			throw new NoSpecificationScenariosRegisteredException(this);
		}
	}

	/**
	 * Returns the list of transformation rules.
	 * 
	 * @return
	 */
	public List<TransformationRule> getTransformationRules() {
		if (transformationRules == null) {
			transformationRules = new ArrayList<TransformationRule>();
		}
		return transformationRules;
	}

	/**
	 * Returns the list of registered specification scenarios.
	 * 
	 * @return
	 */
	public List<Scenario> getSpecificationScenarios() {
		return specificationScenarios;
	}

	private void addSpecificationScenario(Scenario scenario) {
		specificationScenarios.add(scenario);
	}

	/**
	 * Returns the list of registered assumption scenarios.
	 * 
	 * @return
	 */
	public List<Scenario> getAssumptionScenarios() {
		return assumptionScenarios;
	}

	private void addAssumptionScenario(Scenario scenario) {
		assumptionScenarios.add(scenario);
	}

	/**
	 * Registers a specification scenario.
	 * 
	 * @param scenarioClass
	 * @return
	 */
	protected boolean registerSpecificationScenario(Class<? extends Scenario> scenarioClass) {
		Scenario scenario;
		try {
			scenario = scenarioClass.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
			return false;
		}
		addSpecificationScenario(scenario);
		scenario.setType(Scenario.SPECIFICATION);
		scenario.initialize();
		return true;
	}

	/**
	 * Registers an assumption scenario.
	 * 
	 * @param scenarioClass
	 * @return
	 */
	protected boolean registerAssumptionScenario(Class<? extends Scenario> scenarioClass) {
		Scenario scenario;
		try {
			scenario = scenarioClass.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
			return false;
		}
		addAssumptionScenario(scenario);
		scenario.setType(Scenario.ASSUMPTION);
		scenario.initialize();
		return true;
	}

	protected abstract void registerScenarios();

	protected abstract void registerTransformationRules();

	/**
	 * Registers a transformation rule.
	 * 
	 * @param rule
	 */
	protected void registerRule(TransformationRule rule) {
		getTransformationRules().add(rule);
	}

}
