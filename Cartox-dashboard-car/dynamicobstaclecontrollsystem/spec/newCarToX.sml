//new sml

system specification CarToX {
    
    domain cartox
    
    define CarController as controllable
    define ObstacleController as controllable
	define Environment as uncontrollable
	define CarDashboard as uncontrollable
	define CoordinatePrObstacleessor as uncontrollable
	define StreetSectionController as uncontrollable
    
	collaboration CarDriving {

		dynamic role Environment env
		dynamic role CarController car

		specification scenario CarMovesToNextArea {
			message env -> car.currentGPSCoordinateChanged
		}
	}
    
    collaboration CarApproachingObstacleAssumptions {

		dynamic role Environment env
		dynamic role CarController car
		dynamic role CoordinatePrObstacleessor cp

		/*
		 * When a car approaches an obstacle on the blObstacleked lane,
		 * an according event will Obstaclecur
		 */
		assumption scenario ApproachingObstacleOnBlObstaclekedLaneAssumption
		with dynamic bindings [            
		] {
			message env -> cp.currentGPSCoordinateChanged
			/* das Auto von "out" bis "inEndArea"
			  				oder von "out" bis "inBeginArea" */
			interrupt if [ car.getNextObstacle() == null ]
			bool inBegin = cp.isInBeginArea();
			bool inEnd = cp.isInEndArea();
			bool inObstacle = cp.isInObstacleArea();
			alternative if [!cp.getInObstacleArea && !cp.getInBeginArea && !cp.getInEndArea]{
				message strict requested cp -> car.setApproachingObstacle(obstacle)
					
				message strict requested cp -> cp.setInBeginArea(inBegin) 
				message strict requested cp -> cp.setInEndArea(inEnd)
				message strict requested cp -> cp.setInObstacleArea(inObstacle)
			}
		}


	}
    
    collaboration CarEntersOrLeavesNarrowPassageAssumptions {

		dynamic role Environment env
		dynamic role CarController car
		dynamic role CoordinatePrObstacleessor cp


		assumption scenario CarEntersNarrowPassage
		with dynamic bindings [
		] {
			message env -> cp.currentGPSCoordinateChanged
			// If before there was no obstacle
			// and  
			// das Auto von "inEndArea" bis "inObstacleArea"
			//     oder von "inBeginArea" bis "inObstacleArea" 
			// then the car enters the narrow passage
			interrupt if [ car.getNextObstacle() == null ]
			bool inBegin = cp.isInBeginArea();
			bool inEnd = cp.isInEndArea();
			bool inObstacle = cp.isInObstacleArea();
			alternative if [!cp.getInObstacleArea && (cp.getInBeginArea || cp.getInEndArea )]{
				message strict requested cp -> car.hasEnteredNarrowPassage
					
				message strict requested cp -> cp.setInBeginArea(inBegin) 
				message strict requested cp -> cp.setInEndArea(inEnd)
				message strict requested cp -> cp.setInObstacleArea(inObstacle)
			}
			
		}


		assumption scenario CarHasLeftNarrowPassage
		with dynamic bindings [
		] {
			message env -> cp.currentGPSCoordinateChanged
			/* das Auto von "inObstacleArea" bis "out" */
			interrupt if [  car.getNextObstacle() != null ]
			bool inBegin = cp.isInBeginArea();
			bool inEnd = cp.isInEndArea();
			bool inObstacle = cp.isInObstacleArea();
			alternative if [cp.getInObstacleArea]{
				message strict requested cp -> car.hasLeftNarrowPassage
					
				message strict requested cp -> cp.setInBeginArea(inBegin) 
				message strict requested cp -> cp.setInEndArea(inEnd)
				message strict requested cp -> cp.setInObstacleArea(inObstacle)
			}
		} constraints [
			//forbidden message env -> car.changedToOppositeArea()
		]

	}

    collaboration ShowInformationOnDashboard {

		dynamic role Environment env
		dynamic role CarController car
		//dynamic role Driver driver
		dynamic role CarDashboard dashboard
		dynamic role ObstacleController obstacleControl

		/*
		 * Show stop or go to the driver when approaching the obstacle
		 * before actually reaching it
		 */
		specification scenario DashboardOfCarOnBlObstaclekedLaneShowsStopOrGo
		with dynamic bindings [
			//bind driver to car.driver
			bind dashboard to car.dashboard
		] {
			message env -> car.setApproachingObstacle(*)
			alternative {
				message strict car -> dashboard.showGo
			} or {
				message strict car -> dashboard.showStop
			}
		}

		/*
		 * The dashboard shows a go or a stop light as reaction to the obstacle
		 * controls response after registering
		 */
		specification scenario DashboardShowsGo
		with dynamic bindings [
			bind dashboard to car.dashboard
		] {
			message obstacleControl -> car.enteringAllowed
			message strict requested car -> dashboard.showGo
		}

		specification scenario DashboardShowsStop
		with dynamic bindings [
			bind dashboard to car.dashboard
		] {
			message obstacleControl -> car.enteringDisallowed
			message strict requested car -> dashboard.showStop
		}

	}
    
	collaboration CarsRegisterWithObstacleControl {

		dynamic role Environment env
		dynamic role CarController car
		dynamic role CarController nextCar
		dynamic role CarDashboard dashboard
		//dynamic role Obstacle obstacle
		dynamic role ObstacleController obstacleControl
		//dynamic role LaneArea currentArea
		//dynamic role LaneArea nextArea

		/*
		 * Stop or go must be shown to the driver when the car
		 * approaches an obstacle
		 */
		specification scenario ControlStationAllowsOrDisallowsCarToEnterNarrowPassage
		with dynamic bindings [
			//bind obstacle to car.approachingObstacle
			bind obstacleController to car.getNextObstacle()
			bind dashboard to car.getDashboard()
		] {
			message env -> car.setApproachingObstacle(*)
			message strict requested car -> obstacleControl.register
			alternative {
				message strict requested obstacleController -> car.enteringAllowed
			} or {
				message strict requested obstacleController -> car.enteringDisallowed
			}
		}

		specification scenario ControlStationAllowsOrDisallowsCarOnBlObstaclekedLaneToEnter
		with dynamic bindings [
			//bind obstacle to car.approachingObstacle
			bind obstacleController to car.getNextObstacle()
			bind dashboard to car.getDashboard()
		] {
			message env -> car.setApproachingObstacle(*)
			message strict requested car -> obstacleController.register
			alternative if [ obstacleController.getCarsOnNarrowPassageLaneAllowedToPass.isEmpty() ] {
				message strict requested obstacleController -> car.enteringAllowed
				message strict car -> dashboard.showGo
			} or if [ ! obstacleController.getCarsOnNarrowPassageLaneAllowedToPass.isEmpty() ] {
				message strict requested obstacleController -> car.enteringDisallowed
				message strict car -> dashboard.showStop
			}
		}

		specification scenario CarUnregistersAfterLeavingNarrowPassage
		with dynamic bindings [
			//bind obstacle to car.approachingObstacle
			bind obstacleController to car.getNextObstacle()
		] {
			message env -> car.hasLeftNarrowPassage
			message strict requested car -> obstacleController.unregister
		}

		/*
		 * The obstacle control tells the next car that it may now enter the
		 * narrow passage
		 */
		specification scenario ControlStationTellsNextRegisteredCarToEnter
		with dynamic bindings [
			bind nextCar to obstacleController.getRegisteredCarsWaiting.first()
		] {
			message car -> obstacleControl.unregister
			interrupt if [ nextCar == null ]
			message strict requested obstacleController -> nextCar.enteringAllowed
		}

		/*
		 * A car that has been told to wait may eventually continue driving
		 * and pass the obstacle
		 */
		specification scenario CarEventuallyAllowedToPassObstacle {
			message obstacleController -> car.enteringDisallowed()
			message strict obstacleController -> car.enteringAllowed()
		}

		/*
		 * A car that has been told that it may pass the obstacle will enter
		 * the narrow passage
		 */
		 //TODO _CarOnRightLane see under 
		assumption scenario CarDrivesIntoNarrowPassageIfItMayPassObstacle_CarOnRightLane
		with dynamic bindings [
			//bind currentArea to car.inArea
			//bind nextArea to currentArea.next
			//bind env to car.environment
		] {
			message obstacleController -> car.enteringAllowed
			//interrupt if [ car.onLane != car.drivingInDirectionOfLane ]
			alternative if [ car.getNextObstacle() == null ] {
				message strict requested env -> car.movedToNextArea
			} or if [ car.getNextObstacle()  != null ] {
				//message strict requested env -> car.changedToOppositeArea
				message strict requested env -> car.movedToNextAreaOnOvertakingLane
			}
		}
		
		//TODO _CarOnLeftLane see above
		assumption scenario CarDrivesIntoNarrowPassageIfItMayPassObstacle_CarOnLeftLane
		with dynamic bindings [
			//bind currentArea to car.inArea
			//bind nextArea to currentArea.previous
			//bind env to car.environment
		] {
			message obstacleController -> car.enteringAllowed
			//interrupt if [ car.onLane == car.drivingInDirectionOfLane ]
			alternative if [ car.getNextObstacle() == null ] {
				message strict requested env -> car.movedToNextAreaOnOvertakingLane
			} or if [ car.getNextObstacle() != null ] {
				//message strict requested env -> car.changedToOppositeArea
				message strict requested env -> car.currentGPSCoordinateChanged
			}
		}

	}
	
	collaboration SetStatus{
		dynamic role CoordinatePrObstacleessor cp
		// PseudObstacleode
		// TODO assumption oder specification??
		assumption scenario SetInBeginAreaScenario
		with dynamic bindings [
		] {
			message cp -> cp.setInBeginArea(boolean)
			cp.setInBeginArea(boolean)
		}
		
		assumption scenario SetInEndAreaScenario
		with dynamic bindings [
		] {
			message cp -> cp.setInEndArea(boolean)
			cp.setInEndArea(boolean)
		}
		
		assumption scenario SetInObstacleAreaScenario
		with dynamic bindings [
		] {
			message cp -> cp.setInObstacleArea(boolean)
			cp.setInObstacleArea(boolean)
		}
		
		assumption scenario SetCoordinateProcessorStatusScenario
		with dynamic bindings [
		] {
			message cp -> cp.setInBeginArea(boolean)
			cp.setInBeginArea(boolean)
		
			message cp -> cp.setInObstacleArea(boolean)
			cp.setInObstacleArea(boolean)
			
			message cp -> cp.setInEndArea(boolean)
			cp.setInEndArea(boolean)
		}
	}	

}

