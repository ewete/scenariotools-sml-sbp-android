the format of the coordinates is (latitude, longitude)

The simulation-road is represented as follows:

============================================================
<---  F     E      end            begin   B     A       <---
                          D  C
                      
---> 1    2    3        4      5          6     7        -->
============================================================

Obstacle Control Coordinates

begin: ( 52.37966, 9.72071 )
end  : ( 52.37977, 9.72054 )

The route of one car 
( there is an Obstacle Control 
  at the lane which this car is )
A    : ( 52.37949, 9.72099 )
B    : ( 52.3796 , 9.72082 )
C    : ( 52.37967, 9.72061 )
D    : ( 52.37971 , 9.72055 )
E    : ( 52.37983, 9.72044 )
F    : ( 52.37988, 9.72035 )  

The route of another car
( there is NO Obstacle Control 
  at the lane which this car is )
1    : ( 52.37985, 9.72032 )
2    : ( 52.3798 , 9.72041 )
3    : ( 52.37977, 9.72046 )
4    : ( 52.3797 , 9.72056 )
5    : ( 52.37969, 9.72058 )
6    : ( 52.37959, 9.72073 )
7    : ( 52.37953, 9.72084 )