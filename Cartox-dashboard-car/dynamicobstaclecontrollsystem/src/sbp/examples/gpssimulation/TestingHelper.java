package sbp.examples.gpssimulation;

import java.util.ArrayList;
import java.util.List;

import sbp.examples.domainmodel.CarToXSystem;
import sbp.examples.domainmodel.CoordinatesProcessor;
import sbp.examples.domainmodel.Environment;
import sbp.runtime.adapter.RuntimeAdapter;
import sbp.specification.events.Message;
import sbp.specification.role.Role;

public class TestingHelper {

	private static Role<Environment> env = new Role<Environment>(Environment.class, "env");
	private static Message END = new Message(env, env, "ENDING");

	static public void publishMessages(RuntimeAdapter runtimeAdapter, List<Message> messages,
			CarToXSystem carToXSystem) {
		for (Message m : messages) {
			publish(runtimeAdapter, m);
		}
		sendEndMessage(runtimeAdapter, carToXSystem);
	}

	/*
	 * the publish the message from messageSet1 and messageSet2 alternately 
	 * for example 
	 * messageA from messageSet1 -> messageB from messageSet2 ->
	 * messageC from messageSet1 -> messageD from messageSet2 -> ...
	 */
	static public void publishMessagesInterleaved(RuntimeAdapter runtimeAdapter, List<Message> messageSet1,
			List<Message> messageSet2) {
		List<Message> longerList = (messageSet1.size() > messageSet2.size()) ? messageSet1 : messageSet2;
		List<Message> shorterList = !(messageSet1.size() > messageSet2.size()) ? messageSet1 : messageSet2;

		// CoordinatesProcessor cp =
		// (CoordinatesProcessor)messageSet1.get(0).getReceiver().getBinding();
		// cp.getCar().getDashboard().getInfo();

		for (int i = 0, k = 0; i < longerList.size() || k < shorterList.size();) {
			if (i < longerList.size()) {
				System.out.println("DEBUG:" + getInfoString(longerList, i));
				if (!getInfoString(longerList, i).equals("STOP")) {

					publish(runtimeAdapter, longerList.get(i++));
				} else {
					System.out.println("STOP WAS CAUGHT!");
				}

				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			if (k < shorterList.size()) {
				System.out.println("DEBUG:" + getInfoString(shorterList, k));
				if (!getInfoString(shorterList, k).equals("STOP")) {

					publish(runtimeAdapter, shorterList.get(k++));
				} else {
					System.out.println("STOP WAS CAUGHT!");
				}

				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
	}

	/*
	 * to get the output string of the receiver GO! STOP! etc
	 */
	static public String getInfoString(List<Message> messageSet, int index) {
		CoordinatesProcessor cp = (CoordinatesProcessor) messageSet.get(index).getReceiver().getBinding();
		String infoString = cp.getCar().getDashboard().getInfo();
		return infoString;
	}

	static public void publishMessagesInterleavedRandomly(RuntimeAdapter runtimeAdapter, List<Message> messageSet1,
			List<Message> messageSet2) {
		int random;
		for (int i = 0, k = 0; i < messageSet1.size() && k < messageSet2.size();) {
			random = (int) (Math.random() * 2);
			if (random == 0 && i < messageSet1.size()) {
				publish(runtimeAdapter, messageSet1.get(i++));
			} else if (random == 1 && k < messageSet2.size()) {
				publish(runtimeAdapter, messageSet2.get(k++));
			}
		}
	}

	static private void sendEndMessage(RuntimeAdapter runtimeAdapter, CarToXSystem carToXSystem) {
		// Environment e = CarToXSystemFactory.getInstance().getEnvironment();
		// //This is not agood way to get the environment
		Environment e = carToXSystem.getEnv();// It should be like this
		env.setBinding(e);
		publish(runtimeAdapter, END);
	}

	static private List<Message> generateMessages(Role<Environment> sender, Role<CoordinatesProcessor> receiver,
			String message, List<Object> parameter) {
		List<Message> outputList = new ArrayList<Message>();
		for (int i = 0; i < parameter.size(); i++) {
			outputList.add(new Message(sender, receiver, message, parameter.get(i)));
		}
		return outputList;
	}

	static private void publish(RuntimeAdapter runtimeAdapter, Message m) {
		runtimeAdapter.receive(m);
		runtimeAdapter.publish(m);
	}

}
