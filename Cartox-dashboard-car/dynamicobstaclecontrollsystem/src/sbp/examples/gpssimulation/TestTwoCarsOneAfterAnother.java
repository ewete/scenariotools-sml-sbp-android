package sbp.examples.gpssimulation;

import org.junit.Test;

import sbp.examples.domainmodel.CarToXSystem;
import sbp.examples.domainmodel.CarToXSystemFactory;
import sbp.examples.domainmodel.CoordinatesProcessor;
import sbp.examples.domainmodel.Environment;
import sbp.examples.domainmodel.GPSCoordinate;
import sbp.examples.runconfig.CarToXRunconfig_ObstacleController_TwoCars;
import sbp.runtime.SpecificationRunconfig;
import sbp.runtime.adapter.RuntimeAdapter;
import sbp.specification.events.Message;
import sbp.specification.role.Role;

public class TestTwoCarsOneAfterAnother {

	@Test
	public void test() throws InterruptedException {
		// fail("Not yet implemented");
		System.out.println("TestChangeGPSCoordinates Started");
		// System.out.println("TODO" +
		// Main.carToXSystem.getCarControllers().get(0).getName());
		// fail("Not yet implemented");
		
		GPSCoordinate ocBeginPoint = new GPSCoordinate(52.37966, 9.72071);
		GPSCoordinate ocEndPoint = new GPSCoordinate(52.37977, 9.72054);
		CarToXSystem carToXSystem = CarToXSystemFactory.getInstance().getCarToXSystemTwoCars(ocBeginPoint, ocEndPoint);
		// End: Domain model erzeugen
		SpecificationRunconfig.run(CarToXRunconfig_ObstacleController_TwoCars.class);

		RuntimeAdapter runtimeAdapter = SpecificationRunconfig.getInstance().getAdapter();

		Role<Environment> env = new Role<Environment>(Environment.class, "env");
		Role<CoordinatesProcessor> cpJames = new Role<CoordinatesProcessor>(CoordinatesProcessor.class, "cpJames");
		Role<CoordinatesProcessor> cpAnna = new Role<CoordinatesProcessor>(CoordinatesProcessor.class, "cpAnna");

		Message m1JamesNothing = new Message(env, cpJames, "currentGPSCoordinateChanged",
				new GPSCoordinate(52.37949, 9.72099));
		Message m2JamesApproaching = new Message(env, cpJames, "currentGPSCoordinateChanged",
				new GPSCoordinate(52.3796, 9.72082));
		Message m3JamesEntered = new Message(env, cpJames, "currentGPSCoordinateChanged",
				new GPSCoordinate(52.37967, 9.72061));
		Message m4JamesStillOn = new Message(env, cpJames, "currentGPSCoordinateChanged",
				new GPSCoordinate(52.37971, 9.72055));
		Message m5JamesLeft = new Message(env, cpJames, "currentGPSCoordinateChanged",
				new GPSCoordinate(52.37983, 9.72044));
		Message m6JamesNothing = new Message(env, cpJames, "currentGPSCoordinateChanged",
				new GPSCoordinate(52.37988, 9.72035));

		Message m1AnnaNothing = new Message(env, cpAnna, "currentGPSCoordinateChanged",
				new GPSCoordinate(52.37985, 9.72032));
		Message m2AnnaApproaching = new Message(env, cpAnna, "currentGPSCoordinateChanged",
				new GPSCoordinate(52.3798, 9.72041));
		Message m3AnnaApproachingStill = new Message(env, cpAnna, "currentGPSCoordinateChanged",
				new GPSCoordinate(52.37977, 9.72046));
		Message m4AnnaEntered = new Message(env, cpAnna, "currentGPSCoordinateChanged",
				new GPSCoordinate(52.3797, 9.72056));
		Message m5AnnaStillOn = new Message(env, cpAnna, "currentGPSCoordinateChanged",
				new GPSCoordinate(52.37969, 9.72058));
		Message m6AnnaLeft = new Message(env, cpAnna, "currentGPSCoordinateChanged",
				new GPSCoordinate(52.37959, 9.72073));
		Message m7AnnaNothing = new Message(env, cpAnna, "currentGPSCoordinateChanged",
				new GPSCoordinate(52.37953, 9.72084));

		env.setBinding(carToXSystem.getEnv());
		cpJames.setBinding(carToXSystem.getCarControllers().get(0).getCoordinateProcessor());
		cpAnna.setBinding(carToXSystem.getCarControllers().get(1).getCoordinateProcessor());

		runtimeAdapter.receive(m1JamesNothing);
		runtimeAdapter.publish(m1JamesNothing);

		runtimeAdapter.receive(m2JamesApproaching);
		runtimeAdapter.publish(m2JamesApproaching);

		runtimeAdapter.receive(m3JamesEntered);
		runtimeAdapter.publish(m3JamesEntered);

		runtimeAdapter.receive(m4JamesStillOn);
		runtimeAdapter.publish(m4JamesStillOn);

		runtimeAdapter.receive(m5JamesLeft);
		runtimeAdapter.publish(m5JamesLeft);

		runtimeAdapter.receive(m6JamesNothing);
		runtimeAdapter.publish(m6JamesNothing);

		runtimeAdapter.receive(m1AnnaNothing);
		runtimeAdapter.publish(m1AnnaNothing);

		runtimeAdapter.receive(m2AnnaApproaching);
		runtimeAdapter.publish(m2AnnaApproaching);

		runtimeAdapter.receive(m3AnnaApproachingStill);
		runtimeAdapter.publish(m3AnnaApproachingStill);

		runtimeAdapter.receive(m4AnnaEntered);
		runtimeAdapter.publish(m4AnnaEntered);

		runtimeAdapter.receive(m5AnnaStillOn);
		runtimeAdapter.publish(m5AnnaStillOn);

		runtimeAdapter.receive(m6AnnaLeft);
		runtimeAdapter.publish(m6AnnaLeft);

		runtimeAdapter.receive(m7AnnaNothing);
		runtimeAdapter.publish(m7AnnaNothing);

		// Scenario testScenario = new ChangingCoordinatesTestScenario();
		// SpecificationRunconfig.getInstance().runTestScenario(testScenario);

		while (true) {
			Thread.sleep(0);
		}
		// System.out.println("TestChangeGPSCoordinates Ended");

	}

}
