package sbp.examples.gpssimulation;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import sbp.examples.domainmodel.CarToXSystem;
import sbp.examples.domainmodel.CarToXSystemFactory;
import sbp.examples.domainmodel.CoordinatesProcessor;
import sbp.examples.domainmodel.Environment;
import sbp.examples.domainmodel.GPSCoordinate;
import sbp.examples.runconfig.CarToXRunconfig_ObstacleController_TwoCars_LocalTest;
import sbp.examples.util.CalculateInterpolationPoints;
import sbp.runtime.SpecificationRunconfig;
import sbp.runtime.adapter.RuntimeAdapter;
import sbp.specification.events.Message;
import sbp.specification.role.Role;

public class TestTwoCars_InterPoints {

	@Test
	public void test() throws InterruptedException {
		// fail("Not yet implemented");
		System.out.println("TestChangeGPSCoordinates Started");
		// System.out.println("TODO" +
		// Main.carToXSystem.getCarControllers().get(0).getName());
		// fail("Not yet implemented");
		GPSCoordinate streetBeginPoint = new GPSCoordinate(52.37949, 9.72099);
		GPSCoordinate streetEndPoint = new GPSCoordinate(52.37988, 9.72035);
		List<GPSCoordinate> interpolationPoints = 
				CalculateInterpolationPoints.calInterPoints(streetBeginPoint, 
				streetEndPoint, 6);
		
		GPSCoordinate ocBeginPoint = interpolationPoints.get(4);
		GPSCoordinate ocEndPoint = interpolationPoints.get(1);
		CarToXSystem carToXSystem = CarToXSystemFactory.getInstance().getCarToXSystemTwoCars(ocBeginPoint, ocEndPoint);
		// End: Domain model erzeugen

		SpecificationRunconfig.run(CarToXRunconfig_ObstacleController_TwoCars_LocalTest.class);

		RuntimeAdapter runtimeAdapter = SpecificationRunconfig.getInstance().getAdapter();

		Role<Environment> env = new Role<Environment>(Environment.class, "env");
		Role<CoordinatesProcessor> cpJames = new Role<CoordinatesProcessor>(CoordinatesProcessor.class, "cpJames");
		Role<CoordinatesProcessor> cpAnna = new Role<CoordinatesProcessor>(CoordinatesProcessor.class, "cpAnna");

		// carToXSystem.getInstance().getCarControllers().get(0).getNextOC().setBegin(begin);
		// carToXSystem.getInstance().getCarControllers().get(0).getNextOC().setBegin(end);

		List<Message> cpJamesMsgs = new ArrayList<Message>();
		for (int i=0; i<interpolationPoints.size(); i++)
		{
			cpJamesMsgs.add(new Message(env, cpJames, "currentGPSCoordinateChanged", interpolationPoints.get(i)));
		}

		List<Message> cpAnnasMsgs = new ArrayList<Message>();
		//for (i=size-1, i>=0, i--)
		for (int i=interpolationPoints.size()-1; i>=0; i--)
		{
			cpAnnasMsgs.add(new Message(env, cpAnna, "currentGPSCoordinateChanged", interpolationPoints.get(i)));
		}

		env.setBinding(carToXSystem.getEnv());
		cpJames.setBinding(carToXSystem.getCarControllers().get(0).getCoordinateProcessor());
		cpAnna.setBinding(carToXSystem.getCarControllers().get(1).getCoordinateProcessor());

		TestingHelper.publishMessagesInterleaved(runtimeAdapter, cpJamesMsgs, cpAnnasMsgs);

		while (runtimeAdapter.getEventQueueScenario().hasMessageEventInQueue()) {
			Thread.sleep(0);
		}

	}

}
