package sbp.examples.gpssimulation;

import org.junit.Test;

import sbp.examples.domainmodel.CarToXSystem;
import sbp.examples.domainmodel.CarToXSystemFactory;
import sbp.examples.domainmodel.CoordinatesProcessor;
import sbp.examples.domainmodel.Environment;
import sbp.examples.domainmodel.GPSCoordinate;
import sbp.examples.runconfig.CarToXRunconfig_ObstacleController;
import sbp.runtime.SpecificationRunconfig;
import sbp.runtime.adapter.RuntimeAdapter;
import sbp.specification.events.Message;
import sbp.specification.role.Role;

public class TestChangeCPSCoordinatesOtherDirection {

	@Test
	public void test() throws InterruptedException {
		// fail("Not yet implemented");
		System.out.println("TestChangeGPSCoordinatesOtherDirection Started");
		// System.out.println("TODO" +
		// Main.carToXSystem.getCarControllers().get(0).getName());
		// fail("Not yet implemented");
		CarToXSystem carToXSystem = CarToXSystemFactory.getInstance().getCarToXSystemOneCar();
		// End: Domain model erzeugen
		SpecificationRunconfig.run(CarToXRunconfig_ObstacleController.class);

		RuntimeAdapter runtimeAdapter = SpecificationRunconfig.getInstance().getAdapter();

		Role<Environment> env = new Role<Environment>(Environment.class, "env");
		Role<CoordinatesProcessor> cp = new Role<CoordinatesProcessor>(CoordinatesProcessor.class, "cp");

		env.setBinding(carToXSystem.getEnv());
		cp.setBinding(carToXSystem.getCarControllers().get(0).getCoordinateProcessor());

		Message m1 = new Message(env, cp, "currentGPSCoordinateChanged", new GPSCoordinate(52.37985, 9.72032));
		Message m2 = new Message(env, cp, "currentGPSCoordinateChanged", new GPSCoordinate(52.3798, 9.72041));
		Message m3 = new Message(env, cp, "currentGPSCoordinateChanged", new GPSCoordinate(52.37977, 9.72046));
		Message m4 = new Message(env, cp, "currentGPSCoordinateChanged", new GPSCoordinate(52.3797, 9.72056));
		Message m5 = new Message(env, cp, "currentGPSCoordinateChanged", new GPSCoordinate(52.37969, 9.72058));
		Message m6 = new Message(env, cp, "currentGPSCoordinateChanged", new GPSCoordinate(52.37959, 9.72073));
		Message m7 = new Message(env, cp, "currentGPSCoordinateChanged", new GPSCoordinate(52.37953, 9.72084));

		runtimeAdapter.receive(m1);
		runtimeAdapter.publish(m1);

		runtimeAdapter.receive(m2);
		runtimeAdapter.publish(m2);

		runtimeAdapter.receive(m3);
		runtimeAdapter.publish(m3);

		runtimeAdapter.receive(m4);
		runtimeAdapter.publish(m4);

		runtimeAdapter.receive(m5);
		runtimeAdapter.publish(m5);

		runtimeAdapter.receive(m6);
		runtimeAdapter.publish(m6);

		runtimeAdapter.receive(m7);
		runtimeAdapter.publish(m7);

		// Scenario testScenario = new ChangingCoordinatesTestScenario();
		// SpecificationRunconfig.getInstance().runTestScenario(testScenario);

		while (true) {
			Thread.sleep(0);
		}
		// System.out.println("TestChangeGPSCoordinates Ended");

	}

}
