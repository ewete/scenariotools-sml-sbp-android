package sbp.examples.gpssimulation;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestChangeGPSCoordinates.class })
public class AllTests {

}
