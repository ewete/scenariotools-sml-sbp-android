package sbp.examples.gpssimulation;

import cartox.collaborations.cardriving.CarDrivingCollaboration;
import sbp.examples.domainmodel.GPSCoordinate;
import sbp.specification.scenarios.violations.Violation;

@SuppressWarnings("serial")
public class ChangingCoordinatesTestScenario extends CarDrivingCollaboration {

	@Override
	protected void initialisation() {
		// No Initilization

	}

	@Override
	protected void registerRoleBindings() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void registerAlphabet() {
		setBlocked(env, cp, "currentGPSCoordinateChanged");

	}

	@Override
	protected void body() throws Violation {
		// TODO Auto-generated method stub
		// DONT CREATE CarToXSystem.getInstance() like this
		/*
		 * CarToXSystem carToXSystem = CarToXSystem.getInstance();
		 * env.setBinding(carToXSystem.getEnv());
		 * cp.setBinding(carToXSystem.getCarControllers().get(0).
		 * getCoordinateProcessor());
		 */

		request(env, cp, "currentGPSCoordinateChanged", new GPSCoordinate(52.37949, 9.72099));
		// request(env, cp, "currentGPSCoordinateChanged", new
		// GPSCoordinate(52.3796 , 9.72082 ));
	}
}
