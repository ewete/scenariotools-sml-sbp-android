package sbp.examples.gpssimulation;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import sbp.examples.domainmodel.CarToXSystem;
import sbp.examples.domainmodel.CarToXSystemFactory;
import sbp.examples.domainmodel.CoordinatesProcessor;
import sbp.examples.domainmodel.Environment;
import sbp.examples.domainmodel.GPSCoordinate;
import sbp.examples.runconfig.CarToXRunconfig_ObstacleController;
import sbp.runtime.SpecificationRunconfig;
import sbp.runtime.adapter.RuntimeAdapter;
import sbp.specification.events.Message;
import sbp.specification.role.Role;

public class TestChangeGPSCoordinates {

	@Test
	public void test() throws InterruptedException {
		// fail("Not yet implemented");
		System.out.println("TestChangeGPSCoordinates Started");
		// System.out.println("TODO" +
		// Main.carToXSystem.getCarControllers().get(0).getName());
		// fail("Not yet implemented");
		CarToXSystem carToXSystem = CarToXSystemFactory.getInstance().getCarToXSystemOneCar();
		// End: Domain model erzeugen
		SpecificationRunconfig.run(CarToXRunconfig_ObstacleController.class);

		RuntimeAdapter runtimeAdapter = SpecificationRunconfig.getInstance().getAdapter();

		Role<Environment> env = new Role<Environment>(Environment.class, "env");
		Role<CoordinatesProcessor> cp = new Role<CoordinatesProcessor>(CoordinatesProcessor.class, "cpJamesCar");

		env.setBinding(carToXSystem.getEnv());
		cp.setBinding(carToXSystem.getCarControllers().get(0).getCoordinateProcessor());

		Message m1 = new Message(env, cp, "currentGPSCoordinateChanged", new GPSCoordinate(52.37949, 9.72099));
		Message m2 = new Message(env, cp, "currentGPSCoordinateChanged", new GPSCoordinate(52.3796, 9.72082));
		Message m3 = new Message(env, cp, "currentGPSCoordinateChanged", new GPSCoordinate(52.37967, 9.72061));
		Message m4 = new Message(env, cp, "currentGPSCoordinateChanged", new GPSCoordinate(52.37971, 9.72055));
		Message m5 = new Message(env, cp, "currentGPSCoordinateChanged", new GPSCoordinate(52.37983, 9.72044));
		Message m6 = new Message(env, cp, "currentGPSCoordinateChanged", new GPSCoordinate(52.37988, 9.72035));
		// Message end = new Message(env, env, "TESTSCENARIO ENDS NOW");

		List<Message> messages = new ArrayList<Message>();
		messages.add(m1);
		messages.add(m2);
		messages.add(m3);
		messages.add(m4);
		messages.add(m5);
		messages.add(m6);
		// messages.add(end);

		// nur publish? Doppelte Info
		// runtimeAdapter.receive(m1);
		// runtimeAdapter.publish(m1);
		//
		// runtimeAdapter.receive(m2);
		// runtimeAdapter.publish(m2);
		//
		// runtimeAdapter.receive(m3);
		// runtimeAdapter.publish(m3);
		//
		// runtimeAdapter.receive(m4);
		// runtimeAdapter.publish(m4);
		//
		// runtimeAdapter.receive(m5);
		// runtimeAdapter.publish(m5);
		//
		// runtimeAdapter.receive(m6);
		// runtimeAdapter.publish(m6);
		//
		// runtimeAdapter.receive(end);
		// runtimeAdapter.publish(end);
		//
		TestingHelper.publishMessages(runtimeAdapter, messages, carToXSystem);

		// Vernuenftige Bedingung
		while (runtimeAdapter.getEventQueueScenario().hasMessageEventInQueue()) {
			Thread.sleep(0);
		}

		// Scenario testScenario = new ChangingCoordinatesTestScenario();
		// SpecificationRunconfig.getInstance().runTestScenario(testScenario);

		// System.out.println("TestChangeGPSCoordinates Ended");
	}
}
