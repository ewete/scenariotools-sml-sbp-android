package sbp.examples.gpssimulation;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import sbp.examples.domainmodel.CarToXSystem;
import sbp.examples.domainmodel.CarToXSystemFactory;
import sbp.examples.domainmodel.CoordinatesProcessor;
import sbp.examples.domainmodel.Environment;
import sbp.examples.domainmodel.GPSCoordinate;
import sbp.examples.runconfig.CarToXRunconfig_ObstacleController_TwoCars_LocalTest;
import sbp.runtime.SpecificationRunconfig;
import sbp.runtime.adapter.RuntimeAdapter;
import sbp.specification.events.Message;
import sbp.specification.role.Role;

public class TestTwoCars {

	@Test
	public void test() throws InterruptedException {
		// fail("Not yet implemented");
		System.out.println("TestChangeGPSCoordinates Started");
		// System.out.println("TODO" +
		// Main.carToXSystem.getCarControllers().get(0).getName());
		// fail("Not yet implemented");
		
		GPSCoordinate ocBeginPoint = new GPSCoordinate(52.37966, 9.72071);
		GPSCoordinate ocEndPoint = new GPSCoordinate(52.37977, 9.72054);
		CarToXSystem carToXSystem = CarToXSystemFactory.getInstance().getCarToXSystemTwoCars(ocBeginPoint, ocEndPoint);
		// End: Domain model erzeugen

		SpecificationRunconfig.run(CarToXRunconfig_ObstacleController_TwoCars_LocalTest.class);

		RuntimeAdapter runtimeAdapter = SpecificationRunconfig.getInstance().getAdapter();

		Role<Environment> env = new Role<Environment>(Environment.class, "env");
		Role<CoordinatesProcessor> cpJames = new Role<CoordinatesProcessor>(CoordinatesProcessor.class, "cpJames");
		Role<CoordinatesProcessor> cpAnna = new Role<CoordinatesProcessor>(CoordinatesProcessor.class, "cpAnna");

		// carToXSystem.getInstance().getCarControllers().get(0).getNextOC().setBegin(begin);
		// carToXSystem.getInstance().getCarControllers().get(0).getNextOC().setBegin(end);

		List<Message> cpJamesMsgs = new ArrayList<Message>();
		cpJamesMsgs.add(new Message(env, cpJames, "currentGPSCoordinateChanged", new GPSCoordinate(52.37949, 9.72099)));
		cpJamesMsgs.add(new Message(env, cpJames, "currentGPSCoordinateChanged", new GPSCoordinate(52.3796, 9.72082)));
		cpJamesMsgs.add(new Message(env, cpJames, "currentGPSCoordinateChanged", new GPSCoordinate(52.37967, 9.72061)));
		cpJamesMsgs.add(new Message(env, cpJames, "currentGPSCoordinateChanged", new GPSCoordinate(52.37971, 9.72055)));
		cpJamesMsgs.add(new Message(env, cpJames, "currentGPSCoordinateChanged", new GPSCoordinate(52.37983, 9.72044)));
		cpJamesMsgs.add(new Message(env, cpJames, "currentGPSCoordinateChanged", new GPSCoordinate(52.37988, 9.72035)));

		List<Message> cpAnnasMsgs = new ArrayList<Message>();
		cpAnnasMsgs.add(new Message(env, cpAnna, "currentGPSCoordinateChanged", new GPSCoordinate(52.37985, 9.72032)));
		cpAnnasMsgs.add(new Message(env, cpAnna, "currentGPSCoordinateChanged", new GPSCoordinate(52.3798, 9.72041)));
		cpAnnasMsgs.add(new Message(env, cpAnna, "currentGPSCoordinateChanged", new GPSCoordinate(52.37977, 9.72046)));
		cpAnnasMsgs.add(new Message(env, cpAnna, "currentGPSCoordinateChanged", new GPSCoordinate(52.3797, 9.72056)));
		cpAnnasMsgs.add(new Message(env, cpAnna, "currentGPSCoordinateChanged", new GPSCoordinate(52.37969, 9.72058)));
		cpAnnasMsgs.add(new Message(env, cpAnna, "currentGPSCoordinateChanged", new GPSCoordinate(52.37959, 9.72073)));
		cpAnnasMsgs.add(new Message(env, cpAnna, "currentGPSCoordinateChanged", new GPSCoordinate(52.37953, 9.72084)));

		env.setBinding(carToXSystem.getEnv());
		cpJames.setBinding(carToXSystem.getCarControllers().get(0).getCoordinateProcessor());
		cpAnna.setBinding(carToXSystem.getCarControllers().get(1).getCoordinateProcessor());

		TestingHelper.publishMessagesInterleaved(runtimeAdapter, cpJamesMsgs, cpAnnasMsgs);

		while (runtimeAdapter.getEventQueueScenario().hasMessageEventInQueue()) {
			Thread.sleep(0);
		}

	}

}
