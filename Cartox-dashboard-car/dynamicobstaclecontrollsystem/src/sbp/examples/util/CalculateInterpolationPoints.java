package sbp.examples.util;

import java.util.ArrayList;
import java.util.List;

import sbp.examples.domainmodel.GPSCoordinate;

public class CalculateInterpolationPoints {
	/*
	 * input: the begin- and end-point of the test street section input: the
	 * number of points which is wanted to be generated between the begin- and
	 * end-point
	 * 
	 * the distance between every two points in the neighbourhood is the same.
	 */
	public static List<GPSCoordinate> calInterPoints(GPSCoordinate beginStreetPoint, GPSCoordinate endStreetPoint,
			int numPoints) {
		double lat_1 = beginStreetPoint.getLatitudeDegree();
		double long_1 = beginStreetPoint.getLongitudeDegree();

		double lat_2 = endStreetPoint.getLatitudeDegree();
		double long_2 = endStreetPoint.getLongitudeDegree();

		double unitDistanceLAT = (lat_2 - lat_1) / (numPoints + 1);
		double unitDistanceLONG = (long_2 - long_1) / (numPoints + 1);

		List<GPSCoordinate> interpolationPoints = new ArrayList<GPSCoordinate>();
		double lat_i, long_i;
		for (int i = 1; i <= numPoints; i++) {
			lat_i = lat_1 + unitDistanceLAT * i;
			long_i = long_1 + unitDistanceLONG * i;
			interpolationPoints.add(new GPSCoordinate(long_i, lat_i));
		}
		return interpolationPoints;
	}
}
