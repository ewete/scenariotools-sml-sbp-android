package sbp.examples.util;

import java.util.List;

import org.junit.Test;

import sbp.examples.domainmodel.GPSCoordinate;

public class TestCalInterPoints {

	@Test
	public void test() {
		GPSCoordinate beginStreetPoint = new GPSCoordinate(52.37949, 9.72099);
		GPSCoordinate endStreetPoint = new GPSCoordinate(52.37988, 9.72035);
		List<GPSCoordinate> testArraylist = CalculateInterpolationPoints.calInterPoints(beginStreetPoint,
				endStreetPoint, 2);
		for (int i = 0; i < testArraylist.size(); i++) {
			GPSCoordinate point = testArraylist.get(i);
			System.out.println(point.getLongitudeDegree() + " " + point.getLatitudeDegree() + '\n');
		}
	}

}
