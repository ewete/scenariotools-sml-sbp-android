package sbp.examples.util;

public class HaversineAlgorithm {
	/*
	 * PLEASE NOTE: THE ACCURACY IS NOT RELIABLE!
	 * More accurate methods that consider the Earth's 
	 * ellipticity are given by Vincenty's formulae
	 * 
	 * ONE MORE SOLUTION
	 * to use location APIs 
	 * */

	static final double _eQuatorialEarthRadius = 6378.1370D;
	static final double _d2r = (Math.PI / 180D);

	public static int haversineInM(double lat1, double long1, double lat2, double long2) {
		return (int) (1000D * haversineInKM(lat1, long1, lat2, long2));
	}

	public static double haversineInKM(double lat1, double long1, double lat2, double long2) {
		double dlong = (long2 - long1) * _d2r;
		double dlat = (lat2 - lat1) * _d2r;
		double a = Math.pow(Math.sin(dlat / 2D), 2D)
				+ Math.cos(lat1 * _d2r) * Math.cos(lat2 * _d2r) * Math.pow(Math.sin(dlong / 2D), 2D);
		double c = 2D * Math.atan2(Math.sqrt(a), Math.sqrt(1D - a));
		double d = _eQuatorialEarthRadius * c;

		return d;
	}

	public static void main(String args[]) {
		double lat1, long1, lat2, long2;
		
		
		// inputs are all DECIMAL coordinates ( IN DEGREES )
		// West and South locations are NEGATIVE
		// For example, we calculate the distance
		// between London and Sydney
		// London 51 degree 30' 0" N , 0 degree 0' 0"E
		// DECIMAL 51.5 , 0
		//
		// Sydney 33 degree 51' 5" S , 151 degree 12'34"E
		// DECIMAL -33.865 , 151.209444
		//
		// then the inputs should be as follows:
		// lat1 = 51.5;
		// long1 = 0;
		// lat2 = -33.865;
		// long2 = 151.209444;
		// the output is in kilometre

		lat1 = 52.37433333333333;
		long1 = 9.721333333333332;

		lat2 = 52.37433333333333;
		long2 = 9.721499999999999;

		double result = haversineInKM(lat1, long1, lat2, long2);

		System.out.println(result);
	}

}