package sbp.examples.networkadapter;

import java.io.UnsupportedEncodingException;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import sbp.examples.domainmodel.CarToXSystem;
import sbp.runtime.ObjectRegistry;
import sbp.runtime.SpecificationRunconfig;
import sbp.runtime.adapter.AbstractRuntimeAdapter;
import sbp.runtime.network.exceptions.ClientByeException;
import sbp.runtime.network.messages.Messages;
import sbp.runtime.settings.Settings;
import sbp.specification.events.Message;

public class MQTTDistributedRuntimeAdapter extends AbstractRuntimeAdapter {

	private CarToXSystem carToXSystem;

	private String clientId;
	private MemoryPersistence persistenceSenderMqttClient;
	private MemoryPersistence persistenceReceiverMqttClient;

	private MqttClient senderMqttClient;
	private MqttClient receiverMqttClient;

	public MQTTDistributedRuntimeAdapter(SpecificationRunconfig<?> specificationRunconfig) {
		super(specificationRunconfig);
		clientId = MqttClient.generateClientId();
	}

	@Override
	public void run() {
		startServer();
		connectClients();
	}

	@Override
	public void startServer() {
		try {
			final MqttConnectOptions connOpts = new MqttConnectOptions();
			connOpts.setCleanSession(true);

			persistenceReceiverMqttClient = new MemoryPersistence();
			receiverMqttClient = new MqttClient(IPSettings.BROCKER_URL_CONNECTION, clientId + "_Receiver",
					persistenceReceiverMqttClient);
			receiverMqttClient.setCallback(new MqttCallback() {

				@Override
				public void connectionLost(Throwable throwable) {
					Settings.NETWORK_OUT.println("receiverMqttClient connection Lost " + throwable.getMessage());
					throwable.printStackTrace();
					try {
						connOpts.setCleanSession(false);
						receiverMqttClient.connect(connOpts);
					} catch (MqttSecurityException e) {
						e.printStackTrace();
					} catch (MqttException e) {
						e.printStackTrace();
					}
				}

				@Override
				public void messageArrived(String t, MqttMessage m) throws Exception {
					String messageString = new String(m.getPayload(), "UTF-8");
					if (messageString.equals(Messages.BYE.toString())) {
						throw new ClientByeException();
					} else {
						Message messageEvent = deserializeForNetwork(messageString);
						receive(messageEvent);
					}
				}

				@Override
				public void deliveryComplete(IMqttDeliveryToken t) {
				}
			});
			receiverMqttClient.connect(connOpts);
			Settings.NETWORK_OUT
					.println("receiverMqttClient connected to broker : " + IPSettings.BROCKER_URL_CONNECTION);
			receiverMqttClient.subscribe(IPSettings.TOPIC, IPSettings.QOS);
			Settings.NETWORK_OUT.println("receiverMqttClient subscribed to topic : " + IPSettings.TOPIC);
		} catch (MqttException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void connectClients() {
		try {
			final MqttConnectOptions connOpts = new MqttConnectOptions();
			connOpts.setCleanSession(true);

			persistenceSenderMqttClient = new MemoryPersistence();
			senderMqttClient = new MqttClient(IPSettings.BROCKER_URL_CONNECTION, clientId + "_Sender",
					persistenceSenderMqttClient);
			senderMqttClient.setCallback(new MqttCallback() {
				@Override
				public void connectionLost(Throwable throwable) {
					Settings.NETWORK_OUT.println("senderMqttClient connection Lost " + throwable.getMessage());
					throwable.printStackTrace();
					try {
						connOpts.setCleanSession(false);
						senderMqttClient.connect(connOpts);
					} catch (MqttSecurityException e) {
						e.printStackTrace();
					} catch (MqttException e) {
						e.printStackTrace();
					}
				}

				@Override
				public void messageArrived(String t, MqttMessage m) throws Exception {
				}

				@Override
				public void deliveryComplete(IMqttDeliveryToken t) {
					Settings.NETWORK_OUT.println("senderMqttClient's message delivered with id " + t.getMessageId());
				}
			});
			senderMqttClient.connect(connOpts);
			Settings.NETWORK_OUT.println("senderMqttClient connected to broker: " + IPSettings.BROCKER_URL_CONNECTION);
		} catch (MqttException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void publish(Message event) {
		try {
			MqttMessage message = new MqttMessage(serializeForNetwork(event).getBytes("UTF-8"));
			message.setQos(IPSettings.QOS);
			senderMqttClient.publish(IPSettings.TOPIC, message);
		} catch (MqttPersistenceException e) {
			e.printStackTrace();
		} catch (MqttException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void receive(Message event) {
		if (ObjectRegistry.getInstance().isControllable(event.getSender().getBinding())) {
			final String message = String.format("Ignoring (Already enqueued) : %s", event.getName());
			Settings.NETWORK_OUT.println(message);
		} else {
			getEventQueueScenario().enqueueEvent(event);
		}
	}

	private String serializeForNetwork(Message event) {
		// Settings.NETWORK_OUT.println("Serialize : " + event);
		String out = ObjectsTypeAdapter.getInstance(carToXSystem).getGsonBuilder().create().toJson(event,
				Message.class);
		// Settings.NETWORK_OUT.println("Serialized to : " + out);
		// Settings.NETWORK_OUT.println("--- End serialization ---");

		// printToFile("serializeForNetwork", event, out);
		return out;
	}

	private Message deserializeForNetwork(String event) {
		// Settings.NETWORK_OUT.println("deserialize : " + event);
		Message out = ObjectsTypeAdapter.getInstance(carToXSystem).getGsonBuilder().create().fromJson(event,
				Message.class);
		// Settings.NETWORK_OUT.println("deserialized to : " + out);
		// Settings.NETWORK_OUT.println("--- End deserialization ---");

		// printToFile("deserializeForNetwork", out, event);
		return out;
	}

	public void setCarToXSystem(CarToXSystem carToXSystem) {
		this.carToXSystem = carToXSystem;
	}
}
