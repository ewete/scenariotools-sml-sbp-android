package sbp.examples.networkadapter;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import sbp.examples.domainmodel.CarController;
import sbp.examples.domainmodel.CarDashboard;
import sbp.examples.domainmodel.CarToXSystem;
import sbp.examples.domainmodel.CoordinatesProcessor;
import sbp.examples.domainmodel.Environment;
import sbp.examples.domainmodel.GPSCoordinate;
import sbp.examples.domainmodel.ObstacleController;
import sbp.examples.domainmodel.StreetSectionController;
import sbp.runtime.ObjectRegistry;
import sbp.runtime.settings.Settings;
import sbp.specification.events.Message;
import sbp.specification.role.Role;

public class ObjectsTypeAdapter {

	private CarToXSystem carToXSystem;
	private static ObjectsTypeAdapter instance;

	private GsonBuilder gsonBuilder;

	private ObjectsTypeAdapter() {
		gsonBuilder = new GsonBuilder().registerTypeAdapter(Message.class, new MessageAdapter())
				.registerTypeAdapter(Role.class, new RoleAdapter()).registerTypeAdapter(Class.class, new ClassAdapter())
				.registerTypeAdapter(Environment.class, new EnvironmentAdapter())
				.registerTypeAdapter(CarController.class, new CarAdapter())
				.registerTypeAdapter(CarDashboard.class, new DashboardAdapter())
				.registerTypeAdapter(ObstacleController.class, new ObstacleControlAdapter())
				.registerTypeAdapter(CoordinatesProcessor.class, new CoordinatesProcessorAdapter())
				.registerTypeAdapter(GPSCoordinate.class, new GPSCoordinateAdapter())
				.registerTypeAdapter(List.class, new ListAdapter()).setPrettyPrinting().serializeNulls();
	}

	public void setCarToXSystem(CarToXSystem carToXSystem) {
		this.carToXSystem = carToXSystem;
	}

	public static ObjectsTypeAdapter getInstance(CarToXSystem carToXSystem) {
		if (instance == null) {
			instance = new ObjectsTypeAdapter();
			instance.setCarToXSystem(carToXSystem);
		}
		return instance;
	}

	public GsonBuilder getGsonBuilder() {
		return gsonBuilder;
	}

	private class MessageAdapter implements JsonDeserializer<Message>, JsonSerializer<Message> {

		@Override
		public JsonElement serialize(final Message message, final Type typeOfSrc,
				final JsonSerializationContext context) {

			final JsonObject jsonObject = new JsonObject();

			jsonObject.addProperty("message", message.getMessage());

			final JsonElement jsonSender = context.serialize(message.getSender(), Role.class);
			jsonObject.add("sender", jsonSender);

			final JsonElement jsonReceiver = context.serialize(message.getReceiver(), Role.class);
			jsonObject.add("receiver", jsonReceiver);

			if (!message.getParameters().isEmpty()) {
				final JsonElement jsonParameters = context.serialize(message.getParameters(), List.class);
				jsonObject.add("parameters", jsonParameters);
			}

			// jsonObject.addProperty("strict", message.isStrict());
			// jsonObject.addProperty("requested", message.isRequested());
			// jsonObject.addProperty("initializing", message.isInitializing());

			return jsonObject;
		}

		@Override
		public Message deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context)
				throws JsonParseException {

			final JsonObject jsonObject = json.getAsJsonObject();

			String messageString = jsonObject.get("message").getAsString();

			final JsonElement jsonSender = jsonObject.get("sender");
			Role sender = context.deserialize(jsonSender, Role.class);

			final JsonElement jsonReceiver = jsonObject.get("receiver");
			Role receiver = context.deserialize(jsonReceiver, Role.class);

			Message message = new Message(sender, receiver, messageString);

			if (jsonObject.has("parameters")) {
				final JsonElement jsonParameters = jsonObject.get("parameters");
				List<Object> parameters = context.deserialize(jsonParameters, List.class);
				message.getParameters().addAll(parameters);
			}

			// message.setStrict(jsonObject.get("strict").getAsBoolean());
			// message.setRequested(jsonObject.get("requested").getAsBoolean());
			// message.setInitializing(jsonObject.get("initializing").getAsBoolean());
			return message;
		}
	}

	private class RoleAdapter implements JsonDeserializer<Role<?>>, JsonSerializer<Role<?>> {

		@Override
		public JsonElement serialize(final Role<?> role, final Type typeOfSrc, final JsonSerializationContext context) {

			final JsonObject jsonObject = new JsonObject();

			jsonObject.addProperty("name", role.getName());

			final JsonElement jsonType = context.serialize(role.getType(), Class.class);
			jsonObject.add("type", jsonType);

			JsonElement jsonBinding = null;
			if (role.hasBinding()) {
				if (role.getBinding() instanceof Environment) {
					jsonBinding = context.serialize(role.getBinding(), Environment.class);
				} else if (role.getBinding() instanceof CarController) {
					jsonBinding = context.serialize(role.getBinding(), CarController.class);
				} else if (role.getBinding() instanceof CarDashboard) {
					jsonBinding = context.serialize(role.getBinding(), CarDashboard.class);
				} else if (role.getBinding() instanceof ObstacleController) {
					jsonBinding = context.serialize(role.getBinding(), ObstacleController.class);
				} else if (role.getBinding() instanceof CoordinatesProcessor) {
					jsonBinding = context.serialize(role.getBinding(), CoordinatesProcessor.class);
				}
				if (jsonBinding != null) {
					jsonObject.add("binding", jsonBinding);
				} else {
					Settings.NETWORK_OUT
							.println("RoleAdapter : Serialisation of " + role.getBinding() + " not supported");
				}
			}

			return jsonObject;
		}

		@Override
		public Role<?> deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context)
				throws JsonParseException {

			final JsonObject jsonObject = json.getAsJsonObject();

			String name = jsonObject.get("name").getAsString();

			final JsonElement jsonType = jsonObject.get("type");
			Class type = context.deserialize(jsonType, Class.class);

			Role role = new Role(type, name);

			if (jsonObject.has("binding")) {
				final JsonElement jsonBinding = jsonObject.get("binding");
				if (type.equals(Environment.class)) {
					Environment binding = context.deserialize(jsonBinding, Environment.class);
					role.setBinding(binding);
				} else if (type.equals(CarController.class)) {
					CarController binding = context.deserialize(jsonBinding, CarController.class);
					role.setBinding(binding);
				} else if (type.equals(CarDashboard.class)) {
					CarDashboard binding = context.deserialize(jsonBinding, CarDashboard.class);
					role.setBinding(binding);
				} else if (type.equals(ObstacleController.class)) {
					ObstacleController binding = context.deserialize(jsonBinding, ObstacleController.class);
					role.setBinding(binding);
				} else if (type.equals(CoordinatesProcessor.class)) {
					CoordinatesProcessor binding = context.deserialize(jsonBinding, CoordinatesProcessor.class);
					role.setBinding(binding);
				} else {
					Settings.NETWORK_OUT.println("RoleAdapter : Deserialisation of " + jsonBinding + " not supported");
				}
			}

			return role;
		}
	}

	private class ClassAdapter implements JsonDeserializer<Class<?>>, JsonSerializer<Class<?>> {

		@Override
		public JsonElement serialize(final Class<?> roleType, final Type typeOfSrc,
				final JsonSerializationContext context) {

			final JsonObject jsonObject = new JsonObject();

			jsonObject.addProperty("name", roleType.getName());

			return jsonObject;
		}

		@Override
		public Class<?> deserialize(final JsonElement json, final Type typeOfT,
				final JsonDeserializationContext context) throws JsonParseException {

			final JsonObject jsonObject = json.getAsJsonObject();

			final String typeName = jsonObject.get("name").getAsString();

			Class class1 = null;
			try {
				class1 = Class.forName(typeName);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			if (class1 == null) {
				Settings.NETWORK_OUT.println("ClassAdapter : Deserialisation of " + json + " not supported");
			}
			return class1;
		}
	}

	private class ListAdapter implements JsonDeserializer<List<?>>, JsonSerializer<List<?>> {

		@Override
		public JsonElement serialize(final List<?> parameters, final Type typeOfSrc,
				final JsonSerializationContext context) {

			final JsonArray jsonArray = new JsonArray();

			for (Object param : parameters) {
				Integer objecIDInRegistry = ObjectRegistry.getInstance().getID(param);
				if (objecIDInRegistry == null || objecIDInRegistry < 0) {
					// param not registered
					if (param.getClass().isPrimitive() || isWrapperType(param.getClass())) {
						final JsonObject jsonObject = new JsonObject();
						jsonObject.addProperty("type", param.getClass().getName());
						jsonObject.add("value", new JsonPrimitive(param.toString()));
						jsonArray.add(jsonObject);
					} else if (param.getClass().equals(GPSCoordinate.class)) {
						final JsonObject jsonObject = new JsonObject();
						jsonObject.addProperty("type", param.getClass().getName());
						final JsonElement jsonGPSCoordinate = context.serialize(param, GPSCoordinate.class);
						jsonObject.add("value", jsonGPSCoordinate);
						jsonArray.add(jsonObject);
					} else {
						Settings.NETWORK_OUT.println("ListAdapter : Parameter type not supported for serialization "
								+ param + ". It is neither a registered object nor a primitive type.");
					}
				} else {
					// param registered
					JsonObject jsonObject = new JsonObject();
					jsonObject.addProperty("type", param.getClass().getName());
					jsonObject.addProperty("value", objecIDInRegistry);
					jsonArray.add(jsonObject);
				}
			}

			return jsonArray;
		}

		@Override
		public List<?> deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context)
				throws JsonParseException {

			final JsonArray jsonArray = json.getAsJsonArray();
			final List<Object> parameters = new ArrayList<Object>();
			for (JsonElement jsonElement : jsonArray) {
				JsonObject jsonObject = jsonElement.getAsJsonObject();
				String type = jsonObject.get("type").getAsString();

				try {
					Class<?> clazz = Class.forName(type);
					if (clazz.isPrimitive() || isWrapperType(clazz)) {
						Constructor<?> ctor = clazz.getConstructor(String.class);
						String value = jsonObject.get("value").getAsJsonPrimitive().getAsString();
						Object object = ctor.newInstance(new Object[] { value });
						parameters.add(object);
					} else {
						JsonElement jsonElementValue = jsonObject.get("value");
						String value = null;
						if (jsonElementValue.isJsonPrimitive()) {
							value = jsonObject.get("value").getAsString();
						}
						Integer id = (value == null) ? -1 : new Integer(value);
						Object object = ObjectRegistry.getInstance().getObject((id));
						if (object != null) {
							parameters.add(object);
						} else if (type.equals(GPSCoordinate.class.getName())) {
							final GPSCoordinate gPSCoordinate = context.deserialize(jsonObject.get("value"),
									GPSCoordinate.class);
							parameters.add(gPSCoordinate);
						} else {
							Settings.NETWORK_OUT.println(
									String.format("ListAdapter : Object with id %s not found for deserialization of %s",
											id, jsonObject));
						}
					}
				} catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException
						| IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
				}
			}

			return parameters;
		}
	}

	private class EnvironmentAdapter implements JsonDeserializer<Environment>, JsonSerializer<Environment> {

		@Override
		public JsonElement serialize(final Environment environment, final Type typeOfSrc,
				final JsonSerializationContext context) {

			final JsonObject jsonObject = new JsonObject();

			jsonObject.addProperty("name", environment.getName());

			return jsonObject;
		}

		@Override
		public Environment deserialize(final JsonElement json, final Type typeOfT,
				final JsonDeserializationContext context) throws JsonParseException {

			final JsonObject jsonObject = json.getAsJsonObject();

			final String envName = jsonObject.get("name").getAsString();

			Environment env = carToXSystem.getEnv();

			if (env.getName().equals(envName)) {
				return env;
			} else {
				Settings.NETWORK_OUT.println("Environment deserialize : ERRRRRRRRRRrrrrrrrrrroRRRRRRRR");
				return null;
			}
		}
	}

	private class CarAdapter implements JsonDeserializer<CarController>, JsonSerializer<CarController> {

		@Override
		public JsonElement serialize(final CarController car, final Type typeOfSrc,
				final JsonSerializationContext context) {

			final JsonObject jsonObject = new JsonObject();

			jsonObject.addProperty("name", car.getName());

			return jsonObject;
		}

		@Override
		public CarController deserialize(final JsonElement json, final Type typeOfT,
				final JsonDeserializationContext context) throws JsonParseException {

			final JsonObject jsonObject = json.getAsJsonObject();

			final String carName = jsonObject.get("name").getAsString();

			CarController carOut = null;
			for (CarController car : carToXSystem.getCarControllers()) {
				if (car.getName().equals(carName)) {
					carOut = car;
					break;
				}
			}

			if (carOut != null) {
				return carOut;
			} else {
				Settings.NETWORK_OUT.println("Car deserialize : ERRRRRRRRRRrrrrrrrrrroRRRRRRRR");
				return null;
			}
		}
	}

	private class DashboardAdapter implements JsonDeserializer<CarDashboard>, JsonSerializer<CarDashboard> {

		@Override
		public JsonElement serialize(final CarDashboard dashboard, final Type typeOfSrc,
				final JsonSerializationContext context) {

			final JsonObject jsonObject = new JsonObject();

			jsonObject.addProperty("name", dashboard.getName());

			return jsonObject;
		}

		@Override
		public CarDashboard deserialize(final JsonElement json, final Type typeOfT,
				final JsonDeserializationContext context) throws JsonParseException {

			final JsonObject jsonObject = json.getAsJsonObject();

			final String dashboardName = jsonObject.get("name").getAsString();

			CarDashboard dashboardOut = null;
			for (CarController car : carToXSystem.getCarControllers()) {
				if (car.getDashboard().getName().equals(dashboardName)) {
					dashboardOut = car.getDashboard();
					break;
				}
			}

			if (dashboardOut != null) {
				return dashboardOut;
			} else {
				Settings.NETWORK_OUT.println("Dashboard deserialize : ERRRRRRRRRRrrrrrrrrrroRRRRRRRR");
				return null;
			}
		}
	}

	private class ObstacleControlAdapter
			implements JsonDeserializer<ObstacleController>, JsonSerializer<ObstacleController> {

		@Override
		public JsonElement serialize(final ObstacleController obstacleControl, final Type typeOfSrc,
				final JsonSerializationContext context) {

			final JsonObject jsonObject = new JsonObject();

			jsonObject.addProperty("name", obstacleControl.getName());

			return jsonObject;
		}

		@Override
		public ObstacleController deserialize(final JsonElement json, final Type typeOfT,
				final JsonDeserializationContext context) throws JsonParseException {

			final JsonObject jsonObject = json.getAsJsonObject();

			final String obstacleName = jsonObject.get("name").getAsString();

			ObstacleController obstacleControlOut = null;
			for (StreetSectionController ssc : carToXSystem.getStreetSectionControllers()) {
				for (ObstacleController obstacleControl : ssc.getOc()) {
					if (obstacleControl.getName().equals(obstacleName)) {
						obstacleControlOut = obstacleControl;
						break;
					}
				}
				if (obstacleControlOut != null) {
					break;
				}
			}

			if (obstacleControlOut != null) {
				return obstacleControlOut;
			} else {
				Settings.NETWORK_OUT.println("ObstacleControl deserialize : ERRRRRRRRRRrrrrrrrrrroRRRRRRRR");
				return null;
			}
		}
	}

	private class GPSCoordinateAdapter implements JsonDeserializer<GPSCoordinate>, JsonSerializer<GPSCoordinate> {

		@Override
		public JsonElement serialize(final GPSCoordinate gPSCoordinate, final Type typeOfSrc,
				final JsonSerializationContext context) {

			final JsonObject jsonObject = new JsonObject();

			jsonObject.add("longitude", new JsonPrimitive(gPSCoordinate.getLongitudeDegree()));
			jsonObject.add("latitude", new JsonPrimitive(gPSCoordinate.getLatitudeDegree()));

			return jsonObject;
		}

		@Override
		public GPSCoordinate deserialize(final JsonElement json, final Type typeOfT,
				final JsonDeserializationContext context) throws JsonParseException {

			final JsonObject jsonObject = json.getAsJsonObject();

			final double longitude = jsonObject.get("longitude").getAsJsonPrimitive().getAsDouble();
			final double latitude = jsonObject.get("latitude").getAsJsonPrimitive().getAsDouble();

			return new GPSCoordinate(longitude, latitude);
		}
	}

	private class CoordinatesProcessorAdapter
			implements JsonDeserializer<CoordinatesProcessor>, JsonSerializer<CoordinatesProcessor> {

		@Override
		public JsonElement serialize(final CoordinatesProcessor coordinatesProcessor, final Type typeOfSrc,
				final JsonSerializationContext context) {

			final JsonObject jsonObject = new JsonObject();

			jsonObject.addProperty("name", coordinatesProcessor.getName());

			return jsonObject;
		}

		@Override
		public CoordinatesProcessor deserialize(final JsonElement json, final Type typeOfT,
				final JsonDeserializationContext context) throws JsonParseException {

			final JsonObject jsonObject = json.getAsJsonObject();

			final String coordinatesProcessorName = jsonObject.get("name").getAsString();

			CoordinatesProcessor coordinatesProcessorOut = null;
			for (CarController carController : carToXSystem.getCarControllers()) {
				if (carController.getCoordinateProcessor().getName().equals(coordinatesProcessorName)) {
					coordinatesProcessorOut = carController.getCoordinateProcessor();
					break;
				}
			}

			if (coordinatesProcessorOut != null) {
				return coordinatesProcessorOut;
			} else {
				Settings.NETWORK_OUT.println("CoordinatesProcessor deserialize : ERRRRRRRRRRrrrrrrrrrroRRRRRRRR");
				return null;
			}
		}
	}

	private static Set<Class<?>> getWrapperTypes() {
		Set<Class<?>> ret = new HashSet<Class<?>>();
		ret.add(Boolean.class);
		ret.add(Character.class);
		ret.add(Byte.class);
		ret.add(Short.class);
		ret.add(Integer.class);
		ret.add(Long.class);
		ret.add(Float.class);
		ret.add(Double.class);
		ret.add(Void.class);
		return ret;
	}

	private static final Set<Class<?>> WRAPPER_TYPES = getWrapperTypes();

	private static boolean isWrapperType(Class<?> clazz) {
		return WRAPPER_TYPES.contains(clazz);
	}

}
