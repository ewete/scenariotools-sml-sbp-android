package sbp.examples.runtime;

import sbp.examples.domainmodel.CarToXSystem;
import sbp.examples.domainmodel.CarToXSystemFactory;
import sbp.examples.domainmodel.GPSCoordinate;

public class CarToXSystemWrapper {

    private static CarToXSystem carToXSystemTwoCars;

    public static CarToXSystem getCarToXSystemTowCars() {
        if (carToXSystemTwoCars == null) {
            // Test: Nienburger Str.
            //GPSCoordinate beginOc = new GPSCoordinate(9.7206876, 52.3796598);
            //GPSCoordinate endOc = new GPSCoordinate(9.7205092, 52.3797707);
            // Test oldest
            // GPSCoordinate beginOc = new GPSCoordinate(9.72071, 52.37966);
            // GPSCoordinate endOc = new GPSCoordinate(9.72054, 52.37977);
            // LUH
            GPSCoordinate beginOc = new GPSCoordinate(9.717617, 52.382263);
            GPSCoordinate endOc = new GPSCoordinate(9.717879, 52.382361);
            // Home
            // GPSCoordinate beginOc = new GPSCoordinate(9.769653, 52.362920);
            // GPSCoordinate endOc = new GPSCoordinate(9.769625, 52.362558);
            carToXSystemTwoCars = CarToXSystemFactory.getInstance().getCarToXSystemTwoCars(beginOc, endOc);
        }
        return carToXSystemTwoCars;
    }
}
