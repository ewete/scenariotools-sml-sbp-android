package sbp.examples.runtime;

import sbp.examples.domainmodel.CarToXSystem;
import sbp.examples.domainmodel.CarToXSystemFactory;
import sbp.examples.domainmodel.GPSCoordinate;
import sbp.examples.runconfig.CarToXRunconfig_Annas_TwoCars;
import sbp.runtime.SpecificationRunconfig;

public class MainAnnasDashboard_TwoCars {

	private static CarToXSystem carToXSystemTwoCars;

	public static void main(String[] args) {
		// Begin: Domain model erzeugen
		carToXSystemTwoCars = getCarToXSystemTwoCars();
		// End: Domain model erzeugen
		SpecificationRunconfig.run(CarToXRunconfig_Annas_TwoCars.class);
	}

	public static CarToXSystem getCarToXSystemTwoCars() {
		if (carToXSystemTwoCars == null) {
			carToXSystemTwoCars = CarToXSystemFactory.getInstance().getCarToXSystemTwoCars(new GPSCoordinate(52.37966, 9.72071), new GPSCoordinate(52.37977, 9.72054));
		}
		return carToXSystemTwoCars;
	}
}
