package sbp.examples.runtime;

import sbp.examples.domainmodel.CarToXSystem;
import sbp.examples.domainmodel.CarToXSystemFactory;
import sbp.examples.runconfig.CarToXRunconfig_ObstacleController;
import sbp.runtime.SpecificationRunconfig;
import sbp.runtime.adapter.RuntimeAdapter;

public class MainObstacleController {

	private static CarToXSystem carToXSystem;

	private static RuntimeAdapter runtimeAdapter; // FOR TESTING REASONS

	public static void main(String[] args) {
		// Begin: Domain model erzeugen
		carToXSystem = getCarToXSystem();
		// End: Domain model erzeugen
		SpecificationRunconfig.run(CarToXRunconfig_ObstacleController.class);
		runtimeAdapter = SpecificationRunconfig.getInstance().getAdapter(); // FOR
																			// TESTING
																			// REASONS
	}

	public static CarToXSystem getCarToXSystem() {
		if (carToXSystem == null) {
			carToXSystem = CarToXSystemFactory.getInstance().getCarToXSystemOneCar();
		}
		return carToXSystem;
	}

	public static RuntimeAdapter getRuntimeAdapter() {
		return runtimeAdapter;
	}
}
