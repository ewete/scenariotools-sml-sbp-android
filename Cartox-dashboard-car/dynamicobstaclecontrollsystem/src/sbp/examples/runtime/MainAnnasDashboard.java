package sbp.examples.runtime;

import sbp.examples.domainmodel.CarToXSystem;
import sbp.examples.domainmodel.CarToXSystemFactory;
import sbp.examples.runconfig.CarToXRunconfig_AnnasDashboard;
import sbp.runtime.SpecificationRunconfig;

public class MainAnnasDashboard {

	private static CarToXSystem carToXSystem;

	public static void main(String[] args) {
		// Begin: Domain model erzeugen
		carToXSystem = getCarToXSystem();
		// End: Domain model erzeugen
		SpecificationRunconfig.run(CarToXRunconfig_AnnasDashboard.class);
	}

	public static CarToXSystem getCarToXSystem() {
		if (carToXSystem == null) {
			carToXSystem = CarToXSystemFactory.getInstance().getCarToXSystemOneCar();
		}
		return carToXSystem;
	}
}
