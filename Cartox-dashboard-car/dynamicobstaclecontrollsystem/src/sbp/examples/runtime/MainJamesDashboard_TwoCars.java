package sbp.examples.runtime;

import sbp.examples.domainmodel.CarToXSystem;
import sbp.examples.domainmodel.CarToXSystemFactory;
import sbp.examples.domainmodel.GPSCoordinate;
import sbp.examples.runconfig.CarToXRunconfig_James_TwoCars;
import sbp.runtime.SpecificationRunconfig;

public class MainJamesDashboard_TwoCars {

	private static CarToXSystem carToXSystemTwoCars;

	public static void main(String[] args) {
		// Begin: Domain model erzeugen
		carToXSystemTwoCars = getCarToXSystemTowCars();
		// End: Domain model erzeugen
		SpecificationRunconfig.run(CarToXRunconfig_James_TwoCars.class);
	}

	public static CarToXSystem getCarToXSystemTowCars() {
		if (carToXSystemTwoCars == null) {
			carToXSystemTwoCars = CarToXSystemFactory.getInstance().getCarToXSystemTwoCars(new GPSCoordinate(52.37966, 9.72071), new GPSCoordinate(52.37977, 9.72054));
		}
		return carToXSystemTwoCars;
	}
}
