package sbp.examples.ui.simulation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import sbp.examples.domainmodel.CarController;
import sbp.examples.domainmodel.CoordinatesProcessor;
import sbp.examples.domainmodel.Environment;
import sbp.examples.domainmodel.GPSCoordinate;
import sbp.runtime.adapter.RuntimeAdapter;
import sbp.runtime.settings.Settings;
import sbp.specification.events.Message;
import sbp.specification.role.Role;

public class CarToXSimulationFrameListener implements ActionListener {

	private static CarToXSimulationFrameListener instance;
	private RuntimeAdapter runtimeAdapter;
	private CarController carController;

	private List<GPSCoordinate> interpolatedPoints;
	private int currentPosition = -1;

	private CarToXSimulationFrameListener() {
		this.interpolatedPoints = new ArrayList<>();
		this.interpolatedPoints.add(new GPSCoordinate(52.37949, 9.72099));
		this.interpolatedPoints.add(new GPSCoordinate(52.3796, 9.72082));
		
		this.interpolatedPoints.add(new GPSCoordinate(52.37966, 9.72072));
		this.interpolatedPoints.add(new GPSCoordinate(52.37966, 9.72071));
		
		this.interpolatedPoints.add(new GPSCoordinate(52.37967, 9.72061));
		this.interpolatedPoints.add(new GPSCoordinate(52.37971, 9.72055));
		
		this.interpolatedPoints.add(new GPSCoordinate(52.37977, 9.72054));
		this.interpolatedPoints.add(new GPSCoordinate(52.37978, 9.72053));
		
		this.interpolatedPoints.add(new GPSCoordinate(52.37983, 9.72044));
		this.interpolatedPoints.add(new GPSCoordinate(52.37988, 9.72035));
	}

	public static CarToXSimulationFrameListener getInstance() {
		if (instance == null) {
			instance = new CarToXSimulationFrameListener();
		}
		return instance;
	}

	private void currentGPSCoordinateChangedCar() {
		Role<Environment> environmentRole = new Role<Environment>(Environment.class, "env");
		Role<CoordinatesProcessor> coordinatesProcessorRole = new Role<CoordinatesProcessor>(CoordinatesProcessor.class,
				"cp");

		environmentRole.setBinding(carController.getEnv());
		CoordinatesProcessor coordinatesProcessor = carController.getCoordinateProcessor();
		coordinatesProcessorRole.setBinding(coordinatesProcessor);
		int nextPosition = (this.currentPosition + 1) % this.interpolatedPoints.size();
		Message event = new Message(environmentRole, coordinatesProcessorRole, "currentGPSCoordinateChanged",
				this.interpolatedPoints.get(nextPosition));
		runtimeAdapter.receive(event);
		runtimeAdapter.publish(event);
		this.currentPosition = nextPosition;
	}

	public void setRuntimeAdapter(RuntimeAdapter runtimeAdapter) {
		this.runtimeAdapter = runtimeAdapter;
	}

	public void setCarController(CarController carController) {
		this.carController = carController;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals(DashboardFrame.MOVENEXT_ACTION_COMMAND)) {
			Settings.NETWORK_OUT.println("Move to next from " + carController.getName() + " Car");
			currentGPSCoordinateChangedCar();
		}
	}
}
