package sbp.examples.ui.simulation;

import java.awt.FlowLayout;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import sbp.examples.domainmodel.CarController;
import sbp.runtime.adapter.RuntimeAdapter;
import sbp.ui.ConsoleFrame;

@SuppressWarnings("serial")
public class DashboardFrame extends ConsoleFrame {

	private JLabel label;

	private RuntimeAdapter runtimeAdapter;

	public final static String MOVENEXT_ACTION_COMMAND = "MOVENEXT_ACTION_COMMAND";

	public RuntimeAdapter getRuntimeAdapter() {
		return runtimeAdapter;
	}

	public DashboardFrame(String name, RuntimeAdapter adapter, CarController carController) {
		super(name);
		// getTabbedPane().setVisible(false);
		label = new JLabel("Driving");
		label.setFont(new Font(label.getFont().getName(), Font.PLAIN, 100));
		add(label);

		this.runtimeAdapter = adapter;
		CarToXSimulationFrameListener listener = CarToXSimulationFrameListener.getInstance();
		listener.setRuntimeAdapter(runtimeAdapter);
		listener.setCarController(carController);

		JPanel jpanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		JButton jamesCarMoveNextButton = new JButton("Move " + carController.getName());
		jamesCarMoveNextButton.addActionListener(listener);
		jamesCarMoveNextButton.setActionCommand(MOVENEXT_ACTION_COMMAND);
		jpanel.add(jamesCarMoveNextButton);

		addPanel("Simulation", jpanel);

		pack();
	}

	public JLabel getLabel() {
		return label;
	}

}
