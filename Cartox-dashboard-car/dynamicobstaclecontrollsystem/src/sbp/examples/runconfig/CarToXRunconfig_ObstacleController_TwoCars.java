package sbp.examples.runconfig;

import sbp.examples.domainmodel.CarToXSystem;
import sbp.examples.networkadapter.MQTTDistributedRuntimeAdapter;
import sbp.examples.runtime.MainObstacleController_TwoCars;
import sbp.examples.specification.DynamicOCSpecification;
import sbp.examples.ui.simulation.CarToXSimulationFrame;
import sbp.runtime.SpecificationRunconfig;
import sbp.runtime.settings.Settings;

public class CarToXRunconfig_ObstacleController_TwoCars extends SpecificationRunconfig<DynamicOCSpecification> {

	public CarToXRunconfig_ObstacleController_TwoCars() {
		super(new DynamicOCSpecification());
		MQTTDistributedRuntimeAdapter mqttAdapter = new MQTTDistributedRuntimeAdapter(this);
		mqttAdapter.setCarToXSystem(MainObstacleController_TwoCars.getCarToXSystemTowCars());
		setAdapter(mqttAdapter);
		CarToXSimulationFrame logFrame = new CarToXSimulationFrame("ObstacleController");
		logFrame.setVisible(true);
		Settings.setRuntimeOut(logFrame.getRuntimeLog());
		Settings.setServerOut(logFrame.getServerLog());
		Settings.SPECTATOR__PRINT_CAUGHT_MESSAGE = true;
	}

	@Override
	protected void registerParticipatingObjects() {
		CarToXSystem carToXSystem = MainObstacleController_TwoCars.getCarToXSystemTowCars();
		// StreetsectionControll and its ObstacleController
		registerObject(carToXSystem.getStreetSectionControllers().get(0), CONTROLLABLE);
		registerObject(carToXSystem.getStreetSectionControllers().get(0).getOc().get(0), CONTROLLABLE);
		// Car and Dashboard and CoordinatesProcessor : James
		registerObject(carToXSystem.getCarControllers().get(0), UNCONTROLLABLE);
		registerObject(carToXSystem.getCarControllers().get(0).getDashboard(), UNCONTROLLABLE);
		registerObject(carToXSystem.getCarControllers().get(0).getCoordinateProcessor(), UNCONTROLLABLE);
		// Car and Dashboard and CoordinatesProcessor : Annas
		registerObject(carToXSystem.getCarControllers().get(1), UNCONTROLLABLE);
		registerObject(carToXSystem.getCarControllers().get(1).getDashboard(), UNCONTROLLABLE);
		registerObject(carToXSystem.getCarControllers().get(1).getCoordinateProcessor(), UNCONTROLLABLE);
		// Environment
		registerObject(carToXSystem.getEnv(), UNCONTROLLABLE);
	}

	@Override
	protected void registerNetworkAdressesForObjects() {
		// TODO Auto-generated method stub
	}

	@Override
	protected void registerObservers() {
		// TODO Auto-generated method stub
	}

}
