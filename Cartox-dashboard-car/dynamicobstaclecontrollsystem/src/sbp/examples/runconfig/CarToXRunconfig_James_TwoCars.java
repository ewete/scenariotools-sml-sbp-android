package sbp.examples.runconfig;

import sbp.examples.domainmodel.CarToXSystem;
import sbp.examples.networkadapter.MQTTDistributedRuntimeAdapter;
import sbp.examples.observers.JamesDashboardObserver;
import sbp.examples.runtime.MainJamesDashboard_TwoCars;
import sbp.examples.specification.DynamicOCSpecification;
import sbp.examples.ui.simulation.DashboardFrame;
import sbp.runtime.SpecificationRunconfig;
import sbp.runtime.settings.Settings;

public class CarToXRunconfig_James_TwoCars extends SpecificationRunconfig<DynamicOCSpecification> {

	private JamesDashboardObserver dashboardObserverForJamesCar;

	public CarToXRunconfig_James_TwoCars() {
		super(new DynamicOCSpecification());
		MQTTDistributedRuntimeAdapter mqttAdapter = new MQTTDistributedRuntimeAdapter(this);
		mqttAdapter.setCarToXSystem(MainJamesDashboard_TwoCars.getCarToXSystemTowCars());
		setAdapter(mqttAdapter);
		DashboardFrame jamesDashboardFrame = new DashboardFrame("Dashboard of JamesCar", getAdapter(),
				MainJamesDashboard_TwoCars.getCarToXSystemTowCars().getCarControllers().get(0));
		dashboardObserverForJamesCar.setLabel(jamesDashboardFrame.getLabel());
		jamesDashboardFrame.setVisible(true);
		Settings.setRuntimeOut(jamesDashboardFrame.getRuntimeLog());
		Settings.setServerOut(jamesDashboardFrame.getServerLog());
		Settings.SPECTATOR__PRINT_CAUGHT_MESSAGE = true;
	}

	@Override
	protected void registerParticipatingObjects() {
		CarToXSystem carToXSystem = MainJamesDashboard_TwoCars.getCarToXSystemTowCars();
		// StreetsectionControll and its ObstacleControll
		registerObject(carToXSystem.getStreetSectionControllers().get(0), UNCONTROLLABLE);
		registerObject(carToXSystem.getStreetSectionControllers().get(0).getOc().get(0), UNCONTROLLABLE);
		// Car and Dashboard and CoordinatesProcessor : James
		registerObject(carToXSystem.getCarControllers().get(0), CONTROLLABLE);
		registerObject(carToXSystem.getCarControllers().get(0).getDashboard(), CONTROLLABLE);
		registerObject(carToXSystem.getCarControllers().get(0).getCoordinateProcessor(), CONTROLLABLE);
		// Car and Dashboard and CoordinatesProcessor : Annas
		registerObject(carToXSystem.getCarControllers().get(1), UNCONTROLLABLE);
		registerObject(carToXSystem.getCarControllers().get(1).getDashboard(), UNCONTROLLABLE);
		registerObject(carToXSystem.getCarControllers().get(1).getCoordinateProcessor(), UNCONTROLLABLE);
		// Environment
		registerObject(carToXSystem.getEnv(), UNCONTROLLABLE);
	}

	@Override
	protected void registerNetworkAdressesForObjects() {
		// TODO Auto-generated method stub
	}

	@Override
	protected void registerObservers() {
		dashboardObserverForJamesCar = new JamesDashboardObserver();
		dashboardObserverForJamesCar.setCarToXSystem(MainJamesDashboard_TwoCars.getCarToXSystemTowCars());
		addScenarioObserver(dashboardObserverForJamesCar);
	}
}
