package sbp.examples.runconfig;

import sbp.examples.domainmodel.CarToXSystem;
import sbp.examples.networkadapter.MQTTDistributedRuntimeAdapter;
import sbp.examples.observers.AnnasDashboardObserver;
import sbp.examples.runtime.MainAnnasDashboard_TwoCars;
import sbp.examples.specification.DynamicOCSpecification;
import sbp.examples.ui.simulation.DashboardFrame;
import sbp.runtime.SpecificationRunconfig;
import sbp.runtime.settings.Settings;

public class CarToXRunconfig_Annas_TwoCars extends SpecificationRunconfig<DynamicOCSpecification> {

	private AnnasDashboardObserver dashboardObserverForAnnasCar;

	public CarToXRunconfig_Annas_TwoCars() {
		super(new DynamicOCSpecification());
		MQTTDistributedRuntimeAdapter mqttAdapter = new MQTTDistributedRuntimeAdapter(this);
		mqttAdapter.setCarToXSystem(MainAnnasDashboard_TwoCars.getCarToXSystemTwoCars());
		setAdapter(mqttAdapter);
		DashboardFrame annasDashboardFrame = new DashboardFrame("Dashboard of AnnasCar", getAdapter(),
				MainAnnasDashboard_TwoCars.getCarToXSystemTwoCars().getCarControllers().get(1));
		dashboardObserverForAnnasCar.setLabel(annasDashboardFrame.getLabel());
		annasDashboardFrame.setVisible(true);
		Settings.setRuntimeOut(annasDashboardFrame.getRuntimeLog());
		Settings.setServerOut(annasDashboardFrame.getServerLog());
		Settings.SPECTATOR__PRINT_CAUGHT_MESSAGE = true;
	}

	@Override
	protected void registerParticipatingObjects() {
		CarToXSystem carToXSystem = MainAnnasDashboard_TwoCars.getCarToXSystemTwoCars();
		// StreetsectionControll and its ObstacleControll
		registerObject(carToXSystem.getStreetSectionControllers().get(0), UNCONTROLLABLE);
		registerObject(carToXSystem.getStreetSectionControllers().get(0).getOc().get(0), UNCONTROLLABLE);
		// Car and Dashboard and CoordinatesProcessor : James
		registerObject(carToXSystem.getCarControllers().get(0), UNCONTROLLABLE);
		registerObject(carToXSystem.getCarControllers().get(0).getDashboard(), UNCONTROLLABLE);
		registerObject(carToXSystem.getCarControllers().get(0).getCoordinateProcessor(), UNCONTROLLABLE);
		// Car and Dashboard and CoordinatesProcessor : Annas
		registerObject(carToXSystem.getCarControllers().get(1), CONTROLLABLE);
		registerObject(carToXSystem.getCarControllers().get(1).getDashboard(), CONTROLLABLE);
		registerObject(carToXSystem.getCarControllers().get(1).getCoordinateProcessor(), CONTROLLABLE);
		// Environment
		registerObject(carToXSystem.getEnv(), UNCONTROLLABLE);
	}

	@Override
	protected void registerNetworkAdressesForObjects() {
		// TODO Auto-generated method stub
	}

	@Override
	protected void registerObservers() {
		dashboardObserverForAnnasCar = new AnnasDashboardObserver();
		dashboardObserverForAnnasCar.setCarToXSystem(MainAnnasDashboard_TwoCars.getCarToXSystemTwoCars());
		addScenarioObserver(dashboardObserverForAnnasCar);
	}
}
