package sbp.examples.domainmodel;

import java.util.ArrayList;
import java.util.List;

public class CarToXSystem extends NamedElement {

	private List<StreetSectionController> streetSectionControllers;
	private List<CarController> carControllers;
	private Environment env;
	private static CarToXSystem instance;

	private CarToXSystem() {
		this.streetSectionControllers = new ArrayList<>();
		this.carControllers = new ArrayList<>();
	}

	static CarToXSystem getInstance() {
		if (instance == null) {
			instance = new CarToXSystem();
		}
		return instance;
	}

	public void addStreetSectionController(StreetSectionController streetSectionController) {
		streetSectionControllers.add(streetSectionController);
	}

	public void addCarController(CarController carController) {
		carControllers.add(carController);
	}

	public List<StreetSectionController> getStreetSectionControllers() {
		return streetSectionControllers;
	}

	public List<CarController> getCarControllers() {
		return carControllers;
	}

	public Environment getEnv() {
		return env;
	}

	public void setEnv(Environment env) {
		this.env = env;
	}

}
