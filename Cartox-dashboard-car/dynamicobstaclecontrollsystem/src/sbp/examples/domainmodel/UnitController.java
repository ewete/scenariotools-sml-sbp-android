package sbp.examples.domainmodel;

public class UnitController extends NamedElement {

	private GPSCoordinate begin;
	private GPSCoordinate end;

	public GPSCoordinate getBegin() {
		return begin;
	}

	public void setBegin(GPSCoordinate begin) {
		this.begin = begin;
	}

	public GPSCoordinate getEnd() {
		return end;
	}

	public void setEnd(GPSCoordinate end) {
		this.end = end;
	}

}
