package sbp.examples.domainmodel;

import java.util.List;

import sbp.examples.util.HaversineAlgorithm;

public class CoordinatesProcessor extends NamedElement {

	public static final double TOLERANCE_DISTANCE = 5; // Radius around End and
														// Begin in meters

	private CarController car;
	private List<ObstacleController> oCList;
	private boolean inEndArea = false;
	private boolean inBeginArea = false;
	private boolean inObstacleArea = false;

	private double calculateDistanceToEndOfOC(GPSCoordinate x) {
		GPSCoordinate end = this.car.getNextOC().getEnd();
		return HaversineAlgorithm.haversineInM(end.getLatitudeDegree(), end.getLongitudeDegree(), x.getLatitudeDegree(),
				x.getLongitudeDegree());
	}

	private double calculateDistanceToBeginOfOC(GPSCoordinate x) {
		GPSCoordinate begin = car.getNextOC().getBegin();
		return HaversineAlgorithm.haversineInM(begin.getLatitudeDegree(), begin.getLongitudeDegree(),
				x.getLatitudeDegree(), x.getLongitudeDegree());
	}

	public double getObstacleLength() {
		double obstacleLength = HaversineAlgorithm.haversineInM(car.getNextOC().getEnd().getLatitudeDegree(),
				car.getNextOC().getEnd().getLongitudeDegree(), car.getNextOC().getBegin().getLatitudeDegree(),
				car.getNextOC().getBegin().getLongitudeDegree());
		return obstacleLength;
	}

	public boolean isInEndArea(GPSCoordinate x) {
		if (car.getNextOC() != null) {
			double distanceToEnd = calculateDistanceToEndOfOC(x);
			if (distanceToEnd < TOLERANCE_DISTANCE) {
				return true;
			}

		}
		return false;
	}

	public boolean isInBeginArea(GPSCoordinate x) {
		if (car.getNextOC() != null) {
			double distanceToBegin = calculateDistanceToBeginOfOC(x);
			if (distanceToBegin < TOLERANCE_DISTANCE) {
				return true;
			}

		}
		return false;
	}

	public boolean isInObstacleArea(GPSCoordinate x) {
		if (car.getNextOC() != null) {
			double distanceToBegin = calculateDistanceToBeginOfOC(x);
			double distanceToEnd = calculateDistanceToEndOfOC(x);
			double obstacleLength = getObstacleLength();
			if (distanceToBegin < obstacleLength && distanceToEnd < obstacleLength) {
				return true;
			}
		}
		return false;
	}

	public List<ObstacleController> getOCList() {
		return oCList;
	}

	public void setOCList(List<ObstacleController> oc) {
		this.oCList = oc;
	}

	public boolean isInEndArea() {
		return inEndArea;
	}

	public void setInEndArea(boolean inEndArea) {
		this.inEndArea = inEndArea;
	}

	public boolean isInBeginArea() {
		return inBeginArea;
	}

	public void setInBeginArea(boolean inBeginArea) {
		this.inBeginArea = inBeginArea;
	}

	public boolean isInObstacleArea() {
		return inObstacleArea;
	}

	public void setInObstacleArea(boolean inObstacleArea) {
		this.inObstacleArea = inObstacleArea;
	}

	public CarController getCar() {
		return car;
	}

	public void setCar(CarController car) {
		this.car = car;
	}

}
