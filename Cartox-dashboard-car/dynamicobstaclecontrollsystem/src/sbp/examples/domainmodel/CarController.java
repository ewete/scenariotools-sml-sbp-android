package sbp.examples.domainmodel;

public class CarController extends NamedElement {
	private GPSCoordinate position;

	private StreetSectionController currentSSC;
	private ObstacleController nextOC;
	private CarDashboard dashboard;
	private CoordinatesProcessor coordinateProcessor;
	private Environment env;

	public CoordinatesProcessor getCoordinateProcessor() {
		return coordinateProcessor;
	}

	public void setCoordinateProcessor(CoordinatesProcessor cp) {
		this.coordinateProcessor = cp;
	}

//	private CarStatus carStatus;
//	private double minimalDistance = 5; // 5 meter

//	public CarController() {
//		this.carStatus = CarStatus.APPROACHING;
//	}

	public GPSCoordinate getPosition() {
		return position;
	}

	public void removeOC() {
		this.nextOC = null;
	}

//	private double calculateDistanceToObstacleBegin() {
//		// distance from car to begin
//		GPSCoordinate begin = this.nextOC.getBegin();
//		return HaversineAlgorithm.haversineInM(begin.getLatitudeDegree(), begin.getLongitudeDegree(),
//				position.getLatitudeDegree(), position.getLongitudeDegree());
//	}
//
//	private double calculateDistanceToObstacleEnd() {
//		// distance from car to end
//		GPSCoordinate end = this.nextOC.getEnd();
//		return HaversineAlgorithm.haversineInM(end.getLatitudeDegree(), end.getLongitudeDegree(),
//				position.getLatitudeDegree(), position.getLongitudeDegree());
//	}
	// private double calculateDistanceToObstacleOnLeft() {
	// // distance from car to end
	// // (obstacle is on the left of the car)
	// GPSCoordinate end = this.nextOC.getEnd();
	// return
	// HaversineAlgorithm.haversineInM(end.getLatitudeDegree(),end.getLongitudeDegree(),
	// position.getLatitudeDegree(),position.getLongitudeDegree());
	// }

//	public void currentGPSCoordinateChanged() {
//		ObstacleController obstacleController = this.nextOC;
//		if (obstacleController == null) {
//			obstacleController = currentSSC.getNextOc(this.getPosition());/// set
//		}
//
//		if (obstacleController != null) {
//			double distance1 = calculateDistanceToObstacleBegin();
//			double distance2 = calculateDistanceToObstacleEnd();
//			double obstacleLength = HaversineAlgorithm.haversineInM(obstacleController.getEnd().getLatitudeDegree(),
//					obstacleController.getEnd().getLongitudeDegree(), obstacleController.getBegin().getLatitudeDegree(),
//					obstacleController.getBegin().getLongitudeDegree());
//			double diff = Math.abs(distance1 - distance2);
//			
//			// TODO: We don't need carStatus any more
//			// some code should be deleted
//			if ((diff < obstacleLength + minimalDistance) && (diff > obstacleLength - minimalDistance)) {
//				// setAppraochingobastacle(this.nextOC)
//				// this.nextOC.register(this);
//				if (this.carStatus == CarStatus.APPROACHING) {
//					this.nextOC = obstacleController;
//				} else if (this.carStatus == CarStatus.ENTERED) {
//					// machen wir nichts
//				} else if (this.carStatus == CarStatus.LEFT) {
//					// TODO get the next of nextOC
//					this.nextOC = null;
//				}
//
//			} else {
//				if (this.carStatus == CarStatus.APPROACHING) {
//					this.nextOC = obstacleController;
//				} else if (this.carStatus == CarStatus.ENTERED) {
//					// machen wir nichts
//				} else if (this.carStatus == CarStatus.LEFT) {
//					System.out.println("Fehler this.carStatus == CarStatus.LEFT");
//				}
//			}
//		} else {
//			this.carStatus = CarStatus.DRIVING; // If there is no obstacle the
//												// Car is driving
//		}
//
//	}

	public void setApproachingObstacle(ObstacleController oc) {
		this.nextOC = oc;
	}

	public void enteringAllowed() {

	}

	public void enteringDisallowed() {

	}

//	public void hasEnteredNarrowPassage() {
//		this.carStatus = CarStatus.ENTERED;
//	}
//
//	public void hasLeftNarrowPassage() {
//		this.carStatus = CarStatus.LEFT;
//	}

	public void setPosition(GPSCoordinate newPosition) {
		this.position = newPosition;
	}

//	public CarStatus getCarStatus() {
//		return carStatus;
//	}
//
//	public void setCarStatus(CarStatus carStatus) {
//		this.carStatus = carStatus;
//	}

	public void register() {

	}

	public ObstacleController getNextOC() {
		return nextOC;
	}

	public void setDashboard(CarDashboard dashboard) {
		this.dashboard = dashboard;
	}

	public CarDashboard getDashboard() {
		return dashboard;
	}

	public void setCurrentSSC(StreetSectionController currentSSC) {
		this.currentSSC = currentSSC;
	}

	public void setNextOC(ObstacleController nextOC) {
		this.nextOC = nextOC;
	}

	public void setEnv(Environment env) {
		this.env = env;
	}

	public Environment getEnv() {
		return env;
	}

}
