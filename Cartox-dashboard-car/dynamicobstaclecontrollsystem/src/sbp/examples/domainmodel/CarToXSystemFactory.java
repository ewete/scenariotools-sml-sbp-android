package sbp.examples.domainmodel;

import java.util.ArrayList;
import java.util.List;

public class CarToXSystemFactory {

	private static CarToXSystemFactory instance;
	private CarToXSystem carToXSystemOneCar;
	private CarToXSystem carToXSystemTwoCars;

	private CarToXSystemFactory() {
	}

	public static CarToXSystemFactory getInstance() {
		if (instance == null) {
			instance = new CarToXSystemFactory();
		}
		return instance;
	}

	public CarToXSystem getCarToXSystemOneCar() {
		if (carToXSystemOneCar != null) {
			return carToXSystemOneCar;
		}
		this.carToXSystemOneCar = CarToXSystem.getInstance();
		StreetSectionController scc = createStreetSectionController();
		scc.setName("streetSectionController");
		ObstacleController oc = createObstacleController();
		oc.setName("oc");
		oc.setBegin(new GPSCoordinate(52.37966, 9.72071));
		oc.setEnd(new GPSCoordinate(52.37977, 9.72054));
		scc.addOC(oc);

		OCDashboard ocDashboard = createOCDashboard();
		ocDashboard.setName("ocDashboard");
		ocDashboard.setCurrent(scc);
		ocDashboard.setOc(oc);

		List<ObstacleController> ocList = new ArrayList<ObstacleController>();
		ocList.add(oc);

		CarDashboard jamesCarDashboard = createCarDashboard();
		jamesCarDashboard.setName("jamesCarDashboard");

		CoordinatesProcessor jamesCoordinatesProcessor = createCoordinatesProcessor();
		jamesCoordinatesProcessor.setName("cpJamesCar");

		Environment env = createEnvironment();// erzeugt environment
		env.setName("env");

		CarController jamesCar = createCarController();
		jamesCar.setName("jamesCar");
		jamesCar.setDashboard(jamesCarDashboard);
		jamesCar.setNextOC(oc);
		//jamesCar.setCarStatus(CarStatus.APPROACHING);
		jamesCar.setCurrentSSC(scc);
		jamesCar.setCoordinateProcessor(jamesCoordinatesProcessor);
		jamesCar.setEnv(env);

		jamesCoordinatesProcessor.setCar(jamesCar);
		jamesCoordinatesProcessor.setOCList(ocList);

		// set objectsystem properties
		carToXSystemOneCar.setEnv(env);
		carToXSystemOneCar.addStreetSectionController(scc);
		carToXSystemOneCar.addCarController(jamesCar);

		return carToXSystemOneCar;
	}

	/*
	 * private CarToXSystem getCarToXSystemTwoCars() { return
	 * getCarToXSystemTwoCars(new GPSCoordinate(52.37966, 9.72071), new
	 * GPSCoordinate(52.37977, 9.72054)); }
	 */

	public CarToXSystem getCarToXSystemTwoCars(GPSCoordinate ocBeginPoint, GPSCoordinate ocEndPoint) {
		if (ocBeginPoint == null) {
			// Assign default value new GPSCoordinate(52.37966, 9.72071) ?
			try {
				throw new Exception("ObstacleControl begin point is missing");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (ocEndPoint == null) {
			// Assign default value new GPSCoordinate(52.37977, 9.72054)?
			try {
				throw new Exception("ObstacleControl begin point is missing");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (carToXSystemTwoCars != null) {
			return carToXSystemTwoCars;
		}
		carToXSystemTwoCars = CarToXSystem.getInstance();
		StreetSectionController scc = createStreetSectionController();
		scc.setName("streetSectionController");
		ObstacleController oc = createObstacleController();
		oc.setName("oc");
		// oc.setBegin(new GPSCoordinate(52.37966, 9.72071));
		// oc.setEnd(new GPSCoordinate(52.37977, 9.72054));
		oc.setBegin(ocBeginPoint);
		oc.setEnd(ocEndPoint);
		scc.addOC(oc);

		OCDashboard ocDashboard = createOCDashboard();
		ocDashboard.setName("ocDashboard");
		ocDashboard.setCurrent(scc);
		ocDashboard.setOc(oc);

		List<ObstacleController> ocList = new ArrayList<ObstacleController>();
		ocList.add(oc);

		CarDashboard jamesCarDashboard = createCarDashboard();
		jamesCarDashboard.setName("jamesCarDashboard");

		CoordinatesProcessor jamesCoordinatesProcessor = createCoordinatesProcessor();
		jamesCoordinatesProcessor.setName("cpJamesCar");

		Environment env = createEnvironment();// erzeugt environment
		env.setName("env");

		CarController jamesCar = createCarController();
		jamesCar.setName("jamesCar");
		jamesCar.setDashboard(jamesCarDashboard);
		jamesCar.setNextOC(oc);
		//jamesCar.setCarStatus(CarStatus.APPROACHING);
		jamesCar.setCurrentSSC(scc);
		jamesCar.setCoordinateProcessor(jamesCoordinatesProcessor);
		jamesCar.setEnv(env);

		jamesCoordinatesProcessor.setCar(jamesCar);
		jamesCoordinatesProcessor.setOCList(ocList);

		CarDashboard annasCarDashboard = createCarDashboard();
		annasCarDashboard.setName("annasCarDashboard");

		CoordinatesProcessor annasCoordinatesProcessor = createCoordinatesProcessor();
		annasCoordinatesProcessor.setName("cpAnnasCar");

		CarController annasCar = createCarController();
		annasCar.setName("annasCar");
		annasCar.setDashboard(annasCarDashboard);
		annasCar.setNextOC(oc);
		//annasCar.setCarStatus(CarStatus.APPROACHING);
		annasCar.setCurrentSSC(scc);
		annasCar.setCoordinateProcessor(annasCoordinatesProcessor);
		annasCar.setEnv(env);

		annasCoordinatesProcessor.setCar(annasCar);
		annasCoordinatesProcessor.setOCList(ocList);

		// set objectsystem properties
		carToXSystemTwoCars.setEnv(env);
		carToXSystemTwoCars.addStreetSectionController(scc);
		carToXSystemTwoCars.addCarController(jamesCar);
		carToXSystemTwoCars.addCarController(annasCar);

		return carToXSystemTwoCars;
	}

	private StreetSectionController createStreetSectionController() {
		return new StreetSectionController();
	}

	private ObstacleController createObstacleController() {
		return new ObstacleController();
	}

	private OCDashboard createOCDashboard() {
		return new OCDashboard();
	}

	private CarDashboard createCarDashboard() {
		return new CarDashboard();
	}

	private CarController createCarController() {
		return new CarController();
	}

	private CoordinatesProcessor createCoordinatesProcessor() {
		return new CoordinatesProcessor();
	}

	private Environment createEnvironment() {
		return Environment.getInstance();
	}
}
