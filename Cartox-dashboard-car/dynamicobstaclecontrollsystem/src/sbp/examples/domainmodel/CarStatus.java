package sbp.examples.domainmodel;

public enum CarStatus {
	APPROACHING,
	ENTERED,
	LEFT,
	DRIVING
}
