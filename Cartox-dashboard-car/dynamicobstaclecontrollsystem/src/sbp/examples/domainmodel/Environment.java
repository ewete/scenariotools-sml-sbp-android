package sbp.examples.domainmodel;

public class Environment extends NamedElement {
	private static Environment instance;

	private Environment() {

	}

	public static Environment getInstance() {
		if (instance == null) {
			instance = new Environment();
		}
		return instance;
	}

}
