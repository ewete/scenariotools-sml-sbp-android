package sbp.examples.domainmodel;

import java.util.ArrayList;
import java.util.List;

public class StreetSectionController extends UnitController {
	private List<StreetSectionController> next;
	private List<ObstacleController> oc;

	public List<ObstacleController> getOc() {
		return oc;
	}

	public void addOC(ObstacleController obstacleController) {
		this.oc.add(obstacleController);
	}

	 public ObstacleController getNextOc(GPSCoordinate carCoordinate){
	
		 return oc.get(0);
	 }
	public void addNextSSC(StreetSectionController next) {
		this.next.add(next);
	}

	public StreetSectionController() {
		oc = new ArrayList<ObstacleController>() ;
		next = new ArrayList<StreetSectionController>();
		
	}
}
