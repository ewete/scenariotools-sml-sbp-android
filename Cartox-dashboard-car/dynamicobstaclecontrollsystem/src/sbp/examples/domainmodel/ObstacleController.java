package sbp.examples.domainmodel;

import java.util.ArrayList;
import java.util.List;

public class ObstacleController extends UnitController {

	private List<CarController> carOnNarrowPassageLaneAllowedToPass;
	private List<CarController> registeredCarsWaiting;

	public ObstacleController() {
		this.carOnNarrowPassageLaneAllowedToPass = new ArrayList<>();
		this.registeredCarsWaiting = new ArrayList<>();
	}

	public void register(CarController car) {
		this.registeredCarsWaiting.add(car);
	}

	public void unregister(CarController car) {
		this.carOnNarrowPassageLaneAllowedToPass.remove(car);
	}

	public List<CarController> getRegisteredCarsWaiting() {
		return registeredCarsWaiting;
	}

	public List<CarController> getCarsOnNarrowPassageLaneAllowedToPass() {
		return carOnNarrowPassageLaneAllowedToPass;
	}

	public void allowedToDrive(CarController car) {
		if (this.registeredCarsWaiting.contains(car)) {
			carOnNarrowPassageLaneAllowedToPass.add(car);
			registeredCarsWaiting.remove(car);
		}
	}
}
