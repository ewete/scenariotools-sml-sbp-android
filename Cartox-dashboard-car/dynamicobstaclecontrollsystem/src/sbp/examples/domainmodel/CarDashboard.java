package sbp.examples.domainmodel;

public class CarDashboard extends NamedElement {
	public CarDashboard() {

	}

	private String info = "DRIVING";

	public String getInfo() {
		return info;
	}

	public void showGo() {
		info = "GO";
		System.out.println("GO! from CarDashboard class [" + this.getName() + "]");
	}

	public void showStop() {
		info = "STOP";
		System.out.println("STOP! from CarDashboard class [" + this.getName() + "]");
	}

	// Default Case
	public void showDriving() {
		info = "DRIVING";
		System.out.println("DRIVING from CarDashboard class [" + this.getName() + "]");
	}

}
