package sbp.examples.domainmodel;

public class OCDashboard extends NamedElement {
	private StreetSectionController current;
	private ObstacleController oc;

	public void markBegin(GPSCoordinate begin) {

	}

	public void markEnd(GPSCoordinate end) {

	}

	public void setCurrent(StreetSectionController current) {
		this.current = current;
	}

	public void setOc(ObstacleController oc) {
		this.oc = oc;
	}

	public StreetSectionController getCurrent() {
		return current;
	}

	public ObstacleController getOc() {
		return oc;
	}
}
