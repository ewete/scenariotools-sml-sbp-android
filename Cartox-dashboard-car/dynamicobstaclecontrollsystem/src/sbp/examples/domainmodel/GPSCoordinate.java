package sbp.examples.domainmodel;

import java.util.function.LongToIntFunction;

public class GPSCoordinate {
	private double longitudeDegree;
	private double latitudeDegree;

	public GPSCoordinate(double longitude, double latitude) {
		this.longitudeDegree = longitude;
		this.latitudeDegree = latitude;
	}

	public double getLongitudeDegree() {
		return longitudeDegree;
	}

	public void setLongitudeDegree(double longitudeDegree) {
		this.longitudeDegree = longitudeDegree;
	}

	public double getLatitudeDegree() {
		return latitudeDegree;
	}

	public void setLatitudeDegree(double latitudeDegree) {
		this.latitudeDegree = latitudeDegree;
	}

	@Override
	public String toString() {
		return String.format("GPS Coordinates: longitude %f latitude %f", longitudeDegree, latitudeDegree);
	}

}
