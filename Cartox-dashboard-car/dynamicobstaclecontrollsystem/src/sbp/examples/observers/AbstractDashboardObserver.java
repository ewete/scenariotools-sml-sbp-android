package sbp.examples.observers;

import sbp.examples.domainmodel.CarToXSystem;
import sbp.specification.scenarios.utils.ScenarioObserver;

public abstract class AbstractDashboardObserver extends ScenarioObserver {

	protected CarToXSystem carToXSystem;

	public void setCarToXSystem(CarToXSystem carToXSystem) {
		this.carToXSystem = carToXSystem;
	}
}
