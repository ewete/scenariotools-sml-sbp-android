package sbp.examples.observers;

import java.awt.Color;

import javax.swing.JLabel;

import sbp.examples.domainmodel.CarDashboard;
import sbp.specification.events.Message;
import sbp.specification.role.Role;
import sbp.specification.scenarios.Scenario;

public class AnnasDashboardObserver extends AbstractDashboardObserver {

	private JLabel label;

	public JLabel getLabel() {
		return label;
	}

	public void setLabel(JLabel label) {
		this.label = label;
	}

	@Override
	public void trigger(Scenario scenario, Message messageEvent) {
		Role receiver = messageEvent.getReceiver();
		String message = messageEvent.getMessage();
		if (receiver.getBinding() instanceof CarDashboard) {
			CarDashboard dashboard = (CarDashboard) receiver.getBinding();
			if (dashboard == carToXSystem.getCarControllers().get(1).getDashboard()) {
				if (message.equals("showGo")) {
					showGo();
				} else if (message.equals("showStop")) {
					showStop();
				}
			}
		}
	}

	private void showGo() {
		System.out.println("Show Go from AnnasDashboardObserver");
		label.setText("Go!");
		label.setForeground(Color.green);
	}

	private void showStop() {
		System.out.println("Show Stop from AnnasDashboardObserver");
		label.setText("Stop!");
		label.setForeground(Color.red);
	}

}
