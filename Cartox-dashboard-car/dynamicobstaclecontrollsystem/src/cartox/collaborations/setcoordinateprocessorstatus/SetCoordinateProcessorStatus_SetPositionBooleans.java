package cartox.collaborations.setcoordinateprocessorstatus;

import java.util.List;

import sbp.specification.events.Message;
import sbp.specification.scenarios.violations.Violation;

@SuppressWarnings("serial")
public class SetCoordinateProcessorStatus_SetPositionBooleans extends SetCoordinateProcessorStatusCollaboration {

	@Override
	protected void initialisation() {
		addInitializingMessage(new Message(cp, cp, "setPositionBooleans", RANDOM, RANDOM, RANDOM));
	}

	@Override
	protected void registerRoleBindings() {
		// TODO Auto-generated method stub
	}

	@Override
	protected void registerAlphabet() {
		setBlocked(cp, cp, "setPositionBooleans", RANDOM, RANDOM, RANDOM);
	}

	@Override
	protected void body() throws Violation {
		List<Object> lastMessageParameters = getLastMessage().getParameters();
		boolean isInBeginAreaNew = (boolean) lastMessageParameters.get(0);
		boolean isInObstacleAreaNew = (boolean) lastMessageParameters.get(1);
		boolean isInEndAreaNew = (boolean) lastMessageParameters.get(2);
		cp.getBinding().setInBeginArea(isInBeginAreaNew);
		cp.getBinding().setInObstacleArea(isInObstacleAreaNew);
		cp.getBinding().setInEndArea(isInEndAreaNew);
	}
}
