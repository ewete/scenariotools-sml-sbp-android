package cartox.collaborations.setcoordinateprocessorstatus;

// Framework
import sbp.examples.domainmodel.CoordinatesProcessor;
import sbp.specification.role.Role;
import sbp.specification.scenarios.Scenario;

// Roles
@SuppressWarnings("serial")
public abstract class SetCoordinateProcessorStatusCollaboration extends Scenario {

	// Roles
	protected Role<CoordinatesProcessor> cp = createRole(CoordinatesProcessor.class, "cp");

}