package cartox.collaborations.carsregisterwithobstaclecontrol;

import java.util.ArrayList;
import java.util.List;

import sbp.specification.events.Message;
import sbp.specification.scenarios.violations.Violation;

@SuppressWarnings("serial")
public class CarsRegisterWithObstacleControl_ControlStationAllowsOrDisallowsCarOnBlockedLaneToEnterScenario
		extends CarsRegisterWithObstacleControlCollaboration {

	@Override
	protected void registerAlphabet() {
		setBlocked(cp, car, "setApproachingObstacle", RANDOM);
		setBlocked(car, obstacleController, "register");
		setBlocked(obstacleController, car, "enteringAllowed");
		setBlocked(car, dashboard, "showGo");
		setBlocked(obstacleController, car, "enteringDisallowed");
		setBlocked(car, dashboard, "showStop");
	}

	@Override
	protected void initialisation() {
		addInitializingMessage(new Message(cp, car, "setApproachingObstacle", RANDOM));
	}

	@Override
	protected void registerRoleBindings() {
		bindRoleToObject(obstacleController, car.getBinding().getNextOC());
		bindRoleToObject(dashboard, car.getBinding().getDashboard());
	}

	@Override
	protected void body() throws Violation {
		// true
		request(STRICT, car, obstacleController, "register");
		// Begin Alternative
		List<Message> requestedMessages = new ArrayList<Message>();
		List<Message> waitedForMessages = new ArrayList<Message>();
		if (obstacleController.getBinding().getCarsOnNarrowPassageLaneAllowedToPass().isEmpty()) {
			requestedMessages.add(new Message(STRICT, obstacleController, car, "enteringAllowed"));
			// true
		}
		if (!obstacleController.getBinding().getCarsOnNarrowPassageLaneAllowedToPass().isEmpty()) {
			requestedMessages.add(new Message(STRICT, obstacleController, car, "enteringDisallowed"));
			// true
		}
		// Insert into BP
		doStep(requestedMessages, waitedForMessages);
		// Determine which path has been chosen
		if (getLastMessage().equals(new Message(obstacleController, car, "enteringAllowed"))) {
			// true
			// TODO: WHY HERE A FUNCTION?
			car.getBinding().enteringAllowed();// the function is empty
			// TODO: WHY HERE waitFor (Jianwei)
			waitFor(STRICT, car, dashboard, "showGo");
		} else if (getLastMessage().equals(new Message(obstacleController, car, "enteringDisallowed"))) {
			// true
			// TODO: WHY HERE A FUNCTION?
			car.getBinding().enteringDisallowed();// the function is empty
			// TODO: WHY HERE waitFor (Jianwei)
			waitFor(STRICT, car, dashboard, "showStop");
		}
		// End Alternative
	}
}
