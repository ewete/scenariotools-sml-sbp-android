package cartox.collaborations.carsregisterwithobstaclecontrol;

import sbp.specification.events.Message;
import sbp.specification.scenarios.violations.Violation;

@SuppressWarnings("serial")
public class CarsRegisterWithObstacleControlAssumptions_CarDrivesIntoNarrowPassageIfItMayPassObstacle
		extends CarsRegisterWithObstacleControlCollaboration {

	@Override
	protected void registerAlphabet() {
		setBlocked(obstacleController, car, "enteringAllowed");
		setBlocked(env, cp, "currentGPSCoordinateChanged", RANDOM);
	}

	@Override
	protected void initialisation() {
		addInitializingMessage(new Message(obstacleController, car, "enteringAllowed"));
	}

	@Override
	protected void registerRoleBindings() {
		bindRoleToObject(env, car.getBinding().getEnv());
		bindRoleToObject(cp, car.getBinding().getCoordinateProcessor());
	}

	@Override
	protected void body() throws Violation {
		/*
		 * do { request(STRICT, env, cp, "currentGPSCoordinateChanged", RANDOM);
		 * } while (!cp.getBinding().isInObstacleArea()); // muesste assumption
		 * // sein
		 */
		// true
		// if ((car.getBinding().getOnLane() !=
		// car.getBinding().getDrivingInDirectionOfLane())) {
		// throwViolation(INTERRUPT);
		// }
		request(new Message(STRICT, env, cp, "currentGPSCoordinateChanged", RANDOM));
	}
}
