package cartox.collaborations.carsregisterwithobstaclecontrol;

import sbp.specification.events.Message;
import sbp.specification.scenarios.violations.Violation;

@SuppressWarnings("serial")
public class CarsRegisterWithObstacleControl_ControlStationTellsNextRegisteredCarToEnterScenario
		extends CarsRegisterWithObstacleControlCollaboration {

	@Override
	protected void registerAlphabet() {
		setBlocked(car, obstacleController, "unregister");
		setBlocked(obstacleController, nextCar, "enteringAllowed");
	}

	@Override
	protected void initialisation() {
		addInitializingMessage(new Message(car, obstacleController, "unregister"));
	}

	@Override
	protected void registerRoleBindings() {
		bindRoleToObject(nextCar, obstacleController.getBinding().getRegisteredCarsWaiting().get(0));
	}

	@Override
	protected void body() throws Violation {
		// true
		if ((nextCar.getBinding() == null)) {
			throwViolation(INTERRUPT);
		}
		request(STRICT, obstacleController, nextCar, "enteringAllowed");
		nextCar.getBinding().enteringAllowed();
	}
}
