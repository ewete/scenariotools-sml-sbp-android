package cartox.collaborations.carsregisterwithobstaclecontrol;

import sbp.examples.domainmodel.CarController;
import sbp.examples.domainmodel.CarDashboard;
import sbp.examples.domainmodel.CoordinatesProcessor;
import sbp.examples.domainmodel.Environment;
import sbp.examples.domainmodel.ObstacleController;
// Framework
import sbp.specification.role.Role;
import sbp.specification.scenarios.Scenario;

@SuppressWarnings("serial")
public abstract class CarsRegisterWithObstacleControlCollaboration extends Scenario {

	// Roles
	protected Role<Environment> env = createRole(Environment.class, "env");
	protected Role<CoordinatesProcessor> cp = createRole(CoordinatesProcessor.class, "cp");
	protected Role<CarController> car = createRole(CarController.class, "car");
	protected Role<CarController> nextCar = createRole(CarController.class, "nextCar");
	protected Role<CarDashboard> dashboard = createRole(CarDashboard.class, "dashboard");
	protected Role<ObstacleController> obstacleController = createRole(ObstacleController.class, "obstacleController");

}