package cartox.collaborations.carsregisterwithobstaclecontrol;

import sbp.specification.events.Message;
import sbp.specification.scenarios.violations.Violation;

// Roles

@SuppressWarnings("serial")
public class CarsRegisterWithObstacleControl_CarEventuallyAllowedToPassObstacleScenario
		extends CarsRegisterWithObstacleControlCollaboration {

	@Override
	protected void registerAlphabet() {
		setBlocked(obstacleController, car, "enteringDisallowed");
		setBlocked(obstacleController, car, "enteringAllowed");
	}

	@Override
	protected void initialisation() {
		addInitializingMessage(new Message(obstacleController, car, "enteringDisallowed"));
	}

	@Override
	protected void registerRoleBindings() {
	}

	@Override
	protected void body() throws Violation {
		// true
		waitFor(STRICT, obstacleController, car, "enteringAllowed");
	}
}
