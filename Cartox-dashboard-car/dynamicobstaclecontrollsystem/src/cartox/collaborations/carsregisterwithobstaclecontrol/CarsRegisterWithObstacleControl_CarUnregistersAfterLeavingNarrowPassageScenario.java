package cartox.collaborations.carsregisterwithobstaclecontrol;

import sbp.specification.events.Message;
import sbp.specification.scenarios.violations.Violation;

@SuppressWarnings("serial")
public class CarsRegisterWithObstacleControl_CarUnregistersAfterLeavingNarrowPassageScenario
		extends CarsRegisterWithObstacleControlCollaboration {

	@Override
	protected void registerAlphabet() {
		setBlocked(cp, car, "hasLeftNarrowPassage");
		setBlocked(car, obstacleController, "unregister");
	}

	@Override
	protected void initialisation() {
		addInitializingMessage(new Message(cp, car, "hasLeftNarrowPassage"));
	}

	@Override
	protected void registerRoleBindings() {
		bindRoleToObject(obstacleController, car.getBinding().getNextOC());
	}

	@Override
	protected void body() throws Violation {
		// true
		request(STRICT, car, obstacleController, "unregister");
	}
}
