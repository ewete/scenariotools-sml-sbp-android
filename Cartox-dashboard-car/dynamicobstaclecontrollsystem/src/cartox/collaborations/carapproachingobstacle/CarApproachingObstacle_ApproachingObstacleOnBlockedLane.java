package cartox.collaborations.carapproachingobstacle;

import sbp.examples.domainmodel.CarController;
import sbp.examples.domainmodel.GPSCoordinate;
import sbp.examples.domainmodel.ObstacleController;
import sbp.specification.events.Message;
import sbp.specification.scenarios.violations.Violation;

@SuppressWarnings("serial")
public class CarApproachingObstacle_ApproachingObstacleOnBlockedLane extends CarApproachingObstacleCollaboration {

	@Override
	protected void registerAlphabet() {
		setBlocked(env, cp, "currentGPSCoordinateChanged", RANDOM);// RANDOM
																	// added by
																	// Jianwei
		setBlocked(cp, car, "setApproachingObstacle", obstacleController.getBinding());
		setBlocked(cp, cp, "setPositionBooleans", RANDOM);
	}

	@Override
	protected void initialisation() {
		addInitializingMessage(new Message(env, cp, "currentGPSCoordinateChanged", RANDOM));
	}

	@Override
	protected void registerRoleBindings() {
		bindRoleToObject(obstacleController, (ObstacleController) cp.getBinding().getCar().getNextOC());
		bindRoleToObject(car, (CarController) cp.getBinding().getCar());
	}

	@Override
	protected void body() throws Violation {
		if (((obstacleController.getBinding() == null)) || (cp.getBinding().isInObstacleArea()
				|| cp.getBinding().isInBeginArea() || cp.getBinding().isInEndArea())) {
			throwViolation(INTERRUPT);
		} else if (car.getBinding() == null) {
			throwViolation(SAFETY);
		}
		GPSCoordinate positionNew = (GPSCoordinate) getLastMessage().getParameters().get(0);
		boolean isInBeginAreaNew = cp.getBinding().isInBeginArea(positionNew);
		boolean isInEndAreaNew = cp.getBinding().isInEndArea(positionNew);
		boolean isInObstacleAreaNew = cp.getBinding().isInObstacleArea(positionNew);
		if (isInBeginAreaNew || isInEndAreaNew) {
			request(STRICT, cp, cp, "setPositionBooleans", isInBeginAreaNew, isInObstacleAreaNew, isInEndAreaNew);
			request(STRICT, cp, car, "setApproachingObstacle", obstacleController.getBinding());
		}
	}
}
