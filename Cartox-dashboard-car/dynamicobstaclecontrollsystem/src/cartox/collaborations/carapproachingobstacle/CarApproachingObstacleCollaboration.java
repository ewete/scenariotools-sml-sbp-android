package cartox.collaborations.carapproachingobstacle;

import sbp.examples.domainmodel.CarController;
import sbp.examples.domainmodel.CoordinatesProcessor;
import sbp.examples.domainmodel.Environment;
import sbp.examples.domainmodel.ObstacleController;
// Framework
import sbp.specification.role.Role;
import sbp.specification.scenarios.Scenario;

// Roles
@SuppressWarnings("serial")
public abstract class CarApproachingObstacleCollaboration extends Scenario {

	// Roles
	protected Role<Environment> env = createRole(Environment.class, "env");
	protected Role<CarController> car = createRole(CarController.class, "car");
	protected Role<ObstacleController> obstacleController = createRole(ObstacleController.class, "obstacleController");
	protected Role<CoordinatesProcessor> cp = createRole(CoordinatesProcessor.class, "cp");

}