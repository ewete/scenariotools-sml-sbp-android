package cartox.collaborations.cardriving;

import sbp.specification.scenarios.violations.Violation;
import sbp.examples.domainmodel.GPSCoordinate;
import sbp.specification.events.Message;


// Collaboration
import cartox.collaborations.cardriving.CarDrivingCollaboration;

// Roles

@SuppressWarnings("serial")
public class CarDriving_CarMovesToNextAreaScenario extends CarDrivingCollaboration {
	
	@Override
	protected void registerAlphabet() {
		setBlocked(env, cp, "currentGPSCoordinateChanged", RANDOM);
	}
	
	@Override
	protected void initialisation() {
		addInitializingMessage(new Message(env, cp, "currentGPSCoordinateChanged", RANDOM ));
	}
	
	@Override
	protected void registerRoleBindings() {
	}

	@Override
	protected void body() throws Violation {
		//true
		GPSCoordinate newCoordinate = (GPSCoordinate) getLastMessage().getParameters().get(0);
		System.out.println("Coordinate is : " + newCoordinate.getLongitudeDegree() + ", " + newCoordinate.getLatitudeDegree());
	}
}
