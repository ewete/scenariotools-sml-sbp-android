package cartox.collaborations.cardriving;

import sbp.examples.domainmodel.CoordinatesProcessor;
import sbp.examples.domainmodel.Environment;
// Framework
import sbp.specification.role.Role;
import sbp.specification.scenarios.Scenario;

@SuppressWarnings("serial")
public abstract class CarDrivingCollaboration extends Scenario {

	// Roles
	protected Role<Environment> env = createRole(Environment.class, "env");
	protected Role<CoordinatesProcessor> cp = createRole(CoordinatesProcessor.class, "cp");

}