package cartox.collaborations.obstacleControllerCarCoordination;

import sbp.examples.domainmodel.CarController;
import sbp.examples.domainmodel.ObstacleController;
// Framework
import sbp.specification.role.Role;
import sbp.specification.scenarios.Scenario;

@SuppressWarnings("serial")
public abstract class ObstacleControllerCarCoordinationCollaboration extends Scenario {

	// Roles
	protected Role<CarController> car = createRole(CarController.class, "car");
	protected Role<ObstacleController> obstacleController = createRole(ObstacleController.class, "obstacleController");

}