package cartox.collaborations.obstacleControllerCarCoordination;

import sbp.specification.events.Message;
import sbp.specification.scenarios.violations.Violation;

@SuppressWarnings("serial")
public class ObstacleControllerCarCoordination_CarAllowedToDrive
		extends ObstacleControllerCarCoordinationCollaboration {

	@Override
	protected void initialisation() {
		// TODO Auto-generated method stub
		addInitializingMessage(new Message(obstacleController, car, "enteringAllowed"));
	}

	@Override
	protected void registerRoleBindings() {
		// TODO Auto-generated method stub
	}

	@Override
	protected void registerAlphabet() {
		// TODO Auto-generated method stub
		setBlocked(new Message(obstacleController, car, "enteringAllowed"));
	}

	@Override
	protected void body() throws Violation {
		// TODO Auto-generated method stub
		obstacleController.getBinding().allowedToDrive(car.getBinding());
	}
}
