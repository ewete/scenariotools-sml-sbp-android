package cartox.collaborations.showinformationondashboard;

import sbp.specification.events.Message;
import sbp.specification.scenarios.violations.Violation;

@SuppressWarnings("serial")
public class ShowInformationOnDashboard_DashboardShowsStopScenario extends ShowInformationOnDashboardCollaboration {

	@Override
	protected void registerAlphabet() {
		setBlocked(obstacleController, car, "enteringDisallowed");
		setBlocked(car, dashboard, "showStop");
	}

	@Override
	protected void initialisation() {
		addInitializingMessage(new Message(obstacleController, car, "enteringDisallowed"));
	}

	@Override
	protected void registerRoleBindings() {
		bindRoleToObject(dashboard, car.getBinding().getDashboard());
	}

	@Override
	protected void body() throws Violation {
		// true
		request(STRICT, car, dashboard, "showStop");
	}
}
