package cartox.collaborations.showinformationondashboard;

import java.util.ArrayList;
import java.util.List;

import sbp.specification.events.Message;
import sbp.specification.scenarios.violations.Violation;

@SuppressWarnings("serial")
public class ShowInformationOnDashboard_DashboardOfCarOnBlockedLaneShowsStopOrGoScenario
		extends ShowInformationOnDashboardCollaboration {

	@Override
	protected void registerAlphabet() {
		setBlocked(cp, car, "setApproachingObstacle", RANDOM);
		setBlocked(car, dashboard, "showGo");
		setBlocked(car, dashboard, "showStop");
	}

	@Override
	protected void initialisation() {
		addInitializingMessage(new Message(cp, car, "setApproachingObstacle", RANDOM));
	}

	@Override
	protected void registerRoleBindings() {
		bindRoleToObject(dashboard, car.getBinding().getDashboard());
	}

	@Override
	protected void body() throws Violation {
		// true
		// Begin Alternative
		List<Message> requestedMessages = new ArrayList<Message>();
		List<Message> waitedForMessages = new ArrayList<Message>();
		if (true) {
			waitedForMessages.add(new Message(STRICT, car, dashboard, "showGo"));
			// true
		}
		if (true) {
			waitedForMessages.add(new Message(STRICT, car, dashboard, "showStop"));
			// true
		}
		// Insert into BP
		doStep(requestedMessages, waitedForMessages);
		// Determine which path has been chosen
		if (getLastMessage().equals(new Message(car, dashboard, "showGo"))) {
			// true
			dashboard.getBinding().showGo();// this is output for console
		} else if (getLastMessage().equals(new Message(car, dashboard, "showStop"))) {
			// true
			dashboard.getBinding().showStop();// this is output for console
		}
		// End Alternative
	}
}
