package cartox.collaborations.showinformationondashboard;

import sbp.examples.domainmodel.CarController;
import sbp.examples.domainmodel.CarDashboard;
import sbp.examples.domainmodel.CoordinatesProcessor;
import sbp.examples.domainmodel.ObstacleController;
// Framework
import sbp.specification.role.Role;
import sbp.specification.scenarios.Scenario;

@SuppressWarnings("serial")
public abstract class ShowInformationOnDashboardCollaboration extends Scenario {

	// Roles
	protected Role<CarController> car = createRole(CarController.class, "car");
	protected Role<CarDashboard> dashboard = createRole(CarDashboard.class, "dashboard");
	protected Role<ObstacleController> obstacleController = createRole(ObstacleController.class, "obstacleController");
	protected Role<CoordinatesProcessor> cp = createRole(CoordinatesProcessor.class, "cp");

}