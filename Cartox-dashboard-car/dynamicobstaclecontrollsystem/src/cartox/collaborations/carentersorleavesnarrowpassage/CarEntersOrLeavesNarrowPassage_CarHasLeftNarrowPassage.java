package cartox.collaborations.carentersorleavesnarrowpassage;

import sbp.examples.domainmodel.GPSCoordinate;
import sbp.specification.events.Message;
import sbp.specification.scenarios.violations.Violation;

@SuppressWarnings("serial")
public class CarEntersOrLeavesNarrowPassage_CarHasLeftNarrowPassage
		extends CarEntersOrLeavesNarrowPassageCollaboration {

	@Override
	protected void registerAlphabet() {
		setBlocked(env, cp, "currentGPSCoordinateChanged", RANDOM);
		setBlocked(cp, car, "hasLeftNarrowPassage");
		setBlocked(cp, cp, "setPositionBooleans", RANDOM);
	}

	@Override
	protected void initialisation() {
		addInitializingMessage(new Message(env, cp, "currentGPSCoordinateChanged", RANDOM));
	}

	@Override
	protected void registerRoleBindings() {
		bindRoleToObject(car, cp.getBinding().getCar());
	}

	@Override
	protected void body() throws Violation {
		
		if ((car.getBinding().getNextOC() == null) || !(cp.getBinding().isInObstacleArea())) {
			throwViolation(INTERRUPT);
		}

		GPSCoordinate positionNew = (GPSCoordinate) getLastMessage().getParameters().get(0);
		boolean isInBeginAreaNew = cp.getBinding().isInBeginArea(positionNew);
		boolean isInEndAreaNew = cp.getBinding().isInEndArea(positionNew);
		boolean isInObstacleAreaNew = cp.getBinding().isInObstacleArea(positionNew);

		request(STRICT, cp, cp, "setPositionBooleans", isInBeginAreaNew, isInObstacleAreaNew, isInEndAreaNew);

		if (!isInObstacleAreaNew) {
			request(STRICT, cp, car, "hasLeftNarrowPassage");
		}
	}
}
