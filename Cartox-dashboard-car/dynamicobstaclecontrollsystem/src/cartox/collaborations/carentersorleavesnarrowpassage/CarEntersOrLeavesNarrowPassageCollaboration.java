package cartox.collaborations.carentersorleavesnarrowpassage;

import sbp.examples.domainmodel.CarController;
import sbp.examples.domainmodel.CoordinatesProcessor;
import sbp.examples.domainmodel.Environment;
// Framework
import sbp.specification.role.Role;
import sbp.specification.scenarios.Scenario;

@SuppressWarnings("serial")
public abstract class CarEntersOrLeavesNarrowPassageCollaboration extends Scenario {

	// Roles
	protected Role<Environment> env = createRole(Environment.class, "env");
	protected Role<CarController> car = createRole(CarController.class, "car");
	protected Role<CoordinatesProcessor> cp = createRole(CoordinatesProcessor.class, "cp");

}