package cartox.collaborations.carentersorleavesnarrowpassage;

import sbp.examples.domainmodel.GPSCoordinate;
import sbp.specification.events.Message;
import sbp.specification.scenarios.violations.Violation;

@SuppressWarnings("serial")
public class CarEntersOrLeavesNarrowPassage_CarEntersNarrowPassage
		extends CarEntersOrLeavesNarrowPassageCollaboration {

	@Override
	protected void registerAlphabet() {
		setBlocked(env, cp, "currentGPSCoordinateChanged", RANDOM);
		setBlocked(cp, car, "hasEnteredNarrowPassage");
		setBlocked(cp, cp, "setPositionBooleans", RANDOM);
	}

	@Override
	protected void initialisation() {
		addInitializingMessage(new Message(env, cp, "currentGPSCoordinateChanged", RANDOM));
	}

	@Override
	protected void registerRoleBindings() {
		bindRoleToObject(car, cp.getBinding().getCar());
	}

	@Override
	protected void body() throws Violation {
		
		
		/*
		 * if       the next ObstacleControll is null
		 *    ORDER the car is in ObstacleControl Area (this condition is important)
		 *         (without this condition, this scenario will be triggered once again,
		 *          if the car moves in the Narrowpassage)
		 *    ORDER (the car is NOT in Begin-Area AND the car is NOT in End-Area)  
		 * then the violation of interrupt will be thrown
		 * */
		if ((car.getBinding().getNextOC() == null) || (cp.getBinding().isInObstacleArea()
				|| (!cp.getBinding().isInBeginArea() && !cp.getBinding().isInEndArea()))) {
			throwViolation(INTERRUPT);
		}
		// true

		GPSCoordinate positionNew = (GPSCoordinate) getLastMessage().getParameters().get(0);
		boolean isInBeginAreaNew = cp.getBinding().isInBeginArea(positionNew);
		boolean isInEndAreaNew = cp.getBinding().isInEndArea(positionNew);
		boolean isInObstacleAreaNew = cp.getBinding().isInObstacleArea(positionNew);

		request(STRICT, cp, cp, "setPositionBooleans", isInBeginAreaNew, isInObstacleAreaNew, isInEndAreaNew);
		
		if (isInObstacleAreaNew) {
			request(STRICT, cp, car, "hasEnteredNarrowPassage");
		}
	}
}
