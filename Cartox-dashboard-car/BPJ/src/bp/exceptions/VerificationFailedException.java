package bp.exceptions;


public class VerificationFailedException extends Exception {

	private static final long serialVersionUID = -9016596683250465617L;
	private String shortMessage = null;

	public VerificationFailedException(String description) {
		super(description);
	}

	public void setShortMessage(String shortMsg) {
		shortMessage = shortMsg;
	}

	public String getShortMessage() {
		return shortMessage;
	}
}
