package de.se.uni.hannover.cartox_dashboard_car;

import android.os.AsyncTask;

import sbp.examples.domainmodel.CarController;

public class RunSpecificationTask extends AsyncTask<Class<? extends ACarToXRunconfig_Dashboard_TwoCars>, Void, CarController> {

    private final DashboardActivity dashboardActivity;
    private final Callback mCallback;
    private Exception mException;
    private MQTTDistributedRuntimeAdapter mqttAdapter;

    public RunSpecificationTask(DashboardActivity dashboardActivity, Callback callback) {
        this.dashboardActivity = dashboardActivity;
        this.mCallback = callback;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        if (mException != null) {
            mCallback.onError(mException);
        } else {
            mCallback.onStart();
        }
    }

    @Override
    protected void onPostExecute(CarController result) {
        super.onPostExecute(result);
        if (mException != null) {
            mCallback.onError(mException);
        } else {
            mCallback.onComplete(result);
        }
    }

    @Override
    protected CarController doInBackground(Class<? extends ACarToXRunconfig_Dashboard_TwoCars>... params) {
        try {
            if (params.length > 0) {
                ACarToXRunconfig_Dashboard_TwoCars.main(dashboardActivity, params[0]);
                mqttAdapter = (MQTTDistributedRuntimeAdapter) ACarToXRunconfig_Dashboard_TwoCars.getInstance().getAdapter();
                return ((ACarToXRunconfig_Dashboard_TwoCars) ACarToXRunconfig_Dashboard_TwoCars.getInstance()).getCarController();
            }
        } catch (Exception e) {
            mException = e;
        }
        return null;
    }

    public MQTTDistributedRuntimeAdapter getMqttAdapter() {
        return mqttAdapter;
    }

    public interface Callback {
        void onComplete(CarController result);

        void onError(Exception e);

        void onStart();
    }
}
