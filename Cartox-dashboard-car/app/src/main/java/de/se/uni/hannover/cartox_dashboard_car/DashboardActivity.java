package de.se.uni.hannover.cartox_dashboard_car;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.widget.EditText;
import android.widget.Scroller;
import android.widget.TextView;

import java.util.concurrent.ExecutionException;

import sbp.examples.domainmodel.CarController;

public class DashboardActivity extends AppCompatActivity {

    private static final int ACCESS_PERMS_REQUEST = 1337;
    private static final String[] ACCES_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private EditText mLogsEditText;
    private RunSpecificationTask specificationTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        setContentView(R.layout.activity_dashboard);

        TextView mStatusTextView = (TextView) findViewById(R.id.tv_status);
        mStatusTextView.setText(R.string.tv_status_driving);

        this.mLogsEditText = (EditText) findViewById(R.id.et_log);
        this.mLogsEditText.setScroller(new Scroller(getApplicationContext()));
        this.mLogsEditText.setVerticalScrollBarEnabled(true);
        this.mLogsEditText.setMovementMethod(new ScrollingMovementMethod());
        this.mLogsEditText.setKeyListener(null);
        this.mLogsEditText.setTextIsSelectable(true);

        specificationTask = new RunSpecificationTask(this, new RunSpecificationTask.Callback() {
            @Override
            public void onComplete(CarController result) {
                logMessage("RunSpecificationTask onComplete!");
                if (!canAccessLocation()) {
                    if (Build.VERSION.SDK_INT >= 23) {
                        requestPermissions(ACCES_PERMS, ACCESS_PERMS_REQUEST);
                    } else {
                        logMessage("Build.VERSION.SDK_INT < 23 : Access to GPS denied!");
                    }
                } else {
                    initializeLocationManager();
                }
            }

            @Override
            public void onError(Exception e) {
                logMessage(e.getMessage());
            }

            @Override
            public void onStart() {
                logMessage("RunSpecificationTask onStart!");
            }

        });
        //specificationTask.execute(CarToXRunconfig_JamesDashboard_TwoCars.class);
        specificationTask.execute(CarToXRunconfig_AnnasDashboard_TwoCars.class);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case ACCESS_PERMS_REQUEST:
                if (canAccessLocation()) {
                    initializeLocationManager();
                } else {
                    logMessage("Access to GPS denied!");
                }
                break;
        }
    }

    private void initializeLocationManager() {
        final LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        final CustomLocationListener customLocationListener = new CustomLocationListener(this);
        customLocationListener.setMqttAdapter(specificationTask.getMqttAdapter());
        try {
            CarController carController = specificationTask.get();
            customLocationListener.setCarController(carController);
            int minTimeInMillisec = 0, minDistanceInMeters = 1;
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, minTimeInMillisec, minDistanceInMeters, customLocationListener);
                logMessage("GPS_PROVIDER enabled.");
            } else if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                logMessage("NETWORK_PROVIDER enabled.");
                if (Build.VERSION.SDK_INT >= 23) {
                    if (this.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                            && this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        logMessage("Access to GPS denied!");
                    } else {
                        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, minTimeInMillisec, minDistanceInMeters, customLocationListener);
                    }
                } else {
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, minTimeInMillisec, minDistanceInMeters, customLocationListener);
                }
            } else {
                logMessage("Impossible to get GPS data");
            }
        } catch (InterruptedException | ExecutionException e) {
            logMessage(e.getMessage());
        }
    }

    public void logMessage(final String message) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String sb = mLogsEditText.getText().append("\n").append(message).toString();
                mLogsEditText.setText(sb);
            }
        });
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION) || hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (Build.VERSION.SDK_INT < 23) || (Build.VERSION.SDK_INT >= 23 && PackageManager.PERMISSION_GRANTED == checkSelfPermission(perm));
    }
}
