package de.se.uni.hannover.cartox_dashboard_car.observers;

import android.graphics.Color;
import android.widget.TextView;

import de.se.uni.hannover.cartox_dashboard_car.DashboardActivity;
import de.se.uni.hannover.cartox_dashboard_car.R;
import sbp.examples.domainmodel.CarController;
import sbp.examples.domainmodel.CarDashboard;
import sbp.examples.domainmodel.CarToXSystem;
import sbp.specification.events.Message;
import sbp.specification.role.Role;
import sbp.specification.scenarios.Scenario;
import sbp.specification.scenarios.utils.ScenarioObserver;

public class DashboardObserver_TwoCars extends ScenarioObserver {
    private DashboardActivity dashboardActivity;
    private CarController carController;

    public DashboardObserver_TwoCars(DashboardActivity dashboardActivity, CarController carController) {
        this.dashboardActivity = dashboardActivity;
        this.carController = carController;
    }

    @Override
    public void trigger(Scenario scenario, Message messageEvent) {
        Role receiver = messageEvent.getReceiver();
        String message = messageEvent.getMessage();
        if (receiver.getBinding() instanceof CarDashboard) {
            CarDashboard dashboard = (CarDashboard) receiver.getBinding();
            if (dashboard.equals(carController.getDashboard())) {
                if (message.equals("showGo")) {
                    showGo();
                } else if (message.equals("showStop")) {
                    showStop();
                }
            }
        }
    }

    private void showGo() {
        final TextView label = (TextView) this.dashboardActivity.findViewById(R.id.tv_status);
        this.dashboardActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                label.setText(R.string.tv_status_go);
                label.setTextColor(Color.GREEN);
            }
        });
    }

    private void showStop() {
        final TextView label = (TextView) this.dashboardActivity.findViewById(R.id.tv_status);
        this.dashboardActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                label.setText(R.string.tv_status_stop);
                label.setTextColor(Color.RED);
            }
        });
    }
}
