package de.se.uni.hannover.cartox_dashboard_car;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.io.UnsupportedEncodingException;

import sbp.examples.domainmodel.CarToXSystem;
import sbp.examples.networkadapter.IPSettings;
import sbp.examples.networkadapter.ObjectsTypeAdapter;
import sbp.runtime.ObjectRegistry;
import sbp.runtime.SpecificationRunconfig;
import sbp.runtime.adapter.AbstractRuntimeAdapter;
import sbp.runtime.network.exceptions.ClientByeException;
import sbp.runtime.network.messages.Messages;
import sbp.runtime.settings.Settings;
import sbp.specification.events.Message;

public class MQTTDistributedRuntimeAdapter extends AbstractRuntimeAdapter {

    private CarToXSystem carToXSystem;

    private String clientId;
    private MqttAndroidClient senderMqttClient;
    private MqttAndroidClient receiverMqttClient;

    private DashboardActivity dashboardActivity;

    public MQTTDistributedRuntimeAdapter(SpecificationRunconfig<?> specificationRunconfig, DashboardActivity dashboardActivity) {
        super(specificationRunconfig);
        clientId = MqttClient.generateClientId();
        this.dashboardActivity = dashboardActivity;
    }

    @Override
    public void run() {
        startServer();
        connectClients();
    }

    @Override
    public void startServer() {
        try {
            MemoryPersistence persistenceReceiverMqttClient = new MemoryPersistence();
            receiverMqttClient = new MqttAndroidClient(dashboardActivity.getApplicationContext(), IPSettings.BROCKER_URL_CONNECTION, clientId + "_Receiver",
                    persistenceReceiverMqttClient);
            receiverMqttClient.setCallback(new MqttCallback() {

                @Override
                public void connectionLost(Throwable throwable) {
                    final String message = String.format("receiverMqttClient connection Lost %s", throwable.getMessage());
                    Settings.NETWORK_OUT.println(message);
                    dashboardActivity.logMessage(message);
                    throwable.printStackTrace();
                }

                @Override
                public void messageArrived(String t, MqttMessage m) throws Exception {
                    String messageString = new String(m.getPayload(), "UTF-8");
                    if (messageString.equals(Messages.BYE.toString())) {
                        throw new ClientByeException();
                    } else {
                        Message messageEvent = deserializeForNetwork(messageString);
                        receive(messageEvent);
                    }
                }

                @Override
                public void deliveryComplete(IMqttDeliveryToken t) {
                }
            });
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            receiverMqttClient.connect(connOpts, null, new IMqttActionListener() {

                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    final String message = String.format("receiverMqttClient connected to broker %s", IPSettings.BROCKER_URL_CONNECTION);
                    Settings.NETWORK_OUT.println(message);
                    dashboardActivity.logMessage(message);
                    try {
                        IMqttToken subToken = receiverMqttClient.subscribe(IPSettings.TOPIC, IPSettings.QOS);
                        subToken.setActionCallback(new IMqttActionListener() {
                            @Override
                            public void onSuccess(IMqttToken asyncActionToken) {
                                final String message = String.format("receiverMqttClient subscribed to topic %s", IPSettings.TOPIC);
                                Settings.NETWORK_OUT.println(message);
                                dashboardActivity.logMessage(message);
                            }

                            @Override
                            public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                                Settings.NETWORK_OUT.println(exception.getMessage());
                                dashboardActivity.logMessage(exception.getMessage());
                                exception.printStackTrace();
                            }
                        });
                    } catch (MqttException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Settings.NETWORK_OUT.println(exception.getMessage());
                    dashboardActivity.logMessage(exception.getMessage());
                    exception.printStackTrace();
                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void connectClients() {
        try {
            MemoryPersistence persistenceSenderMqttClient = new MemoryPersistence();
            senderMqttClient = new MqttAndroidClient(dashboardActivity.getApplicationContext(), IPSettings.BROCKER_URL_CONNECTION, clientId + "_Sender",
                    persistenceSenderMqttClient);
            senderMqttClient.setCallback(new MqttCallback() {
                @Override
                public void connectionLost(Throwable throwable) {
                    final String message = String.format("senderMqttClient connection Lost %s", throwable.getMessage());
                    Settings.NETWORK_OUT.println(message);
                    dashboardActivity.logMessage(message);
                    throwable.printStackTrace();
                }

                @Override
                public void messageArrived(String t, MqttMessage m) throws Exception {
                }

                @Override
                public void deliveryComplete(IMqttDeliveryToken t) {
                    final String message = String.format("senderMqttClient's message delivered with id %s", t.getMessageId());
                    Settings.NETWORK_OUT.println(message);
                    dashboardActivity.logMessage(message);
                }
            });
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            senderMqttClient.connect(connOpts, null, new IMqttActionListener() {

                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    final String message = String.format("senderMqttClient connected to broker: %s", IPSettings.BROCKER_URL_CONNECTION);
                    Settings.NETWORK_OUT.println(message);
                    dashboardActivity.logMessage(message);
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Settings.NETWORK_OUT.println(exception.getMessage());
                    dashboardActivity.logMessage(exception.getMessage());
                    exception.printStackTrace();
                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void publish(Message event) {
        try {
            dashboardActivity.logMessage("Wird vlt. Echo (publish): " + event.getName());
            MqttMessage message = new MqttMessage(serializeForNetwork(event).getBytes("UTF-8"));
            message.setQos(IPSettings.QOS);
            senderMqttClient.publish(IPSettings.TOPIC, message);
        } catch (MqttPersistenceException e) {
            e.printStackTrace();
        } catch (MqttException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void receive(Message event) {
        if (ObjectRegistry.getInstance().isControllable(event.getSender().getBinding())) {
            final String message = String.format("Ignoring (Already enqueued) : %s", event.getName());
            Settings.NETWORK_OUT.println(message);
            dashboardActivity.logMessage(message);
        } else {
            getEventQueueScenario().enqueueEvent(event);
        }
    }

    private String serializeForNetwork(Message event) {
        // Settings.NETWORK_OUT.println("Serialize : " + event);
        String out = ObjectsTypeAdapter.getInstance(carToXSystem).getGsonBuilder().create().toJson(event, Message.class);
        // Settings.NETWORK_OUT.println("Serialized to : " + out);
        // Settings.NETWORK_OUT.println("--- End serialization ---");

        // printToFile("serializeForNetwork", event, out);
        return out;
    }

    private Message deserializeForNetwork(String event) {
        // Settings.NETWORK_OUT.println("deserialize : " + event);
        Message out = ObjectsTypeAdapter.getInstance(carToXSystem).getGsonBuilder().create().fromJson(event, Message.class);
        // Settings.NETWORK_OUT.println("deserialized to : " + out);
        // Settings.NETWORK_OUT.println("--- End deserialization ---");

        // printToFile("deserializeForNetwork", out, event);
        return out;
    }

    public void setCarToXSystem(CarToXSystem carToXSystem) {
        this.carToXSystem = carToXSystem;
    }

}
