package de.se.uni.hannover.cartox_dashboard_car;

import de.se.uni.hannover.cartox_dashboard_car.observers.DashboardObserver_TwoCars;
import sbp.examples.domainmodel.CarController;
import sbp.examples.domainmodel.CarToXSystem;
import sbp.examples.runtime.CarToXSystemWrapper;
import sbp.examples.specification.DynamicOCSpecification;
import sbp.runtime.SpecificationRunconfig;
import sbp.runtime.settings.Settings;

public abstract class ACarToXRunconfig_Dashboard_TwoCars extends SpecificationRunconfig<DynamicOCSpecification> {

    protected static DashboardActivity dashboardActivity;

    public ACarToXRunconfig_Dashboard_TwoCars() {
        super(new DynamicOCSpecification());
        MQTTDistributedRuntimeAdapter mqttAdapter = new MQTTDistributedRuntimeAdapter(this, dashboardActivity);
        mqttAdapter.setCarToXSystem(CarToXSystemWrapper.getCarToXSystemTowCars());
        setAdapter(mqttAdapter);
        //Settings.setRuntimeOut(System.out);
        //Settings.setServerOut(System.out);
        Settings.SPECTATOR__PRINT_CAUGHT_MESSAGE = true;
    }

    @Override
    protected final void registerNetworkAdressesForObjects() {
    }

    public final static void main(DashboardActivity dashboard, Class<? extends ACarToXRunconfig_Dashboard_TwoCars> specificationRunconfigClass){
        dashboardActivity = dashboard;
        SpecificationRunconfig.run(specificationRunconfigClass);
    }

    public abstract CarController getCarController();
}
