package de.se.uni.hannover.cartox_dashboard_car;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;

import java.util.Locale;

import sbp.examples.domainmodel.CarController;
import sbp.examples.domainmodel.CoordinatesProcessor;
import sbp.examples.domainmodel.Environment;
import sbp.examples.domainmodel.GPSCoordinate;
import sbp.runtime.settings.Settings;
import sbp.specification.events.Message;
import sbp.specification.role.Role;

/**
 * Created by ericwete on 23.05.2017.
 */

public class CustomLocationListener implements LocationListener {

    private MQTTDistributedRuntimeAdapter mqttAdapter;
    private CarController carController;

    private DashboardActivity dashboardActivity;

    public CustomLocationListener(DashboardActivity dashboardActivity) {
        this.dashboardActivity = dashboardActivity;
    }

    @Override
    public void onLocationChanged(Location location) {
        Role<Environment> environmentRole = new Role<>(Environment.class, "env");
        Role<CoordinatesProcessor> coordinatesProcessorRole = new Role<>(CoordinatesProcessor.class, "cp");

        environmentRole.setBinding(carController.getEnv());
        CoordinatesProcessor coordinatesProcessor = carController.getCoordinateProcessor();
        coordinatesProcessorRole.setBinding(coordinatesProcessor);
        double longitude = location.getLongitude();
        double latitude = location.getLatitude();
        GPSCoordinate gpsCoordinate = new GPSCoordinate(longitude, latitude);
        Message event = new Message(environmentRole, coordinatesProcessorRole, "currentGPSCoordinateChanged", gpsCoordinate);
        //dashboardActivity.logMessage("Locationlistener (to receive and publish) : " + event.getName());
        mqttAdapter.receive(event);
        mqttAdapter.publish(event);
    }

    public void setCarController(CarController carController) {
        this.carController = carController;
    }

    public void setMqttAdapter(MQTTDistributedRuntimeAdapter mqttAdapter) {
        this.mqttAdapter = mqttAdapter;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }
}
