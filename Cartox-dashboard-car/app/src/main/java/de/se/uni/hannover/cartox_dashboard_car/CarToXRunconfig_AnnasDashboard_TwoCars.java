package de.se.uni.hannover.cartox_dashboard_car;

import de.se.uni.hannover.cartox_dashboard_car.observers.DashboardObserver_TwoCars;
import sbp.examples.domainmodel.CarController;
import sbp.examples.domainmodel.CarToXSystem;
import sbp.examples.runtime.CarToXSystemWrapper;

public class CarToXRunconfig_AnnasDashboard_TwoCars extends ACarToXRunconfig_Dashboard_TwoCars {

    public CarToXRunconfig_AnnasDashboard_TwoCars() {
        super();
    }

    @Override
    protected void registerParticipatingObjects() {
        CarToXSystem carToXSystem = CarToXSystemWrapper.getCarToXSystemTowCars();
        // StreetsectionControll and its ObstacleControll
        registerObject(carToXSystem.getStreetSectionControllers().get(0), UNCONTROLLABLE);
        registerObject(carToXSystem.getStreetSectionControllers().get(0).getOc().get(0), UNCONTROLLABLE);
        // Car and Dashboard and CoordinatesProcessor : James
        registerObject(carToXSystem.getCarControllers().get(0), UNCONTROLLABLE);
        registerObject(carToXSystem.getCarControllers().get(0).getDashboard(), UNCONTROLLABLE);
        registerObject(carToXSystem.getCarControllers().get(0).getCoordinateProcessor(), UNCONTROLLABLE);
        // Car and Dashboard and CoordinatesProcessor : Annas
        registerObject(carToXSystem.getCarControllers().get(1), CONTROLLABLE);
        registerObject(carToXSystem.getCarControllers().get(1).getDashboard(), CONTROLLABLE);
        registerObject(carToXSystem.getCarControllers().get(1).getCoordinateProcessor(), CONTROLLABLE);
        // Environment
        registerObject(carToXSystem.getEnv(), UNCONTROLLABLE);
    }

    @Override
    protected void registerObservers() {
        DashboardObserver_TwoCars dashboardObserver = new DashboardObserver_TwoCars(dashboardActivity, getCarController());
        addScenarioObserver(dashboardObserver);
    }

    @Override
    public CarController getCarController() {
        return CarToXSystemWrapper.getCarToXSystemTowCars().getCarControllers().get(1);
    }
}
