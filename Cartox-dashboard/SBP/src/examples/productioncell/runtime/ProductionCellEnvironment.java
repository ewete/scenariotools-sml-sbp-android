package examples.productioncell.runtime;

import examples.productioncell.model.Controller;
import examples.productioncell.model.TableSensor;
import sbp.runtime.adapter.RuntimeAdapter;
import sbp.specification.events.Message;
import sbp.specification.role.Role;

public class ProductionCellEnvironment extends Thread {

	private RuntimeAdapter adapter;

	private Role c = new Role(Controller.class, "c");
	private Role ts = new Role(TableSensor.class, "ts");

	public ProductionCellEnvironment(RuntimeAdapter adapter) {
		this.adapter = adapter;
	}

	@Override
	public void run() {
		while (true) {
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			adapter.receive(new Message(false, true, ts, c, "blankArrived"));
		}
	}

}
