package examples.productioncell.runtime;

import java.io.PrintStream;

import sbp.runtime.settings.Settings;

public class ProductionCellSettings {

	public static void setSettings(PrintStream runtimeStream, PrintStream serverStream) {
		Settings.setRuntimeOut(runtimeStream);
		Settings.setServerOut(serverStream);
		Settings.IDLE__PRINT_SUPERSTEP_IN_CONSOLE = true;
		Settings.SPECTATOR__PRINT_CAUGHT_MESSAGE = true;
		Settings.EXECUTOR__PRINT_ON_METHOD_EXECUTION = true;
		// Settings.PRINT_SPECIFICATION_SCENARIO_PROGRESS = true;
		// Settings.PRINT_ASSUMPTION_SCENARIO_PROGRESS = true;
		// Settings.SPECTATOR_PRINT_ACTIVE_ASSUMPTION_SCENARIOS = true;
		// Settings.SPECTATOR_PRINT_ACTIVE_SPECIFICATION_SCENARIOS = true;
	}

}
