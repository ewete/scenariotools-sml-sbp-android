package examples.productioncell.runtime;

import examples.productioncell.model.ArmA;
import examples.productioncell.model.Controller;
import examples.productioncell.model.TableSensor;

public class ProductionCellObjectSystem {

	private static ProductionCellObjectSystem instance;

	public Controller controller;
	public TableSensor tableSensor;
	public ArmA armA;

	public static ProductionCellObjectSystem getInstance() {
		if (instance == null) {
			instance = new ProductionCellObjectSystem();
		}
		return instance;
	}

	public ProductionCellObjectSystem() {
		setupObjectSystem();
	}

	private void setupObjectSystem() {
		controller = new Controller();
		controller.setName("ctrl");
		tableSensor = new TableSensor();
		tableSensor.setName("ts");
		armA = new ArmA();
		armA.setName("armA");
		controller.setArmA(armA);
		controller.setTableSensor(tableSensor);
	}

}
