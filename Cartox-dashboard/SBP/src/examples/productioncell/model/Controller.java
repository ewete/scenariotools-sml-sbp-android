package examples.productioncell.model;

public class Controller extends NamedElement {
	private ArmA armA;
	private TableSensor tableSensor;

	public ArmA getArmA() {
		return armA;
	}

	public void setArmA(ArmA armA) {
		this.armA = armA;
	}

	public TableSensor getTableSensor() {
		return tableSensor;
	}

	public void setTableSensor(TableSensor tableSensor) {
		this.tableSensor = tableSensor;
	}

	public void blankArrived(boolean p) {
		if (p)
			System.out.println("Controller: There is a blank on the table!");
	}

}
