package examples.productioncell.specification;

import examples.productioncell.collaborations.productionCell.MoveToPressAfterPickUp;
import examples.productioncell.collaborations.productionCell.PickUpOnBlankArrived;
import examples.productioncell.collaborations.productionCell.RespondAfterMoveToPress;
import examples.productioncell.collaborations.productionCell.RespondAfterPickUp;
import sbp.specification.Specification;

public class ProductionCellSpecification extends Specification {

	@Override
	protected void registerScenarios() {
		registerSpecificationScenario(PickUpOnBlankArrived.class);
		registerSpecificationScenario(MoveToPressAfterPickUp.class);
		registerSpecificationScenario(RespondAfterPickUp.class);
		registerSpecificationScenario(RespondAfterMoveToPress.class);
	}

	@Override
	protected void registerTransformationRules() {
		
	}

}
