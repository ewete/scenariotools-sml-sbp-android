package sbp.runtime.adapter;

import java.io.IOException;

import sbp.runtime.ObjectRegistry;
import sbp.runtime.SpecificationRunconfig;
import sbp.runtime.network.NetworkEntry;
import sbp.runtime.network.client.Client;
import sbp.runtime.network.exceptions.UnableToConnectException;
import sbp.runtime.network.server.Server;
import sbp.specification.events.Message;

public class DistributedRuntimeAdapter extends AbstractRuntimeAdapter {

	public DistributedRuntimeAdapter(SpecificationRunconfig<?> specificationRunconfig, int port) {
		super(specificationRunconfig);
		this.port = port;
	}

	private Client client;
	private Server server;
	private int port;

	@Override
	public void run() {
		startServer();
		connectClients();
	}

	@Override
	public void startServer() {
		server = new Server(port);
		server.setEventQueueScenario(getEventQueueScenario());
		server.start();
	}

	@Override
	public void connectClients() {
		client = new Client();
		try {
			for (NetworkEntry networkEntry : ObjectRegistry.getInstance().getNetworkAddresses()) {
				if (!client.isConnectedTo(networkEntry.getHostName(), networkEntry.getPort())) {
					Object object = ObjectRegistry.getInstance().getObjectNetworkMapper().getNetworkToEntryObjectMap()
							.get(networkEntry);
					client.connect(networkEntry.getHostName(), networkEntry.getPort(), object);
					ObjectRegistry.getInstance().getObjectNetworkMapper().setClientConnected(object, true);
				}
			}
		} catch (IOException | UnableToConnectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void publish(Message event) {
		try {
			client.publish(event.serializeForNetwork());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void receive(Message event) {
		getEventQueueScenario().enqueueEvent(event);
	}

}
