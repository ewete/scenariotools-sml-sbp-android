package sbp.runtime.adapter;

import sbp.runtime.SpecificationRunconfig;
import sbp.specification.events.Message;
import sbp.specification.scenarios.system.EventQueueScenario;

public interface RuntimeAdapter {

	SpecificationRunconfig<?> getSpecificationRunconfig();

	void setSpecificationRunconfig(SpecificationRunconfig<?> specificationRunconfig);

	EventQueueScenario getEventQueueScenario();

	void run();

	void startServer();
	
	void connectClients();

	void publish(Message event);
	
	void receive(Message event);

}
