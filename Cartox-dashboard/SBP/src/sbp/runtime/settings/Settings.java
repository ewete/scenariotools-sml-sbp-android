package sbp.runtime.settings;

import java.io.PrintStream;

public class Settings {

	// SpectatorScenario
	public static int SPECTATOR__SLEEP_TIME = 1;
	public static boolean SPECTATOR__PRINT_CAUGHT_MESSAGE = false;
	public static boolean SPECTATOR__PRINT_ACTIVE_SPECIFICATION_SCENARIOS = false;
	public static boolean SPECTATOR__PRINT_ACTIVE_ASSUMPTION_SCENARIOS = false;
	public static boolean SPECTATOR__PRINT_ACTIVE_SYSTEM_SCENARIOS = false;

	// IdleScenario
	public static boolean IDLE__PRINT_SUPERSTEP_IN_CONSOLE = true;

	// Executor
	public static boolean EXECUTOR__PRINT_ON_METHOD_EXECUTION = false;
	public static boolean EXECUTOR__PRINT_ON_RULE_EXECUTION = false;
	public static boolean EXECUTOR__EXECUTE_METHODS = false;
	public static boolean EXECUTOR__EXECUTE_SETTERS = true;
	public static boolean EXECUTOR__EXECUTE_RULES = true;

	// Scenario
	public static boolean SCENARIO__PRINT_SYSTEM_PROGRESS = false;
	public static boolean SCENARIO__PRINT_SPECIFICATION_PROGRESS = false;
	public static boolean SCENARIO__PRINT_ASSUMPTION_PROGRESS = false;

	// PrintStreams
	public static PrintStream SPECTATOR_OUT = System.out;
	public static PrintStream EXECUTOR_OUT = System.out;
	public static PrintStream IDLE_OUT = System.out;
	public static PrintStream SCENARIO_OUT = System.out;
	public static PrintStream NETWORK_OUT = System.out;

	public static void setOut(PrintStream printStream) {
		System.setOut(printStream);
	}

	public static void setRuntimeOut(PrintStream printStream) {
		SPECTATOR_OUT = printStream;
		EXECUTOR_OUT = printStream;
		IDLE_OUT = printStream;
		SCENARIO_OUT = printStream;
	}

	public static void setServerOut(PrintStream serverStream) {
		NETWORK_OUT = serverStream;
	}

	public static boolean DEBUG__ENABLE_SUPERSTEP_PERFORMANCE_TEST = false;

}
