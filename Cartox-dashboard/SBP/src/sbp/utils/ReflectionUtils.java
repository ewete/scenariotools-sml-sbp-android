package sbp.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReflectionUtils {

	public static String findNameForObject(Object object) {
		String name = "";
		if (object == null) {
			return null;
		}
		try {
			Class<?> c = object.getClass();
			Field field = recursiveGetNameField(c);
			field.setAccessible(true);
			Object value = field.get(object);
			if (value instanceof String) {
				name = (String) value;
			}
		} catch (SecurityException | IllegalArgumentException | IllegalAccessException | NoSuchFieldException e) {
			name = object.toString();
			// e.printStackTrace();
		}
		return name;
	}

	private static Field recursiveGetNameField(Class<?> c) throws NoSuchFieldException {
		Field field = null;
		try {
			field = c.getDeclaredField("name");
		} catch (NoSuchFieldException | SecurityException e) {
			Class<?> superClass = c.getSuperclass();
			if (superClass != null) {
				field = recursiveGetNameField(superClass);
			}
		}
		if (field == null) {
			throw new NoSuchFieldException();
		}
		return field;
	}

	public static List<Class<?>> getAllSuperClasses(Class<?> c) {
		List<Class<?>> superClasses = new ArrayList<Class<?>>();

		while (c.getSuperclass() != null) {
			superClasses.add(c.getSuperclass());
			c = c.getSuperclass();
		}

		return superClasses;
	}

	public static List<Class<?>> getAllInterfaces(Class<?> c) {
		List<Class<?>> interfaces = new ArrayList<Class<?>>();

		interfaces.addAll(Arrays.asList(c.getInterfaces()));

		if (c.isInterface()) {
			interfaces.add(c);
		}

		while (c.getSuperclass() != null) {
			interfaces.addAll(Arrays.asList(c.getInterfaces()));
			c = c.getSuperclass();
		}

		return interfaces;
	}

}
