package sbp.specification.events;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ParameterRandom implements Serializable {

	private static ParameterRandom instance;

	public static ParameterRandom getInstance() {
		if (instance == null) {
			instance = new ParameterRandom();
		}
		return instance;
	}

}
