package sbp.specification.role;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import sbp.exceptions.RoleTypeNotSpecifiedException;
import sbp.runtime.ObjectRegistry;
import sbp.utils.ListUtils;
import sbp.utils.ReflectionUtils;

public class Role<T> implements Serializable {

	public static final Role<IdleRole> IDLE = new IdleRole();

	// private T binding;
	private int bindingID = -1;

	protected String name = "";
	protected Class<T> type;

	public Role(Class<T> type, String name) {
		setName(name);
		this.type = type;
	}

	@SuppressWarnings("unchecked")
	public Role(String name) {
		setName(name);
		try {
			this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
		} catch (ClassCastException e) {
			throw new RoleTypeNotSpecifiedException(this);
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Class<?> getType() {
		return type;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Role) {
			@SuppressWarnings("unchecked")
			Role<T> other = (Role<T>) obj;

			if (other.getType() == this.getType()) {
				return true;
			}
			if (ReflectionUtils.getAllSuperClasses(other.getType()).contains(this.getType())) {
				return true;
			}
			if (ReflectionUtils.getAllInterfaces(this.getType()).contains(other.getType())) {
				return true;
			}
			List<Class<?>> otherInterfaces = ListUtils.arrayToList(other.getType().getInterfaces());
			if (otherInterfaces.contains(this.getType())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String toString() {
		return getName() + "[" + ReflectionUtils.findNameForObject(getBinding()) + "]";// +
																						// "#"
																						// +
																						// System.identityHashCode(this);
	}

	public T getBinding() {
		// return binding;
		return (T) ObjectRegistry.getInstance().getObject(bindingID);
	}

	public void setBinding(T binding) {
		// this.binding = binding;
		this.bindingID = ObjectRegistry.getInstance().getID(binding);
	}

	public boolean hasBinding() {
		return getBinding() != null;
	}

	public void copyBinding(Role<T> other) {
		if (this.equals(other)) {
			setBinding((T) other.getBinding());
		}
	}

	public void reset() {
		setBinding(null);
	}

}
