package sbp.specification.scenarios.system;

import java.util.HashSet;
import java.util.Set;

import sbp.runtime.settings.Settings;
import sbp.specification.events.Message;
import sbp.specification.scenarios.Scenario;
import sbp.specification.scenarios.violations.Violation;

public class SpectatorScenario extends SystemScenario {

	private static final long serialVersionUID = 2975094940756638926L;

	@Override
	protected void body() throws Violation {
		// Wait for events
		waitForAnything();
		// Sleep to refresh bp.getAllBThreads()
		if (Settings.SPECTATOR__PRINT_ACTIVE_SPECIFICATION_SCENARIOS
				|| Settings.SPECTATOR__PRINT_ACTIVE_SYSTEM_SCENARIOS
				|| Settings.SPECTATOR__PRINT_ACTIVE_ASSUMPTION_SCENARIOS) {
			sleep(Settings.SPECTATOR__SLEEP_TIME);
		}
		// Process event
		onMessageReceived();
		notifyObservers();
	}

	protected void onMessageReceived() {
		if (getLastMessage().equals(Message.IDLE)) {
			return;
		}

		if (Settings.SPECTATOR__PRINT_CAUGHT_MESSAGE) {
			Settings.SPECTATOR_OUT.println("!-> Caught message:     " + getLastMessage());
		}
		if (Settings.SPECTATOR__PRINT_ACTIVE_SPECIFICATION_SCENARIOS
				|| Settings.SPECTATOR__PRINT_ACTIVE_SYSTEM_SCENARIOS
				|| Settings.SPECTATOR__PRINT_ACTIVE_ASSUMPTION_SCENARIOS) {
			Settings.SPECTATOR_OUT.println("!-> Active scenarios:");
			Set<Double> keys = new HashSet<Double>();
			sleep(10);
			keys.addAll(bp.getAllBThreads().keySet());
			for (double key : keys) {
				Scenario scenario = (Scenario) bp.getAllBThreads().get(key);
				if (scenario == null)
					continue;
				if ((scenario.getType().equals(Scenario.SYSTEM) && Settings.SPECTATOR__PRINT_ACTIVE_SYSTEM_SCENARIOS
						&& scenario.isActive())
						|| (scenario.getType().equals(Scenario.SPECIFICATION)
								&& Settings.SPECTATOR__PRINT_ACTIVE_SPECIFICATION_SCENARIOS && scenario.isActive())
						|| (scenario.getType().equals(Scenario.ASSUMPTION)
								&& Settings.SPECTATOR__PRINT_ACTIVE_ASSUMPTION_SCENARIOS && scenario.isActive())) {
					Settings.SPECTATOR_OUT.println("	-----> " + scenario.getType() + " " + scenario + " | "
							+ (scenario.isActive() ? "<active>" : "<>"));
				}
			}
		}
	}

}
