package sbp.specification.scenarios.utils;

import sbp.specification.events.Message;
import sbp.specification.scenarios.Scenario;

public abstract class ScenarioObserver {

	private Scenario scenario;

	public Scenario getScenario() {
		return scenario;
	}
	
	public void setScenario(Scenario scenario) {
		this.scenario = scenario;
		scenario.addObserver(this);
	}

	public void destroy() {
		scenario.removeObserver(this);
		scenario = null;
	}

	public abstract void trigger(Scenario scenario, Message messageEvent);

}
