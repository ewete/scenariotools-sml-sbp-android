package sbp.specification.scenarios.utils;

public enum ExecutionMode {

	COPY_ON_INITIALIZATION, SINGULAR, LOOP, CASE, TEST

}
