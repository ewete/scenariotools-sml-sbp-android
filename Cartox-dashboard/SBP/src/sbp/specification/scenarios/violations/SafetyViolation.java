package sbp.specification.scenarios.violations;

import sbp.specification.scenarios.Scenario;
import sbp.specification.scenarios.utils.ViolationKind;

@SuppressWarnings("serial")
public class SafetyViolation extends Violation {

	public SafetyViolation(Scenario scenario, ViolationCause violationCause) {
		super(scenario, violationCause, ViolationKind.SAFETY);
	}

	public SafetyViolation(Scenario scenario) {
		super(scenario, ViolationKind.INTERRUPT);
	}

}
