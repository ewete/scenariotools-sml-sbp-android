package sbp.specification.scenarios.violations;

import sbp.specification.scenarios.Scenario;
import sbp.specification.scenarios.utils.ViolationKind;

@SuppressWarnings("serial")
public abstract class Violation extends Exception {

	private Scenario scenario;
	private ViolationCause violationCause;
	private ViolationKind violationKind;

	public Violation(Scenario scenario, ViolationCause violationCause, ViolationKind violationKind) {
		super(((violationKind == ViolationKind.INTERRUPT) ? "Cold" : "Hot") + " violation in Scenario " + scenario.getName()
				+ " was caught. " + violationCause.toString());
		this.scenario = scenario;
		this.violationCause = violationCause;
		this.violationKind = violationKind;
	}

	public Violation(Scenario scenario, ViolationKind violationKind) {
		super(((violationKind == ViolationKind.INTERRUPT) ? "Cold" : "Hot") + " violation in Scenario " + scenario.getName()
				+ " was caught.");
		this.scenario = scenario;
		this.violationKind = violationKind;
	}

	public Scenario getScenario() {
		return scenario;
	}

	public void setScenario(Scenario scenario) {
		this.scenario = scenario;
	}

	public ViolationCause getViolationCause() {
		return violationCause;
	}

	public void setViolationCause(ViolationCause violationCause) {
		this.violationCause = violationCause;
	}

	public ViolationKind getViolationKind() {
		return violationKind;
	}

	public void setViolationKind(ViolationKind violationKind) {
		this.violationKind = violationKind;
	}

}
