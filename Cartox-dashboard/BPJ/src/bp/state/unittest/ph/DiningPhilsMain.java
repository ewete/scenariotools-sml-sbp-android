package bp.state.unittest.ph;

import bp.BProgram;

// Main class
public class DiningPhilsMain {
	// ----------------------------------------
	static public void main(String arg[]) {
		try {
			BProgram.startBApplication(DiningPhilsBApp.class, "bp.state.unittest.ph");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
