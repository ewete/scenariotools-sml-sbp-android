package bp.eventSets;

import java.io.Serializable;

/**
 * @author Bertrand Russell
 * 
 *         A set that contains everything.
 */
public class AllEvents implements EventSetInterface, Serializable {
	/**
	 * @see bp.eventSets.EventSetInterface#contains(java.lang.Object)
	 */
	public boolean contains(Object o) {
		return true;
	}

	public String toString() {
		return ("All");
	}
}
