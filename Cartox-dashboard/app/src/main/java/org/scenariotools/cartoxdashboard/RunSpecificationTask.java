package org.scenariotools.cartoxdashboard;

import android.os.AsyncTask;

import org.scenariotools.cartoxdashboard.runtime.MQTT_Exple3_v2_CarToXRunconfig_JamesDashboard;

import java.io.PrintStream;

import sbp.runtime.settings.Settings;

public class RunSpecificationTask extends AsyncTask<Void, Void, Void> {

    private final DashboardActivity dashboardActivity;

    private Exception mException;

    private final Callback mCallback;

    public interface Callback {
        void onComplete(Void result);

        void onError(Exception e);

        void onStart();
    }

    public RunSpecificationTask(DashboardActivity dashboardActivity, Callback callback) {
        this.dashboardActivity = dashboardActivity;
        this.mCallback = callback;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        if (mException != null) {
            mCallback.onError(mException);
        } else {
            mCallback.onStart();
        }
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        if (mException != null) {
            mCallback.onError(mException);
        } else {
            mCallback.onComplete(result);
        }
    }

    @Override
    protected Void doInBackground(Void... params) {
        try {
            //Exple3_v2_CarToXRunconfig_localhost_JamesDashboard_8861.main(dashboardActivity);
            MQTT_Exple3_v2_CarToXRunconfig_JamesDashboard.main(dashboardActivity);
            PrintStream ps = new PrintStream(new CustomOutputStream(dashboardActivity));
            Settings.setRuntimeOut(ps);
            Settings.setServerOut(ps);
            Settings.SPECTATOR__PRINT_CAUGHT_MESSAGE = true;
        } catch (Exception e) {
            mException = e;
        }
        return null;
    }
}
