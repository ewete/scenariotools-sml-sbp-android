package org.scenariotools.cartoxdashboard;

import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class DashboardActivity extends AppCompatActivity {

    private TextView mStatusTextView;
    private EditText mLogsEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        setContentView(R.layout.activity_dashboard);

        this.mStatusTextView = (TextView) findViewById(R.id.tv_status);
        this.mStatusTextView.setText(R.string.tv_status_driving);

        this.mLogsEditText = (EditText) findViewById(R.id.et_log);

        RunSpecificationTask specificationTask = new RunSpecificationTask(this, new RunSpecificationTask.Callback() {
            @Override
            public void onComplete(Void result) {
            }

            @Override
            public void onError(Exception e) {
            }

            @Override
            public void onStart() {
                logMessage("Specification started");
            }
        });
        specificationTask.execute();
    }

    public void logMessage(String message) {
        //TODO: print at most X message => more than X => erase the oldest one
        StringBuilder sb = new StringBuilder(this.mLogsEditText.getText()).append("\n").append(message);
        this.mLogsEditText.setText(sb.toString());
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }
}
