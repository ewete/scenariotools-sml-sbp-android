package org.scenariotools.cartoxdashboard.runtime.observers;

import android.graphics.Color;
import android.widget.TextView;

import org.scenariotools.cartoxdashboard.DashboardActivity;
import org.scenariotools.cartoxdashboard.R;

import cartox.Dashboard;
import cartox.runtime.CarToXObjectSystem;
import sbp.specification.events.Message;
import sbp.specification.role.Role;
import sbp.specification.scenarios.Scenario;
import sbp.specification.scenarios.utils.ScenarioObserver;

public class Exple3_JamesDashboardObserver extends ScenarioObserver {
    private DashboardActivity dashboardActivity;

    public Exple3_JamesDashboardObserver(DashboardActivity dashboardActivity) {
        this.dashboardActivity = dashboardActivity;
    }

    @Override
    public void trigger(Scenario scenario, Message messageEvent) {
        Role receiver = messageEvent.getReceiver();
        String message = messageEvent.getMessage();
        if (receiver.getBinding() instanceof Dashboard) {
            Dashboard dashboard = (Dashboard) receiver.getBinding();
            if (dashboard == CarToXObjectSystem.getInstance().CarToX.getCars().get(0).getDashboard()) {
                if (message.equals("showGo")) {
                    showGo();
                } else if (message.equals("showStop")) {
                    showStop();
                }
            }
        }
    }

    private void showGo() {
        final TextView label = (TextView) this.dashboardActivity.findViewById(R.id.tv_status);
        this.dashboardActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                label.setText(R.string.tv_status_go);
                label.setTextColor(Color.GREEN);
            }
        });
    }

    private void showStop() {
        final TextView label = (TextView) this.dashboardActivity.findViewById(R.id.tv_status);
        this.dashboardActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                label.setText(R.string.tv_status_stop);
                label.setTextColor(Color.RED);
            }
        });
    }
}
