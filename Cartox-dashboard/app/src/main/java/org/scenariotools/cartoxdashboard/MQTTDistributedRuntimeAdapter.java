package org.scenariotools.cartoxdashboard;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import cartox.Car;
import cartox.Dashboard;
import cartox.Driver;
import cartox.Environment;
import cartox.Lane;
import cartox.LaneArea;
import cartox.Obstacle;
import cartox.ObstacleControl;
import cartox.StreetSection;
import cartox.runtime.CarToXObjectSystem;
import sbp.runtime.ObjectRegistry;
import sbp.runtime.SpecificationRunconfig;
import sbp.runtime.adapter.AbstractRuntimeAdapter;
import sbp.runtime.network.exceptions.ClientByeException;
import sbp.runtime.network.messages.Messages;
import sbp.runtime.settings.Settings;
import sbp.specification.events.Message;
import sbp.specification.role.Role;

public class MQTTDistributedRuntimeAdapter extends AbstractRuntimeAdapter {

    private String clientId;

    private MqttAndroidClient senderMqttClient;
    private MqttAndroidClient receiverMqttClient;

    private List<String> lastDeliveredMessageIds;
    private DashboardActivity dashboardActivity;

    public MQTTDistributedRuntimeAdapter(SpecificationRunconfig<?> specificationRunconfig, DashboardActivity dashboardActivity) {
        super(specificationRunconfig);
        clientId = MqttClient.generateClientId();
        lastDeliveredMessageIds = new ArrayList<>();
        this.dashboardActivity = dashboardActivity;
    }

    @Override
    public void run() {
        startServer();
        connectClients();
    }

    @Override
    public void startServer() {
        try {
            MemoryPersistence persistenceReceiverMqttClient = new MemoryPersistence();
            receiverMqttClient = new MqttAndroidClient(dashboardActivity.getApplicationContext(), IPSettings.BROCKER_URL_CONNECTION, clientId + "_Receiver",
                    persistenceReceiverMqttClient);
            receiverMqttClient.setCallback(new MqttCallback() {

                @Override
                public void connectionLost(Throwable throwable) {
                    final String message = String.format("receiverMqttClient connection Lost %s", throwable.getMessage());
                    Settings.NETWORK_OUT.println(message);
                    dashboardActivity.logMessage(message);
                    throwable.printStackTrace();
                }

                @Override
                public void messageArrived(String t, MqttMessage m) throws Exception {
                    String messageString = new String(m.getPayload());
                    if (messageString.equals(Messages.BYE.toString())) {
                        throw new ClientByeException();
                    } else {
                        // Avoid echo messages
                        if (lastDeliveredMessageIds.contains(messageString)) {
                            lastDeliveredMessageIds.remove(messageString);
                        } else {
                            Message messageEvent = deserializeForNetwork(messageString);
                            messageEvent = messageEvent.deserializeForNetwork();
                            final String message = String.format("New Message : %s" ,messageEvent.getName());
                            Settings.NETWORK_OUT.println(message);
                            dashboardActivity.logMessage(message);
                            getEventQueueScenario().enqueueEvent(messageEvent);
                        }
                    }
                }

                @Override
                public void deliveryComplete(IMqttDeliveryToken t) {
                }
            });
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            receiverMqttClient.connect(connOpts, null, new IMqttActionListener() {

                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    final String message = String.format("receiverMqttClient connected to broker %s", IPSettings.BROCKER_URL_CONNECTION);
                    Settings.NETWORK_OUT.println(message);
                    dashboardActivity.logMessage(message);
                    try {
                        IMqttToken subToken = receiverMqttClient.subscribe(IPSettings.TOPIC, IPSettings.QOS);
                        subToken.setActionCallback(new IMqttActionListener() {
                            @Override
                            public void onSuccess(IMqttToken asyncActionToken) {
                                final String message = String.format("receiverMqttClient subscribed to topic %s", IPSettings.TOPIC);
                                Settings.NETWORK_OUT.println(message);
                                dashboardActivity.logMessage(message);
                            }

                            @Override
                            public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                            }
                        });
                    } catch (MqttException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void connectClients() {
        try {
            MemoryPersistence persistenceSenderMqttClient = new MemoryPersistence();
            senderMqttClient = new MqttAndroidClient(dashboardActivity.getApplicationContext(), IPSettings.BROCKER_URL_CONNECTION, clientId + "_Sender",
                    persistenceSenderMqttClient);
            senderMqttClient.setCallback(new MqttCallback() {
                @Override
                public void connectionLost(Throwable throwable) {
                    final String message = String.format("senderMqttClient connection Lost %s", throwable.getMessage());
                    Settings.NETWORK_OUT.println(message);
                    dashboardActivity.logMessage(message);
                    throwable.printStackTrace();
                }

                @Override
                public void messageArrived(String t, MqttMessage m) throws Exception {
                }

                @Override
                public void deliveryComplete(IMqttDeliveryToken t) {
                    try {
                        //Settings.NETWORK_OUT.println("senderMqttClient's message delivered with id " + t.getMessageId());
                        lastDeliveredMessageIds.add(new String(t.getMessage().getPayload(), "UTF-8"));
                    } catch (MqttException | UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            });
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            senderMqttClient.connect(connOpts, null, new IMqttActionListener() {

                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    final String message = String.format("senderMqttClient connected to broker: %s", IPSettings.BROCKER_URL_CONNECTION);
                    Settings.NETWORK_OUT.println(message);
                    dashboardActivity.logMessage(message);
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {

                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void publish(Message event) {
        try {
            MqttMessage message = new MqttMessage(serializeForNetwork(event).getBytes("UTF-8"));
            message.setQos(IPSettings.QOS);
            senderMqttClient.publish(IPSettings.TOPIC, message);
        } catch (MqttException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void receive(Message event) {
        getEventQueueScenario().enqueueEvent(event);
    }

    private String serializeForNetwork(Message event) {
        // Settings.NETWORK_OUT.println("Serialize : " + event);
        //String out = gsonBuilder.create().toJson(event, Message.class);
        // Settings.NETWORK_OUT.println("Serialized to : " + out);
        // Settings.NETWORK_OUT.println("--- End serialization ---");

        // printToFile("serializeForNetwork", event, out);
        //return out;
        return gsonBuilder.create().toJson(event, Message.class);
    }

    private Message deserializeForNetwork(String event) {
        // Settings.NETWORK_OUT.println("deserialize : " + event);
        //Message out = gsonBuilder.create().fromJson(event, Message.class);
        // Settings.NETWORK_OUT.println("deserialized to : " + out);
        // Settings.NETWORK_OUT.println("--- End deserialization ---");

        // printToFile("deserializeForNetwork", out, event);
        //return out;
        return gsonBuilder.create().fromJson(event, Message.class);
    }

    private final GsonBuilder gsonBuilder = new GsonBuilder().registerTypeAdapter(Message.class, new MessageAdapter())
            .registerTypeAdapter(Role.class, new RoleAdapter()).registerTypeAdapter(Class.class, new ClassAdapter())
            .registerTypeAdapter(Environment.class, new EnvironmentAdapter())
            .registerTypeAdapter(Car.class, new CarAdapter())
            .registerTypeAdapter(Dashboard.class, new DashboardAdapter())
            .registerTypeAdapter(Obstacle.class, new ObstacleAdapter())
            .registerTypeAdapter(ObstacleControl.class, new ObstacleControlAdapter())
            .registerTypeAdapter(List.class, new ListAdapter())
            .registerTypeAdapter(LaneArea.class, new LaneAreaAdapter())
            .registerTypeAdapter(Driver.class, new DriverAdapter()).setPrettyPrinting().serializeNulls();

    private class MessageAdapter implements JsonDeserializer<Message>, JsonSerializer<Message> {

        @Override
        public JsonElement serialize(final Message message, final Type typeOfSrc,
                                     final JsonSerializationContext context) {

            final JsonObject jsonObject = new JsonObject();

            jsonObject.addProperty("message", message.getMessage());

            final JsonElement jsonSender = context.serialize(message.getSender(), Role.class);
            jsonObject.add("sender", jsonSender);

            final JsonElement jsonReceiver = context.serialize(message.getReceiver(), Role.class);
            jsonObject.add("receiver", jsonReceiver);

            if (!message.getParameters().isEmpty()) {
                final JsonElement jsonParameters = context.serialize(message.getParameters(), List.class);
                jsonObject.add("parameters", jsonParameters);
            }

            // jsonObject.addProperty("strict", message.isStrict());
            // jsonObject.addProperty("requested", message.isRequested());
            // jsonObject.addProperty("initializing", message.isInitializing());

            return jsonObject;
        }

        @Override
        public Message deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context)
                throws JsonParseException {

            final JsonObject jsonObject = json.getAsJsonObject();

            String messageString = jsonObject.get("message").getAsString();

            final JsonElement jsonSender = jsonObject.get("sender");
            Role sender = context.deserialize(jsonSender, Role.class);

            final JsonElement jsonReceiver = jsonObject.get("receiver");
            Role receiver = context.deserialize(jsonReceiver, Role.class);

            Message message = new Message(sender, receiver, messageString);

            if (jsonObject.has("parameters")) {
                final JsonElement jsonParameters = jsonObject.get("parameters");
                List<Object> parameters = context.deserialize(jsonParameters, List.class);
                message.getParameters().addAll(parameters);
            }

            // message.setStrict(jsonObject.get("strict").getAsBoolean());
            // message.setRequested(jsonObject.get("requested").getAsBoolean());
            // message.setInitializing(jsonObject.get("initializing").getAsBoolean());
            return message;
        }
    }

    private class RoleAdapter implements JsonDeserializer<Role<?>>, JsonSerializer<Role<?>> {

        @Override
        public JsonElement serialize(final Role<?> role, final Type typeOfSrc, final JsonSerializationContext context) {

            final JsonObject jsonObject = new JsonObject();

            jsonObject.addProperty("name", role.getName());

            final JsonElement jsonType = context.serialize(role.getType(), Class.class);
            jsonObject.add("type", jsonType);

            JsonElement jsonBinding = null;
            if (role.hasBinding()) {
                if (role.getBinding() instanceof Environment) {
                    jsonBinding = context.serialize(role.getBinding(), Environment.class);
                } else if (role.getBinding() instanceof Car) {
                    jsonBinding = context.serialize(role.getBinding(), Car.class);
                } else if (role.getBinding() instanceof Dashboard) {
                    jsonBinding = context.serialize(role.getBinding(), Dashboard.class);
                } else if (role.getBinding() instanceof Obstacle) {
                    jsonBinding = context.serialize(role.getBinding(), Obstacle.class);
                } else if (role.getBinding() instanceof ObstacleControl) {
                    jsonBinding = context.serialize(role.getBinding(), ObstacleControl.class);
                } else if (role.getBinding() instanceof LaneArea) {
                    jsonBinding = context.serialize(role.getBinding(), LaneArea.class);
                } else if (role.getBinding() instanceof Driver) {
                    jsonBinding = context.serialize(role.getBinding(), Driver.class);
                }
                if (jsonBinding != null) {
                    jsonObject.add("binding", jsonBinding);
                } else {
                    Settings.NETWORK_OUT
                            .println("RoleAdapter : Serialisation of " + role.getBinding() + " not supported");
                }
            }

            return jsonObject;
        }

        @Override
        public Role<?> deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context)
                throws JsonParseException {

            final JsonObject jsonObject = json.getAsJsonObject();

            String name = jsonObject.get("name").getAsString();

            final JsonElement jsonType = jsonObject.get("type");
            Class type = context.deserialize(jsonType, Class.class);

            Role role = new Role(type, name);

            if (jsonObject.has("binding")) {
                final JsonElement jsonBinding = jsonObject.get("binding");
                if (type.equals(Environment.class)) {
                    Environment binding = context.deserialize(jsonBinding, Environment.class);
                    role.setBinding(binding);
                } else if (type.equals(Car.class)) {
                    Car binding = context.deserialize(jsonBinding, Car.class);
                    role.setBinding(binding);
                } else if (type.equals(Dashboard.class)) {
                    Dashboard binding = context.deserialize(jsonBinding, Dashboard.class);
                    role.setBinding(binding);
                } else if (type.equals(Obstacle.class)) {
                    Obstacle binding = context.deserialize(jsonBinding, Obstacle.class);
                    role.setBinding(binding);
                } else if (type.equals(ObstacleControl.class)) {
                    ObstacleControl binding = context.deserialize(jsonBinding, ObstacleControl.class);
                    role.setBinding(binding);
                } else if (type.equals(LaneArea.class)) {
                    LaneArea binding = context.deserialize(jsonBinding, LaneArea.class);
                    role.setBinding(binding);
                } else if (type.equals(Driver.class)) {
                    Driver binding = context.deserialize(jsonBinding, Driver.class);
                    role.setBinding(binding);
                } else {
                    Settings.NETWORK_OUT.println("RoleAdapter : Deserialisation of " + jsonBinding + " not supported");
                }
            }

            return role;
        }
    }

    private class ClassAdapter implements JsonDeserializer<Class<?>>, JsonSerializer<Class<?>> {

        @Override
        public JsonElement serialize(final Class<?> roleType, final Type typeOfSrc,
                                     final JsonSerializationContext context) {

            final JsonObject jsonObject = new JsonObject();

            jsonObject.addProperty("name", roleType.getName());

            return jsonObject;
        }

        @Override
        public Class<?> deserialize(final JsonElement json, final Type typeOfT,
                                    final JsonDeserializationContext context) throws JsonParseException {

            final JsonObject jsonObject = json.getAsJsonObject();

            final String typeName = jsonObject.get("name").getAsString();

            Class class1 = null;
            try {
                class1 = Class.forName(typeName);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            if (class1 == null) {
                Settings.NETWORK_OUT.println("ClassAdapter : Deserialisation of " + json + " not supported");
            }
            return class1;
        }
    }

    private class ListAdapter implements JsonDeserializer<List<?>>, JsonSerializer<List<?>> {

        @Override
        public JsonElement serialize(final List<?> parameters, final Type typeOfSrc,
                                     final JsonSerializationContext context) {

            final JsonArray jsonArray = new JsonArray();

            for (Object param : parameters) {
                Integer objecIDInRegistry = ObjectRegistry.getInstance().getID(param);
                if (objecIDInRegistry == null || objecIDInRegistry < 0) {
                    // param not registered
                    if (param.getClass().isPrimitive()) {
                        final JsonObject jsonObject = new JsonObject();
                        jsonObject.addProperty("type", param.getClass().getName());
                        jsonObject.addProperty("value", param.toString());
                        jsonArray.add(jsonObject);
                    } else {
                        Settings.NETWORK_OUT.println("ListAdapter : Parameter type not supported for serialization "
                                + param + ". It is neither a registered object nor a primitive type.");
                    }
                } else {
                    // param registered
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("type", param.getClass().getName());
                    jsonObject.addProperty("value", objecIDInRegistry);
                    jsonArray.add(jsonObject);
                }
            }

            return jsonArray;
        }

        @Override
        public List<?> deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context)
                throws JsonParseException {

            final JsonArray jsonArray = json.getAsJsonArray();
            final List<Object> parameters = new ArrayList<>();

            for (JsonElement jsonElement : jsonArray) {
                JsonObject jsonObject = jsonElement.getAsJsonObject();
                String type = jsonObject.get("type").getAsString();
                String value = jsonObject.get("value").getAsString();
                try {
                    Class<?> clazz = Class.forName(type);
                    if (clazz.isPrimitive()) {
                        Constructor<?> ctor = clazz.getConstructor(String.class);
                        Object object = ctor.newInstance(new Object[]{value});
                        parameters.add(object);
                    } else {
                        Integer id = Integer.valueOf(value);
                        Object object = ObjectRegistry.getInstance().getObject((id));
                        if (object != null) {
                            parameters.add(object);
                        } else {
                            Settings.NETWORK_OUT.println(
                                    String.format("ListAdapter : Object with id %s not found for deserialization of %s",
                                            id, jsonObject));
                        }
                    }
                } catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException
                        | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            }

            return parameters;
        }
    }

    private class EnvironmentAdapter implements JsonDeserializer<Environment>, JsonSerializer<Environment> {

        @Override
        public JsonElement serialize(final Environment environment, final Type typeOfSrc,
                                     final JsonSerializationContext context) {

            final JsonObject jsonObject = new JsonObject();

            jsonObject.addProperty("name", environment.getName());

            return jsonObject;
        }

        @Override
        public Environment deserialize(final JsonElement json, final Type typeOfT,
                                       final JsonDeserializationContext context) throws JsonParseException {

            final JsonObject jsonObject = json.getAsJsonObject();

            final String envName = jsonObject.get("name").getAsString();

            Environment env = CarToXObjectSystem.getInstance().CarToX.getEnvironment();

            if (env.getName().equals(envName)) {
                return env;
            } else {
                Settings.NETWORK_OUT.println("Environment deserialize : ERRRRRRRRRRrrrrrrrrrroRRRRRRRR");
                return null;
            }
        }
    }

    private class CarAdapter implements JsonDeserializer<Car>, JsonSerializer<Car> {

        @Override
        public JsonElement serialize(final Car car, final Type typeOfSrc, final JsonSerializationContext context) {

            final JsonObject jsonObject = new JsonObject();

            jsonObject.addProperty("name", car.getName());

            return jsonObject;
        }

        @Override
        public Car deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context)
                throws JsonParseException {

            final JsonObject jsonObject = json.getAsJsonObject();

            final String carName = jsonObject.get("name").getAsString();

            Car carOut = null;
            for (Car car : CarToXObjectSystem.getInstance().CarToX.getCars()) {
                if (car.getName().equals(carName)) {
                    carOut = car;
                    break;
                }
            }

            if (carOut != null) {
                return carOut;
            } else {
                Settings.NETWORK_OUT.println("Car deserialize : ERRRRRRRRRRrrrrrrrrrroRRRRRRRR");
                return null;
            }
        }
    }

    private class DashboardAdapter implements JsonDeserializer<Dashboard>, JsonSerializer<Dashboard> {

        @Override
        public JsonElement serialize(final Dashboard dashboard, final Type typeOfSrc,
                                     final JsonSerializationContext context) {

            final JsonObject jsonObject = new JsonObject();

            jsonObject.addProperty("name", dashboard.getName());

            return jsonObject;
        }

        @Override
        public Dashboard deserialize(final JsonElement json, final Type typeOfT,
                                     final JsonDeserializationContext context) throws JsonParseException {

            final JsonObject jsonObject = json.getAsJsonObject();

            final String dashboardName = jsonObject.get("name").getAsString();

            Dashboard dashboardOut = null;
            for (Car car : CarToXObjectSystem.getInstance().CarToX.getCars()) {
                if (car.getDashboard().getName().equals(dashboardName)) {
                    dashboardOut = car.getDashboard();
                    break;
                }
            }

            if (dashboardOut != null) {
                return dashboardOut;
            } else {
                Settings.NETWORK_OUT.println("Dashboard deserialize : ERRRRRRRRRRrrrrrrrrrroRRRRRRRR");
                return null;
            }
        }
    }

    private class ObstacleAdapter implements JsonDeserializer<Obstacle>, JsonSerializer<Obstacle> {

        @Override
        public JsonElement serialize(final Obstacle obstacle, final Type typeOfSrc,
                                     final JsonSerializationContext context) {

            final JsonObject jsonObject = new JsonObject();

            jsonObject.addProperty("name", obstacle.getName());

            return jsonObject;
        }

        @Override
        public Obstacle deserialize(final JsonElement json, final Type typeOfT,
                                    final JsonDeserializationContext context) throws JsonParseException {

            final JsonObject jsonObject = json.getAsJsonObject();

            final String obstacleName = jsonObject.get("name").getAsString();

            Obstacle obstacleOut = null;
            for (StreetSection street : CarToXObjectSystem.getInstance().CarToX.getStreetSections()) {
                for (Obstacle obstacle : street.getObstacles()) {
                    if (obstacle.getName().equals(obstacleName)) {
                        obstacleOut = obstacle;
                        break;
                    }
                }
                if (obstacleOut != null) {
                    break;
                }
            }

            if (obstacleOut != null) {
                return obstacleOut;
            } else {
                Settings.NETWORK_OUT.println("Obstacle deserialize : ERRRRRRRRRRrrrrrrrrrroRRRRRRRR");
                return null;
            }
        }
    }

    private class ObstacleControlAdapter implements JsonDeserializer<ObstacleControl>, JsonSerializer<ObstacleControl> {

        @Override
        public JsonElement serialize(final ObstacleControl obstacleControl, final Type typeOfSrc,
                                     final JsonSerializationContext context) {

            final JsonObject jsonObject = new JsonObject();

            jsonObject.addProperty("name", obstacleControl.getName());

            return jsonObject;
        }

        @Override
        public ObstacleControl deserialize(final JsonElement json, final Type typeOfT,
                                           final JsonDeserializationContext context) throws JsonParseException {

            final JsonObject jsonObject = json.getAsJsonObject();

            final String obstacleName = jsonObject.get("name").getAsString();

            ObstacleControl obstacleControlOut = null;
            for (ObstacleControl obstacleControl : CarToXObjectSystem.getInstance().CarToX.getObstacleControls()) {
                if (obstacleControl.getName().equals(obstacleName)) {
                    obstacleControlOut = obstacleControl;
                    break;
                }
            }

            if (obstacleControlOut != null) {
                return obstacleControlOut;
            } else {
                Settings.NETWORK_OUT.println("ObstacleControl deserialize : ERRRRRRRRRRrrrrrrrrrroRRRRRRRR");
                return null;
            }
        }
    }

    private class LaneAreaAdapter implements JsonDeserializer<LaneArea>, JsonSerializer<LaneArea> {

        @Override
        public JsonElement serialize(final LaneArea laneArea, final Type typeOfSrc,
                                     final JsonSerializationContext context) {

            final JsonObject jsonObject = new JsonObject();

            jsonObject.addProperty("name", laneArea.getName());

            return jsonObject;
        }

        @Override
        public LaneArea deserialize(final JsonElement json, final Type typeOfT,
                                    final JsonDeserializationContext context) throws JsonParseException {

            final JsonObject jsonObject = json.getAsJsonObject();

            final String laName = jsonObject.get("name").getAsString();

            LaneArea laneAreaOut = null;
            for (StreetSection street : CarToXObjectSystem.getInstance().CarToX.getStreetSections()) {
                for (Lane lane : street.getLanes()) {
                    for (LaneArea la : lane.getLaneAreas()) {
                        if (la.getName().equals(laName)) {
                            laneAreaOut = la;
                            break;
                        }
                    }
                    if (laneAreaOut != null) {
                        break;
                    }
                }
                if (laneAreaOut != null) {
                    break;
                }
            }

            if (laneAreaOut != null) {
                return laneAreaOut;
            } else {
                Settings.NETWORK_OUT.println("LaneArea deserialize : ERRRRRRRRRRrrrrrrrrrroRRRRRRRR");
                return null;
            }
        }
    }

    private class DriverAdapter implements JsonDeserializer<Driver>, JsonSerializer<Driver> {

        @Override
        public JsonElement serialize(final Driver driver, final Type typeOfSrc,
                                     final JsonSerializationContext context) {

            final JsonObject jsonObject = new JsonObject();

            jsonObject.addProperty("name", driver.getName());

            return jsonObject;
        }

        @Override
        public Driver deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context)
                throws JsonParseException {

            final JsonObject jsonObject = json.getAsJsonObject();

            final String driverName = jsonObject.get("name").getAsString();

            Driver driverOut = null;
            for (Car car : CarToXObjectSystem.getInstance().CarToX.getCars()) {
                if (car.getDriver().getName().equals(driverName)) {
                    driverOut = car.getDriver();
                    break;
                }
            }

            if (driverOut != null) {
                return driverOut;
            } else {
                Settings.NETWORK_OUT.println("Driver deserialize : ERRRRRRRRRRrrrrrrrrrroRRRRRRRR");
                return null;
            }
        }
    }
}
