package org.scenariotools.cartoxdashboard.runtime;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.scenariotools.cartoxdashboard.DashboardActivity;
import org.scenariotools.cartoxdashboard.runtime.observers.Exple3_JamesDashboardObserver;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import cartox.CarToX;
import cartox.CartoxPackage;
import cartox.runtime.CarToXObjectSystem;
import cartox.specification.CarToXSpecification;
import sbp.runtime.SpecificationRunconfig;

public class Exple3_v2_CarToXRunconfig_localhost_JamesDashboard_8861 extends SpecificationRunconfig<CarToXSpecification> {
    private final int JamesDashboard_port = 8861;

    private final String JamesCar_hostName = "192.168.56.1";
    private final int JamesCar_port = 8860;

    private static Exple3_JamesDashboardObserver dashboardObserverForJamesCar;

    public Exple3_v2_CarToXRunconfig_localhost_JamesDashboard_8861() {
        super(new CarToXSpecification());
        enableDistributedExecutionMode(JamesDashboard_port);
        //Settings.setRuntimeOut(ps);
        //Settings.setServerOut(ps);
        //Settings.SPECTATOR__PRINT_CAUGHT_MESSAGE = true;
    }

    @Override
    protected void registerParticipatingObjects() {
        CarToXObjectSystem objectSystem = CarToXObjectSystem.getInstance();
        objectSystem.CarToX = initObjectSystem();
        registerObject(objectSystem.CarToX, UNCONTROLLABLE); // CarToX
        registerObject(objectSystem.CarToX.getCars().get(0), UNCONTROLLABLE); // JamesCar
        registerObject(objectSystem.CarToX.getCars().get(0).getDashboard(), CONTROLLABLE); // JamesCarDashboard
        registerObject(objectSystem.CarToX.getCars().get(0).getDriver(), UNCONTROLLABLE); // James
        registerObject(objectSystem.CarToX.getCars().get(1), UNCONTROLLABLE); // AnnasCar
        registerObject(objectSystem.CarToX.getCars().get(1).getDashboard(), UNCONTROLLABLE); // AnnasCarDashboard
        registerObject(objectSystem.CarToX.getCars().get(1).getDriver(), UNCONTROLLABLE); // Anna
        registerObject(objectSystem.CarToX.getEnvironment(), UNCONTROLLABLE); // env
        registerObject(objectSystem.CarToX.getStreetSections().get(0), UNCONTROLLABLE); // section1
        registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0), UNCONTROLLABLE); // lane1 // TODO: Check subclasses
        registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0).getLaneAreas().get(0), UNCONTROLLABLE); // lane1_area1_free
        registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0).getLaneAreas().get(1), UNCONTROLLABLE); // lane1_area2_beforeObstacle
        registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0).getLaneAreas().get(2), UNCONTROLLABLE); // lane1_area3_Obstacle
        registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0).getLaneAreas().get(3), UNCONTROLLABLE); // lane1_area4_afterObstacle
        registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0).getLaneAreas().get(4), UNCONTROLLABLE); // lane1_area5_free
        registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1), UNCONTROLLABLE); // lane2 // TODO: Check subclasses
        registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1).getLaneAreas().get(0), UNCONTROLLABLE); // lane2_area1_free
        registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1).getLaneAreas().get(1), UNCONTROLLABLE); // lane2_area2_beforeNarrowPassage
        registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1).getLaneAreas().get(2), UNCONTROLLABLE); // lane2_area3_NarrowPassage
        registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1).getLaneAreas().get(3), UNCONTROLLABLE); // lane2_area4_afterNarrowPassage
        registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1).getLaneAreas().get(4), UNCONTROLLABLE); // lane2_area5_free
        registerObject(objectSystem.CarToX.getStreetSections().get(0).getObstacles().get(0), UNCONTROLLABLE); // obstacle // TODO: Check subclasses
        registerObject(objectSystem.CarToX.getObstacleControls().get(0), UNCONTROLLABLE); // obstacleControl
    }

    @Override
    protected void registerNetworkAdressesForObjects() {
        CarToXObjectSystem objectSystem = CarToXObjectSystem.getInstance();
        registerAddress(objectSystem.CarToX.getCars().get(0), JamesCar_hostName, JamesCar_port);
    }

    @Override
    protected void registerObservers() {
        dashboardObserverForJamesCar = new Exple3_JamesDashboardObserver(dashboardActivity);
        addScenarioObserver(dashboardObserverForJamesCar);
    }

    private static DashboardActivity dashboardActivity;

    public static void main(DashboardActivity dashboard) {
        dashboardActivity = dashboard;
        SpecificationRunconfig.run(Exple3_v2_CarToXRunconfig_localhost_JamesDashboard_8861.class);
    }

    public CarToX initObjectSystem(){
        CartoxPackage.eINSTANCE.eClass();
        Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
        Map<String, Object> m = reg.getExtensionToFactoryMap();
        m.put("xmi", new XMIResourceFactoryImpl());
        ResourceSet resSet = new ResourceSetImpl();
        try {
            InputStream inStream = dashboardActivity.getAssets().open("TwoCarsAndObstacle/cartox.xmi");
            Resource resource = resSet.createResource(URI.createURI("TwoCarsAndObstacle/cartox.xmi"), "xmi");
            resource.load(inStream, m);
            return (CarToX) resource.getContents().get(0);
        } catch (IOException ex) {
            return null;
        }
    }
}
