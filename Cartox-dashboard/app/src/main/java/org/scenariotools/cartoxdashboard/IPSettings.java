package org.scenariotools.cartoxdashboard;

public interface IPSettings {
	final String JamesDashboard_hostName = "localhost";
	final int JamesDashboard_port = 8861;

	final String JamesCar_hostName = "localhost";
	final int JamesCar_port = 8860;

	final String AnnasDashboard_hostName = "localhost";
	final int AnnasDashboard_port = 8862;

	final String AnnasCar_hostName = "localhost";
	final int AnnasCar_port = 8860;
	
	final String BROCKER_URL_CONNECTION = "tcp://iot.eclipse.org:1883";
	final int QOS = 2;
	final String TOPIC = "Cartox.sbp.example";
}
