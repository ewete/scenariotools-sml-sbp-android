package org.scenariotools.cartoxdashboard;

import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;
import java.io.OutputStream;

public class CustomOutputStream extends OutputStream {

    private DashboardActivity dashboardActivity;

    public CustomOutputStream(DashboardActivity dashboardActivity) {
        this.dashboardActivity = dashboardActivity;
    }

    @Override
    public void write(int b) throws IOException {
        final String message = String.valueOf((char) b);
        dashboardActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((EditText) dashboardActivity.findViewById(R.id.et_log)).setText(message + "\n");
            }
        });
    }
}
