package cartox.ui.simulation;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JPanel;

import cartox.Car;
import cartox.Lane;
import cartox.LaneArea;
import cartox.StreetSection;
import cartox.runtime.CarToXObjectSystem;

@SuppressWarnings("serial")
public class StreetSectionView extends JPanel {

	private LaneView rightLane;
	private LaneView leftLane;

	private Map<LaneArea, LaneAreaView> laneAreaMap;

	/**
	 * Create the panel.
	 * 
	 * @param carToXSimulationFrame
	 */
	public StreetSectionView(StreetSection streetSection, CarToXSimulationFrame carToXSimulationFrame) {
		Lane l0 = streetSection.getLanes().get(0);
		Lane l1 = streetSection.getLanes().get(1);
		rightLane = new LaneView(l0, this);
		leftLane = new LaneView(l1, this);

		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 279, 2, 2, 0 };
		gridBagLayout.rowHeights = new int[] { 2, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
		setLayout(gridBagLayout);

		GridBagConstraints gbc_rightLane = new GridBagConstraints();
		gbc_rightLane.anchor = GridBagConstraints.NORTHWEST;
		gbc_rightLane.insets = new Insets(0, 0, 0, 5);
		gbc_rightLane.gridx = 0;
		gbc_rightLane.gridy = 0;
		add(rightLane, gbc_rightLane);
		GridBagConstraints gbc_leftLane = new GridBagConstraints();
		gbc_leftLane.anchor = GridBagConstraints.NORTHWEST;
		gbc_leftLane.gridx = 0;
		gbc_leftLane.gridy = 1;
		add(leftLane, gbc_leftLane);

		for (int i = l0.getLaneAreas().size() - 1; i >= 0; i--) {
			LaneArea areaModel = l0.getLaneAreas().get(i);
			LaneAreaView areaView = new LaneAreaView(areaModel);
			if (areaModel.getObstacle() != null) {
				ObstacleView obstacleView = new ObstacleView(areaModel.getObstacle());
				areaView.add(obstacleView);
			}
			rightLane.add(areaView);
			getLaneAreaMap().put(areaModel, areaView);
		}

		for (int i = 0; i < l1.getLaneAreas().size(); i++) {
			LaneArea areaModel = l1.getLaneAreas().get(i);
			LaneAreaView areaView = new LaneAreaView(areaModel);
			if (areaModel.getObstacle() != null) {
				ObstacleView obstacleView = new ObstacleView(areaModel.getObstacle());
				areaView.add(obstacleView);
			}
			leftLane.add(areaView);
			getLaneAreaMap().put(areaModel, areaView);
		}

		for (Car car : CarToXObjectSystem.getInstance().CarToX.getCars()) {
			CarView carView = new CarView(car);
			getViewForLane(car.getInArea()).add(carView);
		}
		
		LaneAreaViewMouseListener.getInstance().setMap(getLaneAreaMap());
	}

	public Map<LaneArea, LaneAreaView> getLaneAreaMap() {
		if (laneAreaMap == null) {
			laneAreaMap = new HashMap<LaneArea, LaneAreaView>();
		}
		return laneAreaMap;
	}

	public LaneAreaView getViewForLane(LaneArea areaModel) {
		return getLaneAreaMap().get(areaModel);
	}

	public LaneView getRightLane() {
		return rightLane;
	}

	public LaneView getLeftLane() {
		return leftLane;
	}

}
