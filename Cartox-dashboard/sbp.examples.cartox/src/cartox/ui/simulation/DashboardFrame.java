package cartox.ui.simulation;

import java.awt.Font;

import javax.swing.JLabel;

import sbp.ui.ConsoleFrame;

@SuppressWarnings("serial")
public class DashboardFrame extends ConsoleFrame {

	private JLabel label;

	public DashboardFrame(String name) {
		super(name);
		//getTabbedPane().setVisible(false);
		label = new JLabel("Driving");
		label.setFont(new Font(label.getFont().getName(), Font.PLAIN, 100));
		add(label);
		pack();
	}

	public JLabel getLabel() {
		return label;
	}

}
