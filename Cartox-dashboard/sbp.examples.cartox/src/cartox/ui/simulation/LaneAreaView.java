package cartox.ui.simulation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;

import cartox.LaneArea;

@SuppressWarnings("serial")
public class LaneAreaView extends JPanel {

	private LaneArea laneArea;

	/**
	 * Create the panel.
	 * 
	 * @param laneArea
	 * 
	 * @param carToXSimulationFrame
	 */
	public LaneAreaView(LaneArea laneArea) {
		setToolTipText(laneArea.getName());
		setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		setPreferredSize(new Dimension(150, 50));

		setBackground(Color.lightGray);
		addMouseListener(LaneAreaViewMouseListener.getInstance());

		this.laneArea = laneArea;
//		setLayout(new BorderLayout(0, 0));
		setLayout(new FlowLayout());
	}

	public LaneAreaView getNext() {
		LaneView parent = (LaneView) getParent();
		return parent.getNext(this);
	}

	public LaneAreaView getPrevious() {
		LaneView parent = (LaneView) getParent();
		return parent.getPrevious(this);
	}

	public LaneAreaView getOpposite() {
		LaneView parent = (LaneView) getParent();
		return parent.getOpposite(this);
	}

	public boolean isFree() {
		return getComponents().length == 0;
	}

	public LaneArea getLaneArea() {
		return laneArea;
	}

	public void highlight(Color color) {
		setBackground(color);
	}

	public boolean isBlockedByObstacle() {
		for (Component c : getComponents()) {
			if (c instanceof ObstacleView) {
				return true;
			}
		}
		return false;
	}

	public ObstacleView getObstacle() {
		for (Component c : getComponents()) {
			if (c instanceof ObstacleView) {
				return (ObstacleView) c;
			}
		}
		return null;
	}

}
