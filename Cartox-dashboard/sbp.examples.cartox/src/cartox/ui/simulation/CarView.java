package cartox.ui.simulation;

import javax.swing.JLabel;

import cartox.Car;

@SuppressWarnings("serial")
public class CarView extends JLabel {

	private Car car;

	public CarView(Car car) {
		this.car = car;
		setText(car.getName());
		addMouseListener(LaneAreaViewMouseListener.getInstance());
	}

	public Car getCar() {
		return car;
	}

}
