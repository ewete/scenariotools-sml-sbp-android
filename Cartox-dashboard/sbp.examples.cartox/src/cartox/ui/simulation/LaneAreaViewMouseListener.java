package cartox.ui.simulation;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Map;

import cartox.Car;
import cartox.Environment;
import cartox.LaneArea;
import cartox.runtime.CarToXObjectSystem;
import sbp.runtime.adapter.RuntimeAdapter;
import sbp.specification.events.Message;
import sbp.specification.role.Role;

public class LaneAreaViewMouseListener implements MouseListener {

	private static final Color INIT = Color.lightGray;
	private static final Color CAR_SELECTED = Color.yellow;
	private static final Color OBSTACLE_FOUND = Color.red;
	private static final Color NARROW_PASSAGE_ENTERED = Color.orange;
	private static final Color NARROW_PASSAGE_LEFT = Color.green;

	private static LaneAreaViewMouseListener instance;

	private Color state = INIT;
	private CarView selectedCar;

	private Map<LaneArea, LaneAreaView> laneAreaMap;
	private RuntimeAdapter runtimeAdapter;

	public static LaneAreaViewMouseListener getInstance() {
		if (instance == null) {
			instance = new LaneAreaViewMouseListener();
		}
		return instance;
	}

	public Color getState() {
		return state;
	}

	public void setState(Color state) {
		this.state = state;
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		if (arg0.getComponent() instanceof CarView) {
			clicked((CarView) arg0.getComponent());
		} else if (arg0.getComponent() instanceof LaneAreaView) {
			clicked((LaneAreaView) arg0.getComponent());
		}
	}

	private void clicked(CarView view) {
		if (state == INIT) {
			// Select Car
			selectedCar = view;
			selectCar();
			setState(CAR_SELECTED);
		} else if (state == CAR_SELECTED) {
			if (view == selectedCar) {
				deselectCar(view);
				selectedCar = null;
				setState(INIT);
			} else {
				// Reselect Car
				deselectCar(view);
				selectedCar = view;
				selectCar();
				setState(CAR_SELECTED);
			}
		}
	}

	private void selectCar() {
		Car car = selectedCar.getCar();
		LaneAreaView oppositeView = getOppositeView(car);
		LaneAreaView nextView = getNextView(car);
		if (nextView.isFree()) {
			nextView.highlight(CAR_SELECTED);
		}
		if (oppositeView.isFree()) {
			oppositeView.highlight(CAR_SELECTED);
		}
	}

	private void deselectCar(CarView view) {
		Car car = view.getCar();
		LaneAreaView oppositeView = getOppositeView(car);
		LaneAreaView nextView = getNextView(car);
		nextView.highlight(INIT);
		oppositeView.highlight(INIT);
	}

	private void clicked(LaneAreaView view) {
		if (state == INIT) {
			// No Car selected -> Do nothing
		} else if (state == CAR_SELECTED) {
			if (view.isFree() && carCanMove(view)) {
				// Move Car
				Car car = selectedCar.getCar();
				deselectCar(selectedCar);
				LaneAreaView current = (LaneAreaView) selectedCar.getParent();
				current.remove(selectedCar);
				current.validate();
				current.repaint();
				view.add(selectedCar);
				// car.setInArea(view.getLaneArea());
				// car.setOnLane((Lane) view.getLaneArea().eContainer());
				if (view == getNextView(car)) {
					if (current.getNext() == view) {
						movedToNextArea(car);
					} else if (current.getPrevious() == view) {
						movedToNextAreaOnOvertakingLane(car);
					}
				} else if (view == getOppositeView(car)) {
					changedToOppositeArea(car);
				}
				if (view.getNext().isBlockedByObstacle()) {
					view.getNext().highlight(OBSTACLE_FOUND);
					setState(OBSTACLE_FOUND);
				} else if (view.getOpposite().isBlockedByObstacle()) {
					view.highlight(NARROW_PASSAGE_ENTERED);
					setState(NARROW_PASSAGE_ENTERED);
				} else if (!view.getOpposite().isBlockedByObstacle() && current.getOpposite().isBlockedByObstacle()) {
					view.highlight(NARROW_PASSAGE_LEFT);
					setState(NARROW_PASSAGE_LEFT);
				} else if (view.getNext().getOpposite().isBlockedByObstacle()) {
					view.getNext().getOpposite().highlight(OBSTACLE_FOUND);
					setState(OBSTACLE_FOUND);
				} else {
					selectedCar = null;
					setState(INIT);
				}
			}
		} else if (state == OBSTACLE_FOUND) {
			if (view.getBackground() == Color.red) {
				setApproachingObstacle(selectedCar.getCar(), view.getObstacle());
				view.highlight(INIT);
				selectedCar = null;
				setState(INIT);
			}
		} else if (state == NARROW_PASSAGE_ENTERED) {
			if (view.getBackground() == NARROW_PASSAGE_ENTERED) {
				hasEnteredNarrowPassage(selectedCar.getCar());
				view.highlight(INIT);
				selectedCar = null;
				setState(INIT);
			}
		} else if (state == NARROW_PASSAGE_LEFT) {
			if (view.getBackground() == NARROW_PASSAGE_LEFT) {
				hasLeftNarrowPassage(selectedCar.getCar());
				view.highlight(INIT);
				selectedCar = null;
				setState(INIT);
			}
		}
	}

	private Role<Environment> environmentRole = new Role<Environment>(Environment.class, "env");
	private Role<Car> carRole = new Role<Car>(Car.class, "car");

	private void movedToNextArea(Car car) {
		environmentRole.setBinding(CarToXObjectSystem.getInstance().CarToX.getEnvironment());
		carRole.setBinding(car);
		Message event = new Message(environmentRole, carRole, "movedToNextArea");
		runtimeAdapter.receive(event);
		runtimeAdapter.publish(event);
	}

	private void movedToNextAreaOnOvertakingLane(Car car) {
		environmentRole.setBinding(CarToXObjectSystem.getInstance().CarToX.getEnvironment());
		carRole.setBinding(car);
		Message event = new Message(environmentRole, carRole, "movedToNextAreaOnOvertakingLane");
		runtimeAdapter.receive(event);
		runtimeAdapter.publish(event);
	}

	private void changedToOppositeArea(Car car) {
		environmentRole.setBinding(CarToXObjectSystem.getInstance().CarToX.getEnvironment());
		carRole.setBinding(car);
		Message event = new Message(environmentRole, carRole, "changedToOppositeArea");
		runtimeAdapter.receive(event);
		runtimeAdapter.publish(event);
	}

	private void setApproachingObstacle(Car car, ObstacleView obstacle) {
		environmentRole.setBinding(CarToXObjectSystem.getInstance().CarToX.getEnvironment());
		carRole.setBinding(car);
		Message event = new Message(environmentRole, carRole, "setApproachingObstacle",
				obstacle.getObstacle());
		runtimeAdapter.receive(event);
		runtimeAdapter.publish(event);
	}

	private void hasEnteredNarrowPassage(Car car) {
		environmentRole.setBinding(CarToXObjectSystem.getInstance().CarToX.getEnvironment());
		carRole.setBinding(car);
		Message event = new Message(environmentRole, carRole, "hasEnteredNarrowPassage");
		runtimeAdapter.receive(event);
		runtimeAdapter.publish(event);
	}

	private void hasLeftNarrowPassage(Car car) {
		environmentRole.setBinding(CarToXObjectSystem.getInstance().CarToX.getEnvironment());
		carRole.setBinding(car);
		Message event = new Message(environmentRole, carRole, "hasLeftNarrowPassage");
		runtimeAdapter.receive(event);
		runtimeAdapter.publish(event);
	}

	private boolean carCanMove(LaneAreaView view) {
		Car car = selectedCar.getCar();

		LaneAreaView oppositeView = getOppositeView(car);
		LaneAreaView nextView = getNextView(car);

		return view == oppositeView || view == nextView;
	}

	private LaneAreaView getNextView(Car car) {
		return laneAreaMap.get(getNext(car));
	}

	private LaneAreaView getOppositeView(Car car) {
		return laneAreaMap.get(getOpposite(car));
	}

	private LaneArea getNext(Car car) {
		LaneArea current = car.getInArea();
		if (car.getOnLane() == car.getDrivingInDirectionOfLane()) {
			return current.getNext();
		} else {
			return current.getPrevious();
		}
	}

	private LaneArea getOpposite(Car car) {
		LaneArea current = car.getInArea();
		return current.getOpposite();
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void setMap(Map<LaneArea, LaneAreaView> laneAreaMap) {
		this.laneAreaMap = laneAreaMap;
	}

	public RuntimeAdapter getRuntimeAdapter() {
		return runtimeAdapter;
	}

	public void setRuntimeAdapter(RuntimeAdapter runtimeAdapter) {
		this.runtimeAdapter = runtimeAdapter;
	}

}
