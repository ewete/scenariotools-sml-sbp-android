package cartox.ui.simulation;

import cartox.StreetSection;
import cartox.runtime.CarToXObjectSystem;
import sbp.runtime.adapter.RuntimeAdapter;
import sbp.ui.ConsoleFrame;

@SuppressWarnings("serial")
public class CarToXSimulationFrame extends ConsoleFrame {

	private RuntimeAdapter runtimeAdapter;

	public RuntimeAdapter getRuntimeAdapter() {
		return runtimeAdapter;
	}
	
	/**
	 * Create the frame.
	 */
	public CarToXSimulationFrame(String name, RuntimeAdapter adapter) {
		super(name);
		this.runtimeAdapter = adapter;
		LaneAreaViewMouseListener.getInstance().setRuntimeAdapter(runtimeAdapter);

		StreetSection streetSectionModel = CarToXObjectSystem.getInstance().CarToX.getStreetSections().get(0);
		StreetSectionView streetSectionView = new StreetSectionView(streetSectionModel, this);

		addPanel("Simulation", streetSectionView);

		pack();
	}

}
