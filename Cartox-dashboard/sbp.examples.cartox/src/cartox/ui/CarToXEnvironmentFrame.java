package cartox.ui;
		
import sbp.ui.ConsoleFrame;
import sbp.ui.MessageButton;
import sbp.runtime.adapter.RuntimeAdapter;

import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import cartox.runtime.CarToXObjectSystem;

import cartox.Environment;
import cartox.Car;
import cartox.Dashboard;
import cartox.ObstacleControl;

@SuppressWarnings("serial")
public class CarToXEnvironmentFrame extends ConsoleFrame {

	private RuntimeAdapter runtimeAdapter;

	private void createButtons() {
		CarToXObjectSystem objectSystem = CarToXObjectSystem.getInstance();
		JPanel buttonPane = new JPanel();
		JScrollPane scrollPane = new JScrollPane(buttonPane, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.Y_AXIS));
		
		buttonPane.add(new MessageButton<Environment, Car>(runtimeAdapter, objectSystem.CarToX.getEnvironment(), objectSystem.CarToX.getCars().get(0), "movedToNextArea"));
		buttonPane.add(new MessageButton<Environment, Car>(runtimeAdapter, objectSystem.CarToX.getEnvironment(), objectSystem.CarToX.getCars().get(1), "movedToNextArea"));
		buttonPane.add(new MessageButton<Environment, Car>(runtimeAdapter, objectSystem.CarToX.getEnvironment(), objectSystem.CarToX.getCars().get(0), "changedToOppositeArea"));
		buttonPane.add(new MessageButton<Environment, Car>(runtimeAdapter, objectSystem.CarToX.getEnvironment(), objectSystem.CarToX.getCars().get(1), "changedToOppositeArea"));
		buttonPane.add(new MessageButton<Environment, Car>(runtimeAdapter, objectSystem.CarToX.getEnvironment(), objectSystem.CarToX.getCars().get(0), "movedToNextAreaOnOvertakingLane"));
		buttonPane.add(new MessageButton<Environment, Car>(runtimeAdapter, objectSystem.CarToX.getEnvironment(), objectSystem.CarToX.getCars().get(1), "movedToNextAreaOnOvertakingLane"));
		buttonPane.add(new MessageButton<Environment, Car>(runtimeAdapter, objectSystem.CarToX.getEnvironment(), objectSystem.CarToX.getCars().get(0), "movedToNextArea"));
		buttonPane.add(new MessageButton<Environment, Car>(runtimeAdapter, objectSystem.CarToX.getEnvironment(), objectSystem.CarToX.getCars().get(1), "movedToNextArea"));
		buttonPane.add(new MessageButton<Environment, Car>(runtimeAdapter, objectSystem.CarToX.getEnvironment(), objectSystem.CarToX.getCars().get(0), "approachingObstacle"));
		buttonPane.add(new MessageButton<Environment, Car>(runtimeAdapter, objectSystem.CarToX.getEnvironment(), objectSystem.CarToX.getCars().get(1), "approachingObstacle"));
		buttonPane.add(new MessageButton<Environment, Car>(runtimeAdapter, objectSystem.CarToX.getEnvironment(), objectSystem.CarToX.getCars().get(0), "approachingObstacle"));
		buttonPane.add(new MessageButton<Environment, Car>(runtimeAdapter, objectSystem.CarToX.getEnvironment(), objectSystem.CarToX.getCars().get(1), "approachingObstacle"));
		buttonPane.add(new MessageButton<Environment, Car>(runtimeAdapter, objectSystem.CarToX.getEnvironment(), objectSystem.CarToX.getCars().get(0), "movedToNextAreaOnOvertakingLane"));
		buttonPane.add(new MessageButton<Environment, Car>(runtimeAdapter, objectSystem.CarToX.getEnvironment(), objectSystem.CarToX.getCars().get(1), "movedToNextAreaOnOvertakingLane"));
		buttonPane.add(new MessageButton<Environment, Car>(runtimeAdapter, objectSystem.CarToX.getEnvironment(), objectSystem.CarToX.getCars().get(0), "hasEnteredNarrowPassage"));
		buttonPane.add(new MessageButton<Environment, Car>(runtimeAdapter, objectSystem.CarToX.getEnvironment(), objectSystem.CarToX.getCars().get(1), "hasEnteredNarrowPassage"));
		buttonPane.add(new MessageButton<Environment, Car>(runtimeAdapter, objectSystem.CarToX.getEnvironment(), objectSystem.CarToX.getCars().get(0), "movedToNextArea"));
		buttonPane.add(new MessageButton<Environment, Car>(runtimeAdapter, objectSystem.CarToX.getEnvironment(), objectSystem.CarToX.getCars().get(1), "movedToNextArea"));
		buttonPane.add(new MessageButton<Environment, Car>(runtimeAdapter, objectSystem.CarToX.getEnvironment(), objectSystem.CarToX.getCars().get(0), "hasLeftNarrowPassage"));
		buttonPane.add(new MessageButton<Environment, Car>(runtimeAdapter, objectSystem.CarToX.getEnvironment(), objectSystem.CarToX.getCars().get(1), "hasLeftNarrowPassage"));
		buttonPane.add(new MessageButton<Environment, Car>(runtimeAdapter, objectSystem.CarToX.getEnvironment(), objectSystem.CarToX.getCars().get(0), "approachingObstacle"));
		buttonPane.add(new MessageButton<Environment, Car>(runtimeAdapter, objectSystem.CarToX.getEnvironment(), objectSystem.CarToX.getCars().get(1), "approachingObstacle"));
		buttonPane.add(new MessageButton<Environment, Car>(runtimeAdapter, objectSystem.CarToX.getEnvironment(), objectSystem.CarToX.getCars().get(0), "approachingObstacle"));
		buttonPane.add(new MessageButton<Environment, Car>(runtimeAdapter, objectSystem.CarToX.getEnvironment(), objectSystem.CarToX.getCars().get(1), "approachingObstacle"));
		buttonPane.add(new MessageButton<Environment, Car>(runtimeAdapter, objectSystem.CarToX.getEnvironment(), objectSystem.CarToX.getCars().get(0), "approachingObstacle"));
		buttonPane.add(new MessageButton<Environment, Car>(runtimeAdapter, objectSystem.CarToX.getEnvironment(), objectSystem.CarToX.getCars().get(1), "approachingObstacle"));
		buttonPane.add(new MessageButton<Environment, Car>(runtimeAdapter, objectSystem.CarToX.getEnvironment(), objectSystem.CarToX.getCars().get(0), "hasLeftNarrowPassage"));
		buttonPane.add(new MessageButton<Environment, Car>(runtimeAdapter, objectSystem.CarToX.getEnvironment(), objectSystem.CarToX.getCars().get(1), "hasLeftNarrowPassage"));
		buttonPane.add(new MessageButton<Environment, Car>(runtimeAdapter, objectSystem.CarToX.getEnvironment(), objectSystem.CarToX.getCars().get(0), "movedToNextArea"));
		buttonPane.add(new MessageButton<Environment, Car>(runtimeAdapter, objectSystem.CarToX.getEnvironment(), objectSystem.CarToX.getCars().get(1), "movedToNextArea"));
		buttonPane.add(new MessageButton<Environment, Car>(runtimeAdapter, objectSystem.CarToX.getEnvironment(), objectSystem.CarToX.getCars().get(0), "changedToOppositeArea"));
		buttonPane.add(new MessageButton<Environment, Car>(runtimeAdapter, objectSystem.CarToX.getEnvironment(), objectSystem.CarToX.getCars().get(1), "changedToOppositeArea"));
		buttonPane.add(new MessageButton<Environment, Car>(runtimeAdapter, objectSystem.CarToX.getEnvironment(), objectSystem.CarToX.getCars().get(0), "movedToNextAreaOnOvertakingLane"));
		buttonPane.add(new MessageButton<Environment, Car>(runtimeAdapter, objectSystem.CarToX.getEnvironment(), objectSystem.CarToX.getCars().get(1), "movedToNextAreaOnOvertakingLane"));
		
		add(scrollPane);
		scrollPane.setPreferredSize(new Dimension(400, 500));
	}

	public CarToXEnvironmentFrame(String name, RuntimeAdapter adapter) {
		super(name);
		this.runtimeAdapter = adapter;
		createButtons();
		
		pack();
	}

}
