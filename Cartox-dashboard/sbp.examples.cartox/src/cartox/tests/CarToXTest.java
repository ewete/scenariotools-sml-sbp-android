package cartox.tests;

import org.junit.Test;

import cartox.runtime.CarToXRunconfigSimulationUI;
import sbp.runtime.SpecificationRunconfig;
import sbp.specification.scenarios.Scenario;

public class CarToXTest {

	@Test
	public void test() throws InterruptedException {
		SpecificationRunconfig.run(CarToXRunconfigSimulationUI.class);
		
		Scenario testScenario = new TestScenario_DashboardOfJamesCarShowsGo();
		SpecificationRunconfig.getInstance().runTestScenario(testScenario);
		
		while(!testScenario.isFinished()) {
			Thread.sleep(0);
		}
	}

}
