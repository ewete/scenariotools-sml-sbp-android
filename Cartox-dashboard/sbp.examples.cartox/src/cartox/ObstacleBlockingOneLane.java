/**
 */
package cartox;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Obstacle Blocking One Lane</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see cartox.CartoxPackage#getObstacleBlockingOneLane()
 * @model
 * @generated
 */
public interface ObstacleBlockingOneLane extends Obstacle {
} // ObstacleBlockingOneLane
