package cartox.runtime;

import cartox.runtime.observers.Exple3_AnnasDashboardObserver;
import cartox.specification.CarToXSpecification;
import cartox.ui.simulation.DashboardFrame;
import sbp.runtime.SpecificationRunconfig;
import sbp.runtime.settings.Settings;

public class Exple3_CarToXRunconfig_localhost_AnnasDashboard_8873 extends SpecificationRunconfig<CarToXSpecification> {

	public static final int AnnasDashboard_port = 8873;
	public static final String AnnasDashboard_hostName = "localhost";
	
	private DashboardFrame annasDashboardFrame;
	private Exple3_AnnasDashboardObserver dashboardObserverForAnnasCar;
	
	public Exple3_CarToXRunconfig_localhost_AnnasDashboard_8873() {
		super(new CarToXSpecification());
		enableDistributedExecutionMode(AnnasDashboard_port);
		annasDashboardFrame = new DashboardFrame("Dashboard of AnnasCar on " + AnnasDashboard_port);
		dashboardObserverForAnnasCar.setLabel(annasDashboardFrame.getLabel());
		annasDashboardFrame.setVisible(true);
		Settings.setRuntimeOut(annasDashboardFrame.getRuntimeLog());
		Settings.setServerOut(annasDashboardFrame.getServerLog());
		Settings.SPECTATOR__PRINT_CAUGHT_MESSAGE = true;
	}

	@Override
	protected void registerParticipatingObjects() {
		CarToXObjectSystem objectSystem = CarToXObjectSystem.getInstance();
		registerObject(objectSystem.CarToX, UNCONTROLLABLE); // CarToX
		registerObject(objectSystem.CarToX.getCars().get(0), UNCONTROLLABLE); // JamesCar
		registerObject(objectSystem.CarToX.getCars().get(0).getDashboard(), UNCONTROLLABLE); // JamesCarDashboard
		registerObject(objectSystem.CarToX.getCars().get(0).getDriver(), UNCONTROLLABLE); // James
		registerObject(objectSystem.CarToX.getCars().get(1), UNCONTROLLABLE); // AnnasCar
		registerObject(objectSystem.CarToX.getCars().get(1).getDashboard(), CONTROLLABLE); // AnnasCarDashboard
		registerObject(objectSystem.CarToX.getCars().get(1).getDriver(), UNCONTROLLABLE); // Anna
		registerObject(objectSystem.CarToX.getEnvironment(), UNCONTROLLABLE); // env
		registerObject(objectSystem.CarToX.getStreetSections().get(0), UNCONTROLLABLE); // section1
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0), UNCONTROLLABLE); // lane1 // TODO: Check subclasses
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0).getLaneAreas().get(0), UNCONTROLLABLE); // lane1_area1_free
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0).getLaneAreas().get(1), UNCONTROLLABLE); // lane1_area2_beforeObstacle
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0).getLaneAreas().get(2), UNCONTROLLABLE); // lane1_area3_Obstacle
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0).getLaneAreas().get(3), UNCONTROLLABLE); // lane1_area4_afterObstacle
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0).getLaneAreas().get(4), UNCONTROLLABLE); // lane1_area5_free
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1), UNCONTROLLABLE); // lane2 // TODO: Check subclasses
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1).getLaneAreas().get(0), UNCONTROLLABLE); // lane2_area1_free
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1).getLaneAreas().get(1), UNCONTROLLABLE); // lane2_area2_beforeNarrowPassage
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1).getLaneAreas().get(2), UNCONTROLLABLE); // lane2_area3_NarrowPassage
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1).getLaneAreas().get(3), UNCONTROLLABLE); // lane2_area4_afterNarrowPassage
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1).getLaneAreas().get(4), UNCONTROLLABLE); // lane2_area5_free
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getObstacles().get(0), UNCONTROLLABLE); // obstacle // TODO: Check subclasses
		registerObject(objectSystem.CarToX.getObstacleControls().get(0), UNCONTROLLABLE); // obstacleControl
	}

	@Override
	protected void registerNetworkAdressesForObjects() {
		CarToXObjectSystem objectSystem = CarToXObjectSystem.getInstance();
		registerAddress(objectSystem.CarToX.getCars().get(1), Exple3_CarToXRunconfig_localhost_AnnasCar_8872.AnnasCar_hostName, Exple3_CarToXRunconfig_localhost_AnnasCar_8872.AnnasCar_port);
	}
	
	@Override
	protected void registerObservers() {
		dashboardObserverForAnnasCar = new Exple3_AnnasDashboardObserver();
		addScenarioObserver(dashboardObserverForAnnasCar);
	}
	
	public static void main(String[] args) {
		SpecificationRunconfig.run(Exple3_CarToXRunconfig_localhost_AnnasDashboard_8873.class);
	}

}
