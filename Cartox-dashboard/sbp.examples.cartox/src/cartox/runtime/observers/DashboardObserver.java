package cartox.runtime.observers;

import java.awt.Color;

import javax.swing.JLabel;

import cartox.Dashboard;
import cartox.runtime.CarToXObjectSystem;
import sbp.specification.events.Message;
import sbp.specification.role.Role;
import sbp.specification.scenarios.Scenario;
import sbp.specification.scenarios.utils.ScenarioObserver;

public class DashboardObserver extends ScenarioObserver {

	private JLabel label;

	public JLabel getLabel() {
		return label;
	}

	public void setLabel(JLabel label) {
		this.label = label;
	}

	@Override
	public void trigger(Scenario scenario, Message messageEvent) {
		Role receiver = messageEvent.getReceiver();
		String message = messageEvent.getMessage();
		if (receiver.getBinding() instanceof Dashboard) {
			Dashboard dashboard = (Dashboard) receiver.getBinding();
			if (dashboard == CarToXObjectSystem.getInstance().CarToX.getCars().get(0).getDashboard()) {
				if (message.equals("showGo")) {
					showGo();
				} else if (message.equals("showStop")) {
					showStop();
				}
			}
		}
	}

	private void showGo() {
		label.setText("Go!");
		label.setForeground(Color.green);
	}

	private void showStop() {
		label.setText("Stop!");
		label.setForeground(Color.red);
	}

}
