package cartox.runtime;

import cartox.specification.CarToXSpecification;
import sbp.runtime.SpecificationRunconfig;
import sbp.runtime.settings.Settings;
import cartox.ui.CarToXEnvironmentFrame;

public class CarToXRunconfigUI extends SpecificationRunconfig<CarToXSpecification> {

	public CarToXRunconfigUI() {
		super(new CarToXSpecification());
		CarToXEnvironmentFrame logFrame = new CarToXEnvironmentFrame("CarToXSpecification",getAdapter());
		logFrame.setVisible(true);
		Settings.setRuntimeOut(logFrame.getRuntimeLog());
		Settings.setServerOut(logFrame.getServerLog());
		Settings.SPECTATOR__PRINT_CAUGHT_MESSAGE = true;
	}

	@Override
	protected void registerParticipatingObjects() {
		CarToXObjectSystem objectSystem = CarToXObjectSystem.getInstance();
		registerObject(objectSystem.CarToX, CONTROLLABLE); // CarToX
		registerObject(objectSystem.CarToX.getCars().get(0), CONTROLLABLE); // JamesCar
		registerObject(objectSystem.CarToX.getCars().get(0).getDashboard(), UNCONTROLLABLE); // JamesCarDashboard
		registerObject(objectSystem.CarToX.getCars().get(0).getDriver(), UNCONTROLLABLE); // James
		registerObject(objectSystem.CarToX.getCars().get(1), CONTROLLABLE); // AnnasCar
		registerObject(objectSystem.CarToX.getCars().get(1).getDashboard(), UNCONTROLLABLE); // AnnasCarDashboard
		registerObject(objectSystem.CarToX.getCars().get(1).getDriver(), UNCONTROLLABLE); // Anna
		registerObject(objectSystem.CarToX.getEnvironment(), UNCONTROLLABLE); // env
		registerObject(objectSystem.CarToX.getStreetSections().get(0), UNCONTROLLABLE); // section1
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0), CONTROLLABLE); // lane1 // TODO: Check subclasses
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0).getLaneAreas().get(0), UNCONTROLLABLE); // lane1_area1_free
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0).getLaneAreas().get(1), UNCONTROLLABLE); // lane1_area2_beforeObstacle
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0).getLaneAreas().get(2), UNCONTROLLABLE); // lane1_area3_Obstacle
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0).getLaneAreas().get(3), UNCONTROLLABLE); // lane1_area4_afterObstacle
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0).getLaneAreas().get(4), UNCONTROLLABLE); // lane1_area5_free
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1), CONTROLLABLE); // lane2 // TODO: Check subclasses
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1).getLaneAreas().get(0), UNCONTROLLABLE); // lane2_area1_free
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1).getLaneAreas().get(1), UNCONTROLLABLE); // lane2_area2_beforeNarrowPassage
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1).getLaneAreas().get(2), UNCONTROLLABLE); // lane2_area3_NarrowPassage
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1).getLaneAreas().get(3), UNCONTROLLABLE); // lane2_area4_afterNarrowPassage
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1).getLaneAreas().get(4), UNCONTROLLABLE); // lane2_area5_free
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getObstacles().get(0), CONTROLLABLE); // obstacle // TODO: Check subclasses
		registerObject(objectSystem.CarToX.getObstacleControls().get(0), CONTROLLABLE); // obstacleControl
	}

	@Override
	protected void registerNetworkAdressesForObjects() {
		CarToXObjectSystem objectSystem = CarToXObjectSystem.getInstance();
		// registerAddress(objectSystem.CarToX.getCars().get(0), JAMESCAR_HOSTNAME, JAMESCAR_PORT);
		// registerAddress(objectSystem.CarToX.getCars().get(1), ANNASCAR_HOSTNAME, ANNASCAR_PORT);
		// registerAddress(objectSystem.CarToX.getEnvironment(), ENV_HOSTNAME, ENV_PORT);
		// registerAddress(objectSystem.CarToX.getStreetSections().get(0), SECTION1_HOSTNAME, SECTION1_PORT);
		// registerAddress(objectSystem.CarToX.getObstacleControls().get(0), OBSTACLECONTROL_HOSTNAME, OBSTACLECONTROL_PORT);
	}
	
	@Override
	protected void registerObservers() {
		// Add Observers here.
		// addScenarioObserver(scenarioObserver);
	}
	
	public static void main(String[] args) {
		SpecificationRunconfig.run(CarToXRunconfigUI.class);
	}

}
