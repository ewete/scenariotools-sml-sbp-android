package cartox.runtime;

import cartox.specification.CarToXSpecification;
import cartox.ui.simulation.CarToXSimulationFrame;
import sbp.runtime.SpecificationRunconfig;
import sbp.runtime.settings.Settings;

public class Exple1_CarToXRunconfig_localhost_Car_8888 extends SpecificationRunconfig<CarToXSpecification> {
	
	public static final int JamesCar_port = 8888;
	public static final String JamesCar_hostName = "localhost";

	public Exple1_CarToXRunconfig_localhost_Car_8888() {
		super(new CarToXSpecification());
		enableDistributedExecutionMode(JamesCar_port);
		CarToXSimulationFrame logFrame = new CarToXSimulationFrame("JamesCar on " + JamesCar_port, getAdapter());
		logFrame.setVisible(true);
		Settings.setRuntimeOut(logFrame.getRuntimeLog());
		Settings.setServerOut(logFrame.getServerLog());
		Settings.SPECTATOR__PRINT_CAUGHT_MESSAGE = true;
	}

	@Override
	protected void registerParticipatingObjects() {
		CarToXObjectSystem objectSystem = CarToXObjectSystem.getInstance();
		registerObject(objectSystem.CarToX, CONTROLLABLE); // CarToX
		registerObject(objectSystem.CarToX.getCars().get(0), CONTROLLABLE); // JamesCar
		registerObject(objectSystem.CarToX.getCars().get(0).getDashboard(), UNCONTROLLABLE); // JamesCarDashboard
		registerObject(objectSystem.CarToX.getCars().get(0).getDriver(), UNCONTROLLABLE); // James
		registerObject(objectSystem.CarToX.getEnvironment(), UNCONTROLLABLE); // env
		registerObject(objectSystem.CarToX.getStreetSections().get(0), UNCONTROLLABLE); // section1
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0), CONTROLLABLE); // lane1 // TODO: Check subclasses
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0).getLaneAreas().get(0), UNCONTROLLABLE); // lane1_area1_free
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0).getLaneAreas().get(1), UNCONTROLLABLE); // lane1_area2_beforeObstacle
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0).getLaneAreas().get(2), UNCONTROLLABLE); // lane1_area3_Obstacle
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0).getLaneAreas().get(3), UNCONTROLLABLE); // lane1_area4_afterObstacle
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0).getLaneAreas().get(4), UNCONTROLLABLE); // lane1_area5_free
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1), CONTROLLABLE); // lane2 // TODO: Check subclasses
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1).getLaneAreas().get(0), UNCONTROLLABLE); // lane2_area1_free
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1).getLaneAreas().get(1), UNCONTROLLABLE); // lane2_area2_beforeNarrowPassage
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1).getLaneAreas().get(2), UNCONTROLLABLE); // lane2_area3_NarrowPassage
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1).getLaneAreas().get(3), UNCONTROLLABLE); // lane2_area4_afterNarrowPassage
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1).getLaneAreas().get(4), UNCONTROLLABLE); // lane2_area5_free
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getObstacles().get(0), CONTROLLABLE); // obstacle // TODO: Check subclasses
		registerObject(objectSystem.CarToX.getObstacleControls().get(0), CONTROLLABLE); // obstacleControl
	}

	@Override
	protected void registerNetworkAdressesForObjects() {
		CarToXObjectSystem objectSystem = CarToXObjectSystem.getInstance();
		registerAddress(objectSystem.CarToX.getCars().get(0).getDashboard(), Exple1_CarToXRunconfig_localhost_Dashboard_8889.JamesDashboard_hostName, Exple1_CarToXRunconfig_localhost_Dashboard_8889.JamesDashboard_port);
	}
	
	@Override
	protected void registerObservers() {
	}
	
	public static void main(String[] args) {
		SpecificationRunconfig.run(Exple1_CarToXRunconfig_localhost_Car_8888.class);
	}

}
