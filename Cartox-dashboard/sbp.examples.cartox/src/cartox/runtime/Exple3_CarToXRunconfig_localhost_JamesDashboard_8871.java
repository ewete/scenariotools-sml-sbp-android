package cartox.runtime;

import cartox.runtime.observers.Exple3_JamesDashboardObserver;
import cartox.specification.CarToXSpecification;
import cartox.ui.simulation.DashboardFrame;
import sbp.runtime.SpecificationRunconfig;
import sbp.runtime.settings.Settings;

public class Exple3_CarToXRunconfig_localhost_JamesDashboard_8871 extends SpecificationRunconfig<CarToXSpecification> {

	public static final int JamesDashboard_port = 8871;
	public static final String JamesDashboard_hostName = "localhost";
	
	private DashboardFrame jamesDashboardFrame;
	private Exple3_JamesDashboardObserver dashboardObserverForJamesCar;
	
	public Exple3_CarToXRunconfig_localhost_JamesDashboard_8871() {
		super(new CarToXSpecification());
		enableDistributedExecutionMode(JamesDashboard_port);
		jamesDashboardFrame = new DashboardFrame("Dashboard of JamesCar on " + JamesDashboard_port);
		dashboardObserverForJamesCar.setLabel(jamesDashboardFrame.getLabel());
		jamesDashboardFrame.setVisible(true);
		Settings.setRuntimeOut(jamesDashboardFrame.getRuntimeLog());
		Settings.setServerOut(jamesDashboardFrame.getServerLog());
		Settings.SPECTATOR__PRINT_CAUGHT_MESSAGE = true;
	}

	@Override
	protected void registerParticipatingObjects() {
		CarToXObjectSystem objectSystem = CarToXObjectSystem.getInstance();
		registerObject(objectSystem.CarToX, UNCONTROLLABLE); // CarToX
		registerObject(objectSystem.CarToX.getCars().get(0), UNCONTROLLABLE); // JamesCar
		registerObject(objectSystem.CarToX.getCars().get(0).getDashboard(), CONTROLLABLE); // JamesCarDashboard
		registerObject(objectSystem.CarToX.getCars().get(0).getDriver(), UNCONTROLLABLE); // James
		registerObject(objectSystem.CarToX.getCars().get(1), UNCONTROLLABLE); // AnnasCar
		registerObject(objectSystem.CarToX.getCars().get(1).getDashboard(), UNCONTROLLABLE); // AnnasCarDashboard
		registerObject(objectSystem.CarToX.getCars().get(1).getDriver(), UNCONTROLLABLE); // Anna
		registerObject(objectSystem.CarToX.getEnvironment(), UNCONTROLLABLE); // env
		registerObject(objectSystem.CarToX.getStreetSections().get(0), UNCONTROLLABLE); // section1
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0), UNCONTROLLABLE); // lane1 // TODO: Check subclasses
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0).getLaneAreas().get(0), UNCONTROLLABLE); // lane1_area1_free
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0).getLaneAreas().get(1), UNCONTROLLABLE); // lane1_area2_beforeObstacle
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0).getLaneAreas().get(2), UNCONTROLLABLE); // lane1_area3_Obstacle
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0).getLaneAreas().get(3), UNCONTROLLABLE); // lane1_area4_afterObstacle
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0).getLaneAreas().get(4), UNCONTROLLABLE); // lane1_area5_free
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1), UNCONTROLLABLE); // lane2 // TODO: Check subclasses
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1).getLaneAreas().get(0), UNCONTROLLABLE); // lane2_area1_free
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1).getLaneAreas().get(1), UNCONTROLLABLE); // lane2_area2_beforeNarrowPassage
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1).getLaneAreas().get(2), UNCONTROLLABLE); // lane2_area3_NarrowPassage
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1).getLaneAreas().get(3), UNCONTROLLABLE); // lane2_area4_afterNarrowPassage
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1).getLaneAreas().get(4), UNCONTROLLABLE); // lane2_area5_free
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getObstacles().get(0), UNCONTROLLABLE); // obstacle // TODO: Check subclasses
		registerObject(objectSystem.CarToX.getObstacleControls().get(0), UNCONTROLLABLE); // obstacleControl
	}

	@Override
	protected void registerNetworkAdressesForObjects() {
		CarToXObjectSystem objectSystem = CarToXObjectSystem.getInstance();
		registerAddress(objectSystem.CarToX.getCars().get(0), Exple3_CarToXRunconfig_localhost_JamesCar_8870.JamesCar_hostName, Exple3_CarToXRunconfig_localhost_JamesCar_8870.JamesCar_port);
	}
	
	@Override
	protected void registerObservers() {
		dashboardObserverForJamesCar = new Exple3_JamesDashboardObserver();
		addScenarioObserver(dashboardObserverForJamesCar);
	}
	
	public static void main(String[] args) {
		SpecificationRunconfig.run(Exple3_CarToXRunconfig_localhost_JamesDashboard_8871.class);
	}

}
