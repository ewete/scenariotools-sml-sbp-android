package cartox.runtime;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import java.util.Map;

import cartox.CarToX;
import cartox.CartoxPackage;

public class CarToXObjectSystem {

    private static CarToXObjectSystem instance;

    private CarToXObjectSystem(){
    }

    public static CarToXObjectSystem getInstance() {
        if (instance == null)
            instance = new CarToXObjectSystem();
        return instance;
    }

    public CarToX CarToX = null; //(CarToX) loadCarToX("spec/TwoCarsAndObstacle/cartox.xmi");

    public CarToX loadCarToX(String uri) {
        CartoxPackage.eINSTANCE.eClass();
        Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
        Map<String, Object> m = reg.getExtensionToFactoryMap();
        m.put("xmi", new XMIResourceFactoryImpl());
        ResourceSet resSet = new ResourceSetImpl();
        Resource resource = resSet.getResource(URI.createURI(uri), true);
        CarToX carToX = (CarToX) resource.getContents().get(0);
        return carToX;
    }

}
