package cartox.runtime;

import cartox.specification.CarToXSpecification;
import cartox.ui.simulation.CarToXSimulationFrame;
import sbp.runtime.SpecificationRunconfig;
import sbp.runtime.settings.Settings;

public class Exple3_CarToXRunconfig_localhost_AnnasCar_8872 extends SpecificationRunconfig<CarToXSpecification> {

	public static final int AnnasCar_port = 8872;
	public static final String AnnasCar_hostName = "localhost";
	
	public Exple3_CarToXRunconfig_localhost_AnnasCar_8872() {
		super(new CarToXSpecification());
		enableDistributedExecutionMode(AnnasCar_port);
		CarToXSimulationFrame logFrame = new CarToXSimulationFrame("AnnasCar on " + AnnasCar_port, getAdapter());
		logFrame.setVisible(true);
		Settings.setRuntimeOut(logFrame.getRuntimeLog());
		Settings.setServerOut(logFrame.getServerLog());
		Settings.SPECTATOR__PRINT_CAUGHT_MESSAGE = true;
	}

	@Override
	protected void registerParticipatingObjects() {
		CarToXObjectSystem objectSystem = CarToXObjectSystem.getInstance();
		registerObject(objectSystem.CarToX, CONTROLLABLE); // CarToX
		registerObject(objectSystem.CarToX.getCars().get(0), UNCONTROLLABLE); // JamesCar
		registerObject(objectSystem.CarToX.getCars().get(0).getDashboard(), UNCONTROLLABLE); // JamesCarDashboard
		registerObject(objectSystem.CarToX.getCars().get(0).getDriver(), UNCONTROLLABLE); // James
		registerObject(objectSystem.CarToX.getCars().get(1), CONTROLLABLE); // AnnasCar
		registerObject(objectSystem.CarToX.getCars().get(1).getDashboard(), UNCONTROLLABLE); // AnnasCarDashboard
		registerObject(objectSystem.CarToX.getCars().get(1).getDriver(), UNCONTROLLABLE); // Anna
		registerObject(objectSystem.CarToX.getEnvironment(), UNCONTROLLABLE); // env
		registerObject(objectSystem.CarToX.getStreetSections().get(0), UNCONTROLLABLE); // section1
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0), CONTROLLABLE); // lane1 // TODO: Check subclasses
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0).getLaneAreas().get(0), UNCONTROLLABLE); // lane1_area1_free
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0).getLaneAreas().get(1), UNCONTROLLABLE); // lane1_area2_beforeObstacle
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0).getLaneAreas().get(2), UNCONTROLLABLE); // lane1_area3_Obstacle
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0).getLaneAreas().get(3), UNCONTROLLABLE); // lane1_area4_afterObstacle
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(0).getLaneAreas().get(4), UNCONTROLLABLE); // lane1_area5_free
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1), CONTROLLABLE); // lane2 // TODO: Check subclasses
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1).getLaneAreas().get(0), UNCONTROLLABLE); // lane2_area1_free
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1).getLaneAreas().get(1), UNCONTROLLABLE); // lane2_area2_beforeNarrowPassage
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1).getLaneAreas().get(2), UNCONTROLLABLE); // lane2_area3_NarrowPassage
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1).getLaneAreas().get(3), UNCONTROLLABLE); // lane2_area4_afterNarrowPassage
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getLanes().get(1).getLaneAreas().get(4), UNCONTROLLABLE); // lane2_area5_free
		registerObject(objectSystem.CarToX.getStreetSections().get(0).getObstacles().get(0), CONTROLLABLE); // obstacle // TODO: Check subclasses
		registerObject(objectSystem.CarToX.getObstacleControls().get(0), CONTROLLABLE); // obstacleControl
	}

	@Override
	protected void registerNetworkAdressesForObjects() {
		CarToXObjectSystem objectSystem = CarToXObjectSystem.getInstance();
		registerAddress(objectSystem.CarToX.getCars().get(1).getDashboard(), Exple3_CarToXRunconfig_localhost_AnnasDashboard_8873.AnnasDashboard_hostName, Exple3_CarToXRunconfig_localhost_AnnasDashboard_8873.AnnasDashboard_port);
	}
	
	@Override
	protected void registerObservers() {
	}
	
	public static void main(String[] args) {
		SpecificationRunconfig.run(Exple3_CarToXRunconfig_localhost_AnnasCar_8872.class);
	}

}
