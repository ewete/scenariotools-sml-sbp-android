/**
 */
package cartox.impl;

import cartox.CartoxPackage;
import cartox.LaneArea;
import cartox.Obstacle;
import cartox.ObstacleControl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Obstacle</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link cartox.impl.ObstacleImpl#getObstacleArea <em>Obstacle Area</em>}</li>
 *   <li>{@link cartox.impl.ObstacleImpl#getControlledBy <em>Controlled By</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ObstacleImpl extends NamedElementImpl implements Obstacle {
	/**
	 * The cached value of the '{@link #getObstacleArea() <em>Obstacle Area</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObstacleArea()
	 * @generated
	 * @ordered
	 */
	protected LaneArea obstacleArea;

	/**
	 * The cached value of the '{@link #getControlledBy() <em>Controlled By</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getControlledBy()
	 * @generated
	 * @ordered
	 */
	protected ObstacleControl controlledBy;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ObstacleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CartoxPackage.Literals.OBSTACLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LaneArea getObstacleArea() {
		if (obstacleArea != null && obstacleArea.eIsProxy()) {
			InternalEObject oldObstacleArea = (InternalEObject)obstacleArea;
			obstacleArea = (LaneArea)eResolveProxy(oldObstacleArea);
			if (obstacleArea != oldObstacleArea) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CartoxPackage.OBSTACLE__OBSTACLE_AREA, oldObstacleArea, obstacleArea));
			}
		}
		return obstacleArea;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LaneArea basicGetObstacleArea() {
		return obstacleArea;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObstacleArea(LaneArea newObstacleArea, NotificationChain msgs) {
		LaneArea oldObstacleArea = obstacleArea;
		obstacleArea = newObstacleArea;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CartoxPackage.OBSTACLE__OBSTACLE_AREA, oldObstacleArea, newObstacleArea);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObstacleArea(LaneArea newObstacleArea) {
		if (newObstacleArea != obstacleArea) {
			NotificationChain msgs = null;
			if (obstacleArea != null)
				msgs = ((InternalEObject)obstacleArea).eInverseRemove(this, CartoxPackage.LANE_AREA__OBSTACLE, LaneArea.class, msgs);
			if (newObstacleArea != null)
				msgs = ((InternalEObject)newObstacleArea).eInverseAdd(this, CartoxPackage.LANE_AREA__OBSTACLE, LaneArea.class, msgs);
			msgs = basicSetObstacleArea(newObstacleArea, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CartoxPackage.OBSTACLE__OBSTACLE_AREA, newObstacleArea, newObstacleArea));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObstacleControl getControlledBy() {
		if (controlledBy != null && controlledBy.eIsProxy()) {
			InternalEObject oldControlledBy = (InternalEObject)controlledBy;
			controlledBy = (ObstacleControl)eResolveProxy(oldControlledBy);
			if (controlledBy != oldControlledBy) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CartoxPackage.OBSTACLE__CONTROLLED_BY, oldControlledBy, controlledBy));
			}
		}
		return controlledBy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObstacleControl basicGetControlledBy() {
		return controlledBy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetControlledBy(ObstacleControl newControlledBy, NotificationChain msgs) {
		ObstacleControl oldControlledBy = controlledBy;
		controlledBy = newControlledBy;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CartoxPackage.OBSTACLE__CONTROLLED_BY, oldControlledBy, newControlledBy);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setControlledBy(ObstacleControl newControlledBy) {
		if (newControlledBy != controlledBy) {
			NotificationChain msgs = null;
			if (controlledBy != null)
				msgs = ((InternalEObject)controlledBy).eInverseRemove(this, CartoxPackage.OBSTACLE_CONTROL__CONTROLLED_OBSTACLE, ObstacleControl.class, msgs);
			if (newControlledBy != null)
				msgs = ((InternalEObject)newControlledBy).eInverseAdd(this, CartoxPackage.OBSTACLE_CONTROL__CONTROLLED_OBSTACLE, ObstacleControl.class, msgs);
			msgs = basicSetControlledBy(newControlledBy, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CartoxPackage.OBSTACLE__CONTROLLED_BY, newControlledBy, newControlledBy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CartoxPackage.OBSTACLE__OBSTACLE_AREA:
				if (obstacleArea != null)
					msgs = ((InternalEObject)obstacleArea).eInverseRemove(this, CartoxPackage.LANE_AREA__OBSTACLE, LaneArea.class, msgs);
				return basicSetObstacleArea((LaneArea)otherEnd, msgs);
			case CartoxPackage.OBSTACLE__CONTROLLED_BY:
				if (controlledBy != null)
					msgs = ((InternalEObject)controlledBy).eInverseRemove(this, CartoxPackage.OBSTACLE_CONTROL__CONTROLLED_OBSTACLE, ObstacleControl.class, msgs);
				return basicSetControlledBy((ObstacleControl)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CartoxPackage.OBSTACLE__OBSTACLE_AREA:
				return basicSetObstacleArea(null, msgs);
			case CartoxPackage.OBSTACLE__CONTROLLED_BY:
				return basicSetControlledBy(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CartoxPackage.OBSTACLE__OBSTACLE_AREA:
				if (resolve) return getObstacleArea();
				return basicGetObstacleArea();
			case CartoxPackage.OBSTACLE__CONTROLLED_BY:
				if (resolve) return getControlledBy();
				return basicGetControlledBy();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CartoxPackage.OBSTACLE__OBSTACLE_AREA:
				setObstacleArea((LaneArea)newValue);
				return;
			case CartoxPackage.OBSTACLE__CONTROLLED_BY:
				setControlledBy((ObstacleControl)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CartoxPackage.OBSTACLE__OBSTACLE_AREA:
				setObstacleArea((LaneArea)null);
				return;
			case CartoxPackage.OBSTACLE__CONTROLLED_BY:
				setControlledBy((ObstacleControl)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CartoxPackage.OBSTACLE__OBSTACLE_AREA:
				return obstacleArea != null;
			case CartoxPackage.OBSTACLE__CONTROLLED_BY:
				return controlledBy != null;
		}
		return super.eIsSet(featureID);
	}

} //ObstacleImpl
