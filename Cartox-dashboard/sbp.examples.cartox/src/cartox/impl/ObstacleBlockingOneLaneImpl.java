/**
 */
package cartox.impl;

import cartox.CartoxPackage;
import cartox.ObstacleBlockingOneLane;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Obstacle Blocking One Lane</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ObstacleBlockingOneLaneImpl extends ObstacleImpl implements ObstacleBlockingOneLane {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ObstacleBlockingOneLaneImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CartoxPackage.Literals.OBSTACLE_BLOCKING_ONE_LANE;
	}

} //ObstacleBlockingOneLaneImpl
