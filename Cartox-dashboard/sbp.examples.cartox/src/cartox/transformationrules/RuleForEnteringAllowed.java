package cartox.transformationrules;

import cartox.Car;
import cartox.ObstacleControl;
import sbp.specification.events.Message;

public class RuleForEnteringAllowed extends AbstractRule {

	@Override
	public Message getTriggerMessage() {
		return enteringAllowed;
	}

	@Override
	public void execute(Message event) {
		ObstacleControl obstacleControl = (ObstacleControl) event.getSender().getBinding();
		Car car = (Car) event.getReceiver().getBinding();
		obstacleControl.getRegisteredCarsWaiting().remove(car);
		obstacleControl.getCarsOnNarrowPassageLaneAllowedToPass().add(car);
	}

}
