package cartox.transformationrules;

import cartox.Car;
import cartox.LaneArea;
import cartox.Obstacle;
import sbp.specification.events.Message;

public class RuleForMovedToNextArea extends AbstractRule {

	@Override
	public Message getTriggerMessage() {
		return movedToNextArea;
	}

	@Override
	public void execute(Message event) {
		Car car = (Car) event.getReceiver().getBinding();
		LaneArea currentArea = car.getInArea();
		LaneArea nextArea = currentArea.getNext();
		Obstacle obstacle = nextArea.getObstacle();
		if (obstacle != null) {
			throw new RuntimeException("Car " + car.getName() + " crashed into obstacle " + obstacle.getName());
		}
		car.setInArea(nextArea);
	}

}
