package cartox.transformationrules;

import cartox.Car;
import cartox.ObstacleControl;
import sbp.specification.events.Message;

public class RuleForRegister extends AbstractRule {

	@Override
	public Message getTriggerMessage() {
		return new Message(car, obstacleControl, "register");
	}

	@Override
	public void execute(Message event) {
		Car car = (Car) event.getSender().getBinding();
		ObstacleControl obstacleControl = (ObstacleControl) event.getReceiver().getBinding();
		obstacleControl.getRegisteredCarsWaiting().add(car);
	}

}
