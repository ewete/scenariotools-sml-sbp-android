package cartox.transformationrules;

import cartox.Car;
import cartox.ObstacleControl;
import sbp.specification.events.Message;

public class RuleForUnregister extends AbstractRule {

	@Override
	public Message getTriggerMessage() {
		return unregister;
	}

	@Override
	public void execute(Message event) {
		Car car = (Car) event.getSender().getBinding();
		ObstacleControl obstacleControl = (ObstacleControl) event.getReceiver().getBinding();
		car.setApproachingObstacle(null);
		obstacleControl.getCarsOnNarrowPassageLaneAllowedToPass().remove(car);
	}

}
