/**
 */
package cartox;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Driver</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see cartox.CartoxPackage#getDriver()
 * @model
 * @generated
 */
public interface Driver extends NamedElement {
} // Driver
