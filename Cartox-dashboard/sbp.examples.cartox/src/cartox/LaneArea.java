/**
 */
package cartox;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lane Area</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link cartox.LaneArea#getOpposite <em>Opposite</em>}</li>
 *   <li>{@link cartox.LaneArea#getPrevious <em>Previous</em>}</li>
 *   <li>{@link cartox.LaneArea#getNext <em>Next</em>}</li>
 *   <li>{@link cartox.LaneArea#getObstacle <em>Obstacle</em>}</li>
 * </ul>
 *
 * @see cartox.CartoxPackage#getLaneArea()
 * @model
 * @generated
 */
public interface LaneArea extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Opposite</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Opposite</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Opposite</em>' reference.
	 * @see #setOpposite(LaneArea)
	 * @see cartox.CartoxPackage#getLaneArea_Opposite()
	 * @model
	 * @generated
	 */
	LaneArea getOpposite();

	/**
	 * Sets the value of the '{@link cartox.LaneArea#getOpposite <em>Opposite</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Opposite</em>' reference.
	 * @see #getOpposite()
	 * @generated
	 */
	void setOpposite(LaneArea value);

	/**
	 * Returns the value of the '<em><b>Previous</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link cartox.LaneArea#getNext <em>Next</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Previous</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Previous</em>' reference.
	 * @see #setPrevious(LaneArea)
	 * @see cartox.CartoxPackage#getLaneArea_Previous()
	 * @see cartox.LaneArea#getNext
	 * @model opposite="next"
	 * @generated
	 */
	LaneArea getPrevious();

	/**
	 * Sets the value of the '{@link cartox.LaneArea#getPrevious <em>Previous</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Previous</em>' reference.
	 * @see #getPrevious()
	 * @generated
	 */
	void setPrevious(LaneArea value);

	/**
	 * Returns the value of the '<em><b>Next</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link cartox.LaneArea#getPrevious <em>Previous</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Next</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Next</em>' reference.
	 * @see #setNext(LaneArea)
	 * @see cartox.CartoxPackage#getLaneArea_Next()
	 * @see cartox.LaneArea#getPrevious
	 * @model opposite="previous"
	 * @generated
	 */
	LaneArea getNext();

	/**
	 * Sets the value of the '{@link cartox.LaneArea#getNext <em>Next</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Next</em>' reference.
	 * @see #getNext()
	 * @generated
	 */
	void setNext(LaneArea value);

	/**
	 * Returns the value of the '<em><b>Obstacle</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link cartox.Obstacle#getObstacleArea <em>Obstacle Area</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Obstacle</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Obstacle</em>' reference.
	 * @see #setObstacle(Obstacle)
	 * @see cartox.CartoxPackage#getLaneArea_Obstacle()
	 * @see cartox.Obstacle#getObstacleArea
	 * @model opposite="obstacleArea"
	 * @generated
	 */
	Obstacle getObstacle();

	/**
	 * Sets the value of the '{@link cartox.LaneArea#getObstacle <em>Obstacle</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Obstacle</em>' reference.
	 * @see #getObstacle()
	 * @generated
	 */
	void setObstacle(Obstacle value);

} // LaneArea
