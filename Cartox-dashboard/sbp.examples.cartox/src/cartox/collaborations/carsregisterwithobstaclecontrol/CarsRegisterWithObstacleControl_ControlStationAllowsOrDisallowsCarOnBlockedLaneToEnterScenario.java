package cartox.collaborations.carsregisterwithobstaclecontrol;

import sbp.specification.scenarios.violations.Violation;
import sbp.specification.events.Message;

import java.util.List;
import java.util.ArrayList;

// Collaboration
import cartox.collaborations.carsregisterwithobstaclecontrol.CarsRegisterWithObstacleControlCollaboration;

// Roles
import cartox.Obstacle;
import cartox.ObstacleControl;
import cartox.Dashboard;

@SuppressWarnings("serial")
public class CarsRegisterWithObstacleControl_ControlStationAllowsOrDisallowsCarOnBlockedLaneToEnterScenario extends CarsRegisterWithObstacleControlCollaboration {

	@Override
	protected void registerAlphabet() {
		setBlocked(env, car, "setApproachingObstacle", 
		RANDOM
		);
		setBlocked(car, obstacleControl, "register");
		setBlocked(obstacleControl, car, "enteringAllowed");
		setBlocked(car, dashboard, "showGo");
		setBlocked(obstacleControl, car, "enteringDisallowed");
		setBlocked(car, dashboard, "showStop");
	}
	
	@Override
	protected void initialisation() {
		addInitializingMessage(new Message(env, car, "setApproachingObstacle", 
		RANDOM
		));
	}
	
	@Override
	protected void registerRoleBindings() {
		bindRoleToObject(obstacle, (Obstacle) car
		.getBinding().getApproachingObstacle()
		);
		bindRoleToObject(obstacleControl, (ObstacleControl) obstacle
		.getBinding().getControlledBy()
		);
		bindRoleToObject(dashboard, (Dashboard) car
		.getBinding().getDashboard()
		);
	}

	@Override
	protected void body() throws Violation {
		//true
		request(STRICT, car, obstacleControl, "register");
		// Begin Alternative
		List<Message> requestedMessages = new ArrayList<Message>();
		List<Message> waitedForMessages = new ArrayList<Message>();
		if (obstacleControl
		.getBinding().getCarsOnNarrowPassageLaneAllowedToPass()
		.isEmpty()
		) {
		requestedMessages.add(new Message(STRICT, obstacleControl, car, "enteringAllowed"));
		//true
		}
		if (! obstacleControl
		.getBinding().getCarsOnNarrowPassageLaneAllowedToPass()
		.isEmpty()
		) {
		requestedMessages.add(new Message(STRICT, obstacleControl, car, "enteringDisallowed"));
		//true
		}
		// Insert into BP
		doStep(requestedMessages, waitedForMessages);
		// Determine which path has been chosen
		if (getLastMessage().equals(new Message(obstacleControl, car, "enteringAllowed"))) {
			//true
			waitFor(STRICT, car, dashboard, "showGo");
		}else
		if (getLastMessage().equals(new Message(obstacleControl, car, "enteringDisallowed"))) {
			//true
			waitFor(STRICT, car, dashboard, "showStop");
		}
		// End Alternative
	}

	

}
