package cartox.collaborations.carsregisterwithobstaclecontrol;

import sbp.specification.scenarios.violations.Violation;
import sbp.specification.events.Message;


// Collaboration
import cartox.collaborations.carsregisterwithobstaclecontrol.CarsRegisterWithObstacleControlCollaboration;

// Roles
import cartox.Car;

@SuppressWarnings("serial")
public class CarsRegisterWithObstacleControl_ControlStationTellsNextRegisteredCarToEnterScenario extends CarsRegisterWithObstacleControlCollaboration {

	@Override
	protected void registerAlphabet() {
		setBlocked(car, obstacleControl, "unregister");
		setBlocked(obstacleControl, nextCar, "enteringAllowed");
	}
	
	@Override
	protected void initialisation() {
		addInitializingMessage(new Message(car, obstacleControl, "unregister"));
	}
	
	@Override
	protected void registerRoleBindings() {
		bindRoleToObject(nextCar, (Car) obstacleControl
		.getBinding().getRegisteredCarsWaiting()
		.get(0)
		);
	}

	@Override
	protected void body() throws Violation {
		//true
		if ((nextCar.getBinding()
		 == null
		)
		) {
			throwViolation(INTERRUPT);
		}
		request(STRICT, obstacleControl, nextCar, "enteringAllowed");
	}

	

}
