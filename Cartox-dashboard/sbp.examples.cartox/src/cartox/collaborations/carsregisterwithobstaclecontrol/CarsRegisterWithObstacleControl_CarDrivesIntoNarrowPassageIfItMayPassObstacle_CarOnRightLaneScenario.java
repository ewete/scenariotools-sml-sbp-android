package cartox.collaborations.carsregisterwithobstaclecontrol;

import sbp.specification.scenarios.violations.Violation;
import sbp.specification.events.Message;

import java.util.List;
import java.util.ArrayList;

// Collaboration
import cartox.collaborations.carsregisterwithobstaclecontrol.CarsRegisterWithObstacleControlCollaboration;

// Roles
import cartox.LaneArea;
import cartox.Environment;

@SuppressWarnings("serial")
public class CarsRegisterWithObstacleControl_CarDrivesIntoNarrowPassageIfItMayPassObstacle_CarOnRightLaneScenario extends CarsRegisterWithObstacleControlCollaboration {

	@Override
	protected void registerAlphabet() {
		setBlocked(obstacleControl, car, "enteringAllowed");
		setBlocked(env, car, "movedToNextArea");
		setBlocked(env, car, "changedToOppositeArea");
		setBlocked(env, car, "movedToNextAreaOnOvertakingLane");
	}
	
	@Override
	protected void initialisation() {
		addInitializingMessage(new Message(obstacleControl, car, "enteringAllowed"));
	}
	
	@Override
	protected void registerRoleBindings() {
		bindRoleToObject(currentArea, (LaneArea) car
		.getBinding().getInArea()
		);
		bindRoleToObject(nextArea, (LaneArea) currentArea
		.getBinding().getNext()
		);
		bindRoleToObject(env, (Environment) car
		.getBinding().getEnvironment()
		);
	}

	@Override
	protected void body() throws Violation {
		//true
		if ((car
		.getBinding().getOnLane()
		 != car
		.getBinding().getDrivingInDirectionOfLane()
		)
		) {
			throwViolation(INTERRUPT);
		}
		// Begin Alternative
		List<Message> requestedMessages = new ArrayList<Message>();
		List<Message> waitedForMessages = new ArrayList<Message>();
		if ((nextArea
		.getBinding().getObstacle()
		 == null
		)
		) {
		requestedMessages.add(new Message(STRICT, env, car, "movedToNextArea"));
		//true
		}
		if ((nextArea
		.getBinding().getObstacle()
		 != null
		)
		) {
		requestedMessages.add(new Message(STRICT, env, car, "changedToOppositeArea"));
		//true
		}
		// Insert into BP
		doStep(requestedMessages, waitedForMessages);
		// Determine which path has been chosen
		if (getLastMessage().equals(new Message(env, car, "movedToNextArea"))) {
			//true
		}else
		if (getLastMessage().equals(new Message(env, car, "changedToOppositeArea"))) {
			//true
			request(STRICT, env, car, "movedToNextAreaOnOvertakingLane");
		}
		// End Alternative
	}

	

}
