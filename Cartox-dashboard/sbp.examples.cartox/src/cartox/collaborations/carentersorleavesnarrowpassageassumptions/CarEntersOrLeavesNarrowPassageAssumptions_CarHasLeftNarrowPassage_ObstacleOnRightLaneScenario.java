package cartox.collaborations.carentersorleavesnarrowpassageassumptions;

import sbp.specification.scenarios.violations.Violation;
import sbp.specification.events.Message;


// Collaboration
import cartox.collaborations.carentersorleavesnarrowpassageassumptions.CarEntersOrLeavesNarrowPassageAssumptionsCollaboration;

// Roles
import cartox.LaneArea;

@SuppressWarnings("serial")
public class CarEntersOrLeavesNarrowPassageAssumptions_CarHasLeftNarrowPassage_ObstacleOnRightLaneScenario extends CarEntersOrLeavesNarrowPassageAssumptionsCollaboration {

	@Override
	protected void registerAlphabet() {
		setBlocked(env, car, "movedToNextAreaOnOvertakingLane");
		setBlocked(env, car, "hasLeftNarrowPassage");
		// Constraints
		setForbidden(env, car, "changedToOppositeArea");
	}
	
	@Override
	protected void initialisation() {
		addInitializingMessage(new Message(env, car, "movedToNextAreaOnOvertakingLane"));
	}
	
	@Override
	protected void registerRoleBindings() {
		bindRoleToObject(currentArea, (LaneArea) car
		.getBinding().getInArea()
		);
		bindRoleToObject(oppositeArea, (LaneArea) currentArea
		.getBinding().getOpposite()
		);
		bindRoleToObject(oppositePrevArea, (LaneArea) oppositeArea
		.getBinding().getPrevious()
		);
	}

	@Override
	protected void body() throws Violation {
		//true
		if (((oppositeArea
		.getBinding().getObstacle()
		 != null
		)
		 | (oppositePrevArea
		.getBinding().getObstacle()
		 == null
		)
		)
		) {
			throwViolation(INTERRUPT);
		}
		request(STRICT, env, car, "hasLeftNarrowPassage");
	}

	

}
