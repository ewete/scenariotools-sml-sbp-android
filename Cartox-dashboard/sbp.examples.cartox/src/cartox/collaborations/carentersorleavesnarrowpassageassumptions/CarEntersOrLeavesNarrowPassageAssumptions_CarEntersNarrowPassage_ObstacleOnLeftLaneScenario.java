package cartox.collaborations.carentersorleavesnarrowpassageassumptions;

import sbp.specification.scenarios.violations.Violation;
import sbp.specification.events.Message;


// Collaboration
import cartox.collaborations.carentersorleavesnarrowpassageassumptions.CarEntersOrLeavesNarrowPassageAssumptionsCollaboration;

// Roles
import cartox.LaneArea;

@SuppressWarnings("serial")
public class CarEntersOrLeavesNarrowPassageAssumptions_CarEntersNarrowPassage_ObstacleOnLeftLaneScenario extends CarEntersOrLeavesNarrowPassageAssumptionsCollaboration {

	@Override
	protected void registerAlphabet() {
		setBlocked(env, car, "movedToNextArea");
		setBlocked(env, car, "hasEnteredNarrowPassage");
	}
	
	@Override
	protected void initialisation() {
		addInitializingMessage(new Message(env, car, "movedToNextArea"));
	}
	
	@Override
	protected void registerRoleBindings() {
		bindRoleToObject(currentArea, (LaneArea) car
		.getBinding().getInArea()
		);
		bindRoleToObject(oppositeArea, (LaneArea) currentArea
		.getBinding().getOpposite()
		);
		bindRoleToObject(oppositePrevArea, (LaneArea) oppositeArea
		.getBinding().getNext()
		);
	}

	@Override
	protected void body() throws Violation {
		//true
		if (((oppositeArea
		.getBinding().getObstacle()
		 == null
		)
		 | (oppositePrevArea
		.getBinding().getObstacle()
		 != null
		)
		)
		) {
			throwViolation(INTERRUPT);
		}
		request(STRICT, env, car, "hasEnteredNarrowPassage");
	}

	

}
