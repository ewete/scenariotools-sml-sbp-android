package cartox.collaborations.cardriving;

import sbp.specification.scenarios.violations.Violation;
import sbp.specification.events.Message;


// Collaboration
import cartox.collaborations.cardriving.CarDrivingCollaboration;

// Roles

@SuppressWarnings("serial")
public class CarDriving_CarMovesToNextAreaOnOvertakingLaneScenario extends CarDrivingCollaboration {

	@Override
	protected void registerAlphabet() {
		setBlocked(env, car, "movedToNextAreaOnOvertakingLane");
	}
	
	@Override
	protected void initialisation() {
		addInitializingMessage(new Message(env, car, "movedToNextAreaOnOvertakingLane"));
	}
	
	@Override
	protected void registerRoleBindings() {
	}

	@Override
	protected void body() throws Violation {
		//true
	}

	

}
