package cartox.collaborations.cardriving;

import sbp.specification.scenarios.violations.Violation;
import sbp.specification.events.Message;


// Collaboration
import cartox.collaborations.cardriving.CarDrivingCollaboration;

// Roles

@SuppressWarnings("serial")
public class CarDriving_CarMovesToNextAreaScenario extends CarDrivingCollaboration {

	@Override
	protected void registerAlphabet() {
		setBlocked(env, car, "movedToNextArea");
	}
	
	@Override
	protected void initialisation() {
		addInitializingMessage(new Message(env, car, "movedToNextArea"));
	}
	
	@Override
	protected void registerRoleBindings() {
	}

	@Override
	protected void body() throws Violation {
		//true
	}

	

}
