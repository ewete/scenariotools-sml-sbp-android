package cartox.collaborations.carapproachingobstacleassumptions;

import sbp.specification.scenarios.violations.Violation;
import sbp.specification.events.Message;


// Collaboration
import cartox.collaborations.carapproachingobstacleassumptions.CarApproachingObstacleAssumptionsCollaboration;

// Roles
import cartox.LaneArea;
import cartox.Obstacle;

@SuppressWarnings("serial")
public class CarApproachingObstacleAssumptions_ApproachingObstacleOnBlockedLaneAssumption_CarOnLeftLaneScenario extends CarApproachingObstacleAssumptionsCollaboration {

	@Override
	protected void registerAlphabet() {
		setBlocked(env, car, "movedToNextArea");
		setBlocked(env, car, "setApproachingObstacle", 
		obstacle.getBinding()
		);
		// Constraints
		setForbidden(env, car, "movedToNextArea");
	}
	
	@Override
	protected void initialisation() {
		addInitializingMessage(new Message(env, car, "movedToNextArea"));
	}
	
	@Override
	protected void registerRoleBindings() {
		bindRoleToObject(currentArea, (LaneArea) car
		.getBinding().getInArea()
		);
		bindRoleToObject(nextArea, (LaneArea) currentArea
		.getBinding().getNext()
		);
		bindRoleToObject(oppositeNextArea, (LaneArea) nextArea
		.getBinding().getOpposite()
		);
		bindRoleToObject(obstacle, (Obstacle) oppositeNextArea
		.getBinding().getObstacle()
		);
	}

	@Override
	protected void body() throws Violation {
		//true
		if ((obstacle.getBinding()
		 == null
		)
		) {
			throwViolation(INTERRUPT);
		}
		request(STRICT, env, car, "setApproachingObstacle", 
		obstacle.getBinding()
		);
	}

	

}
