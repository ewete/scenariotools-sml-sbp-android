/**
 */
package cartox;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Car To X</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link cartox.CarToX#getCars <em>Cars</em>}</li>
 *   <li>{@link cartox.CarToX#getEnvironment <em>Environment</em>}</li>
 *   <li>{@link cartox.CarToX#getStreetSections <em>Street Sections</em>}</li>
 *   <li>{@link cartox.CarToX#getObstacleControls <em>Obstacle Controls</em>}</li>
 * </ul>
 *
 * @see cartox.CartoxPackage#getCarToX()
 * @model
 * @generated
 */
public interface CarToX extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Cars</b></em>' containment reference list.
	 * The list contents are of type {@link cartox.Car}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cars</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cars</em>' containment reference list.
	 * @see cartox.CartoxPackage#getCarToX_Cars()
	 * @model containment="true"
	 * @generated
	 */
	EList<Car> getCars();

	/**
	 * Returns the value of the '<em><b>Environment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Environment</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Environment</em>' containment reference.
	 * @see #setEnvironment(Environment)
	 * @see cartox.CartoxPackage#getCarToX_Environment()
	 * @model containment="true"
	 * @generated
	 */
	Environment getEnvironment();

	/**
	 * Sets the value of the '{@link cartox.CarToX#getEnvironment <em>Environment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Environment</em>' containment reference.
	 * @see #getEnvironment()
	 * @generated
	 */
	void setEnvironment(Environment value);

	/**
	 * Returns the value of the '<em><b>Street Sections</b></em>' containment reference list.
	 * The list contents are of type {@link cartox.StreetSection}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Street Sections</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Street Sections</em>' containment reference list.
	 * @see cartox.CartoxPackage#getCarToX_StreetSections()
	 * @model containment="true"
	 * @generated
	 */
	EList<StreetSection> getStreetSections();

	/**
	 * Returns the value of the '<em><b>Obstacle Controls</b></em>' containment reference list.
	 * The list contents are of type {@link cartox.ObstacleControl}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Obstacle Controls</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Obstacle Controls</em>' containment reference list.
	 * @see cartox.CartoxPackage#getCarToX_ObstacleControls()
	 * @model containment="true"
	 * @generated
	 */
	EList<ObstacleControl> getObstacleControls();

} // CarToX
