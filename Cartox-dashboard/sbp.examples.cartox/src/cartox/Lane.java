/**
 */
package cartox;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lane</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link cartox.Lane#getLaneAreas <em>Lane Areas</em>}</li>
 * </ul>
 *
 * @see cartox.CartoxPackage#getLane()
 * @model
 * @generated
 */
public interface Lane extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Lane Areas</b></em>' containment reference list.
	 * The list contents are of type {@link cartox.LaneArea}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lane Areas</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lane Areas</em>' containment reference list.
	 * @see cartox.CartoxPackage#getLane_LaneAreas()
	 * @model containment="true"
	 * @generated
	 */
	EList<LaneArea> getLaneAreas();

} // Lane
